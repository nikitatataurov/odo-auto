#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "user" --dbname "scrapper" <<-EOSQL
    CREATE DATABASE scrapper_test;
EOSQL