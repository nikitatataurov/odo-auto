from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from factories import db_connection_factory


engine = create_engine(url=str(db_connection_factory.make_connection_string()), pool_pre_ping=True, echo=False)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db() -> Session:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
