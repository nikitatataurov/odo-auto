from datetime import datetime

from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class SaleOffer(Base):
    """Sale offer model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    mileage: Mapped[int]
    price: Mapped[int]
    created_at: Mapped[datetime]
    car_id: Mapped[int] = mapped_column(ForeignKey('car.id'))
    car: Mapped['Car'] = relationship(backref='sale_offers', init=False)
    marketplace_id: Mapped[int] = mapped_column(ForeignKey('marketplace.id'))
    marketplace: Mapped['Marketplace'] = relationship(backref='sale_offers', init=False)
    url: Mapped[str | None] = mapped_column(nullable=True, default=None)

    __table_args__ = (
        UniqueConstraint('mileage', 'price', 'created_at', 'car_id', 'marketplace_id'),
    )
