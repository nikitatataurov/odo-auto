from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from ..base_class import Base


class Token(Base):
    """Token model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    hash: Mapped[str]
    type: Mapped[str]
    user_id: Mapped[int] = mapped_column(ForeignKey('user.id'))
    removed_at: Mapped[datetime | None] = mapped_column(default=None)
    created_at: Mapped[datetime] = mapped_column(default_factory=datetime.utcnow, init=False)
