from datetime import datetime

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class ParsingHistory(Base):
    """Parsing model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    sale_offers_page_id: Mapped[int] = mapped_column(ForeignKey('sale_offers_page.id'))
    sale_offers_page: Mapped['SaleOffersPage'] = relationship(backref='parsing_history', init=False)
    ended_on_page_number: Mapped[int] = mapped_column(default=1)
    is_stopped: Mapped[bool] = mapped_column(default=False)
    created_at: Mapped[datetime] = mapped_column(default_factory=datetime.utcnow, init=False)
    updated_at: Mapped[datetime] = mapped_column(default_factory=datetime.utcnow, onupdate=datetime.utcnow, init=False)
