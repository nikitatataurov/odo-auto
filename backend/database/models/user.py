from datetime import datetime

from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class User(Base):
    """User model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    username: Mapped[str] = mapped_column(unique=True)
    email: Mapped[str] = mapped_column(unique=True)
    password: Mapped[str]
    created_at: Mapped[datetime] = mapped_column(default_factory=datetime.utcnow, init=False)
    roles: Mapped[list['Role']] = relationship(
        'Role',
        secondary='user_role',
        back_populates='users'
    )
