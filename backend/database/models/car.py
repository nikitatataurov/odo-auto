from datetime import datetime

from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class Car(Base):
    """Car model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    brand: Mapped[str]
    model: Mapped[str]
    engine_volume: Mapped[int]
    production_year: Mapped[int]
    fuel_type_id: Mapped[int] = mapped_column(ForeignKey('fuel_type.id'))
    fuel_type: Mapped['FuelType'] = relationship(backref='cars', init=False)
    created_at: Mapped[datetime] = mapped_column(default_factory=datetime.utcnow, init=False)

    __table_args__ = (
        UniqueConstraint('brand', 'model', 'engine_volume', 'production_year', 'fuel_type_id'),
    )
