from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from ..base_class import Base


class UserRole(Base):
    """Users to roles relation model"""
    id: Mapped[int] = mapped_column(primary_key=True, init=False)
    user_id: Mapped[int] = mapped_column(ForeignKey('user.id'))
    role_id: Mapped[int] = mapped_column(ForeignKey('role.id'))

    __table_args__ = (
        UniqueConstraint('user_id', 'role_id'),
    )
