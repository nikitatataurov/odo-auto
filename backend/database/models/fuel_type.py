from sqlalchemy.orm import Mapped, mapped_column

from ..base_class import Base


class FuelType(Base):
    """Fuel type model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    name: Mapped[str] = mapped_column(unique=True)
