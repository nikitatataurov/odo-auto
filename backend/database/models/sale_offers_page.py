from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class SaleOffersPage(Base):
    """Sale offers page model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    marketplace_id: Mapped[int] = mapped_column(ForeignKey('marketplace.id'))
    marketplace: Mapped['Marketplace'] = relationship(backref='sale_offers_pages', init=False)
    brand: Mapped[str]
    path: Mapped[str]
    query_params: Mapped[str | None] = mapped_column(default=None)

    __table_args__ = (
        UniqueConstraint('marketplace_id', 'brand', 'path'),
    )
