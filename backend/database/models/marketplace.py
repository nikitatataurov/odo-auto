from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class Marketplace(Base):
    """Marketplace model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    name: Mapped[str] = mapped_column(unique=True)
    country_id: Mapped[int] = mapped_column(ForeignKey('country.id'))
    country: Mapped['Country'] = relationship(back_populates='marketplaces', init=False)
    url: Mapped[str]
