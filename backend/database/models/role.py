from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class Role(Base):
    """User`s role model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    name: Mapped[str]
    users: Mapped[list['User']] = relationship(
        'User',
        secondary='user_role',
        back_populates='roles',
        init=False
    )
