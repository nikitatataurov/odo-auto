from sqlalchemy.orm import Mapped, mapped_column, relationship

from ..base_class import Base


class Country(Base):
    """Country model"""
    id: Mapped[int] = mapped_column(primary_key=True, index=True, init=False)
    code: Mapped[str] = mapped_column(unique=True)
    name: Mapped[str] = mapped_column(unique=True)
    marketplaces: Mapped[list['Marketplace']] = relationship(back_populates='country', init=False)
