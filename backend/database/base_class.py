import re

from typing import Any

from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import DeclarativeBase, MappedAsDataclass


class Base(MappedAsDataclass, DeclarativeBase):
    id: Any

    @declared_attr
    def __tablename__(cls) -> str:
        return '_'.join(re.findall(r'[A-Z][a-z]+', cls.__name__)).lower()
