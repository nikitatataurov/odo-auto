from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for marketplaces"""
    return db.query(models.Marketplace)


def get_all(db: Session) -> list[models.Marketplace]:
    """Get all marketplaces"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.Marketplace | None:
    """Get marketplace by id"""
    return get_base_query(db=db).filter(models.Marketplace.id == id).first()


def get_count_all(db: Session) -> int:
    """Get count of all entries"""
    return get_base_query(db=db).count()


def get_by_name(db: Session, name: str) -> models.Marketplace | None:
    """Get marketplace by name"""
    return get_base_query(db=db).filter(models.Marketplace.name == name).first()


def create(db: Session, marketplace: schemas.MarketplaceCreate) -> models.Marketplace:
    """Create marketplace from schema"""
    db_marketplace: models.Marketplace = models.Marketplace(**marketplace.model_dump())
    db.add(db_marketplace)
    db.commit()
    db.refresh(db_marketplace)

    return db_marketplace
