from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for sale offers pages"""
    return db.query(models.SaleOffersPage)


def get_all(db: Session) -> list[models.SaleOffersPage]:
    """Get all sale offers pages"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.SaleOffersPage | None:
    """Get sale offers page by id"""
    return get_base_query(db=db).filter(models.SaleOffersPage.id == id).first()


def get_count_all(db: Session) -> int:
    """Get count of all entries"""
    return get_base_query(db=db).count()


def get_by_marketplace_id_and_brand(
        db: Session,
        marketplace_id: int,
        brand: str,
) -> models.SaleOffersPage | None:
    """Get sale offers page by marketplace_id and brand"""
    return get_base_query(db=db).filter(
        models.SaleOffersPage.marketplace_id == marketplace_id,
        models.SaleOffersPage.brand == brand
    ).first()


def get_all_by_marketplace_id(
        db: Session,
        marketplace_id: int,
) -> list[models.SaleOffersPage]:
    """Get sale offers pages by marketplace_id"""
    return get_base_query(db=db).filter(models.SaleOffersPage.marketplace_id == marketplace_id).all()


def create(db: Session, sale_offers_page: schemas.SaleOffersPageCreate) -> models.SaleOffersPage:
    """Create sale offers page from schema"""
    db_sale_offers_page: models.SaleOffersPage = models.SaleOffersPage(**sale_offers_page.model_dump())
    db.add(db_sale_offers_page)
    db.commit()
    db.refresh(db_sale_offers_page)

    return db_sale_offers_page
