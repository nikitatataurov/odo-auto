from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for fuel types"""
    return db.query(models.FuelType)


def get_all(db: Session) -> list[models.FuelType]:
    """Get all fuel types"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.FuelType | None:
    """Get fuel type by id"""
    return get_base_query(db=db).filter(models.FuelType.id == id).first()


def get_count_all(db: Session) -> int:
    """Get count of all entries"""
    return get_base_query(db=db).count()


def get_by_name(db: Session, name: str) -> models.FuelType | None:
    """Get fuel type by name"""
    return get_base_query(db=db).filter(models.FuelType.name == name).first()


def create(db: Session, fuel_type: schemas.FuelTypeCreate) -> schemas.FuelType:
    """Create fuel type from schema"""
    db_fuel_type: models.FuelType = models.FuelType(**fuel_type.model_dump())
    db.add(db_fuel_type)
    db.commit()
    db.refresh(db_fuel_type)

    return db_fuel_type
