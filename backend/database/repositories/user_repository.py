from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for user"""
    return db.query(models.User)


def get_all(db: Session) -> list[models.User]:
    """Get all users"""
    return get_base_query(db=db).all()


def get_by_username(db: Session, username: str) -> models.User | None:
    """Get user by name"""
    return get_base_query(db=db).filter(models.User.username == username).first()


def create(db: Session, user: schemas.UserCreate, roles: list[models.Role]) -> models.User:
    """Create user from schema"""
    db_user: models.User = models.User(**user.model_dump(), roles=roles)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user
