from datetime import datetime

from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for token"""
    return db.query(models.Token)


def get_by_hash(db: Session, hash: str) -> models.Token | None:
    """Get token by hash"""
    return get_base_query(db=db).filter(models.Token.hash == hash).first()


def create(db: Session, token: schemas.TokenCreate) -> models.Token:
    """Create token from schema"""
    db_token: models.Token = models.Token(**token.model_dump())
    db.add(db_token)
    db.commit()
    db.refresh(db_token)

    return db_token


def set_removed_at(db: Session, db_token: models.Token, removed_at: datetime) -> models.Token:
    """Set removed_at for token"""
    db_token.removed_at = removed_at
    db.add(db_token)
    db.commit()
    db.refresh(db_token)

    return db_token
