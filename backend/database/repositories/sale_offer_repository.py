from datetime import datetime

from sqlalchemy import func, extract
from sqlalchemy.orm import Session, Query

import schemas
import value_objects
from services import date_service
from . import marketplace_repository
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for sale offers"""
    return db.query(models.SaleOffer)


def get_query_by_marketplace_for_current_month(db: Session, marketplace_name: value_objects.MarketplaceName) -> Query:
    """Get sale offers-query by marketplace name for current month"""
    month, year = date_service.get_current_month_and_year()

    return get_base_query(db=db).filter(
        models.SaleOffer.marketplace_id == marketplace_repository.get_by_name(db=db, name=marketplace_name).id,
        extract('year', models.SaleOffer.created_at) == year,
        extract('month', models.SaleOffer.created_at) == month,
    )


def get_all(db: Session) -> list[models.SaleOffer]:
    """Get all sale offers"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.SaleOffer | None:
    """Get sale offer by id"""
    return get_base_query(db=db).filter(models.SaleOffer.id == id).first()


def get_count_all(db: Session) -> int:
    """Get count of all entries"""
    return get_base_query(db=db).count()


def get(
        db: Session,
        mileage: int,
        price: int,
        car_id: int,
        marketplace_id: int,
        created_at: datetime,
        **_,
) -> models.SaleOffer | None:
    """Get sale offer by fields combination"""
    return get_base_query(db=db).filter(
        models.SaleOffer.mileage == mileage,
        models.SaleOffer.price == price,
        models.SaleOffer.car_id == car_id,
        models.SaleOffer.marketplace_id == marketplace_id,
        func.date(models.SaleOffer.created_at) == created_at.date()
    ).first()


def create(db: Session, sale_offer: schemas.SaleOfferCreate) -> models.SaleOffer:
    """Create sale offer from schema"""
    db_sale_offer: models.SaleOffer = models.SaleOffer(**sale_offer.model_dump())
    db.add(db_sale_offer)
    db.commit()
    db.refresh(db_sale_offer)

    return db_sale_offer
