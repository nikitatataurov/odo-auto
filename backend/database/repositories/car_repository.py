from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for cars"""
    return db.query(models.Car)


def get_all(db: Session) -> list[models.Car]:
    """Get all cars"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.Car | None:
    """Get car by id"""
    return get_base_query(db=db).filter(models.Car.id == id).first()


def get_count_all(db: Session) -> int:
    """Get count of all entries"""
    return get_base_query(db=db).count()


def get(
        db: Session,
        brand: str,
        model: str,
        engine_volume: int,
        production_year: int,
        fuel_type_id: int,
        **_,
) -> models.Car | None:
    """Get car by fields combination"""
    return get_base_query(db=db).filter(
        models.Car.brand == brand,
        models.Car.model == model,
        models.Car.engine_volume == engine_volume,
        models.Car.production_year == production_year,
        models.Car.fuel_type_id == fuel_type_id
    ).first()


def create(db: Session, car: schemas.CarCreate) -> models.Car:
    """Create car from schema"""
    db_car: models.Car = models.Car(**car.model_dump())
    db.add(db_car)
    db.commit()
    db.refresh(db_car)

    return db_car
