from sqlalchemy.orm import Session, Query

import value_objects
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for roles"""
    return db.query(models.Role)


def get_by_name(db: Session, name: str) -> models.Role | None:
    """Get role by name"""
    return get_base_query(db=db).filter(models.Role.name == name).first()


def get_admin(db: Session) -> models.Role | None:
    """Get admin role"""
    return get_base_query(db=db).filter(models.Role.name == value_objects.RoleName.ADMIN).first()


def get_user(db: Session) -> models.Role | None:
    """Get user role"""
    return get_base_query(db=db).filter(models.Role.name == value_objects.RoleName.USER).first()
