from sqlalchemy import update, Update
from sqlalchemy.orm import Session, Query

import schemas
from services import date_service
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for parsing history"""
    return db.query(models.ParsingHistory)


def get_all(db: Session) -> list[models.ParsingHistory]:
    """Get all parsing history"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.ParsingHistory | None:
    """Get parsing history by id"""
    return get_base_query(db=db).filter(models.ParsingHistory.id == id).first()


def get_actual_by_sale_offers_page_id(
        db: Session,
        sale_offers_page_id: int,
) -> models.ParsingHistory | None:
    """Get parsing_history in current month by sale_offers_page_id"""
    return get_base_query(db=db).filter(
        models.ParsingHistory.sale_offers_page_id == sale_offers_page_id,
        models.ParsingHistory.created_at.between(
            date_service.get_current_month_first_day(), date_service.get_current_month_last_day()
        ),
    ).first()


def create(db: Session, parsing_history: schemas.ParsingHistoryCreate) -> models.ParsingHistory:
    """Create parsing history from schema"""
    db_parsing_history: models.ParsingHistory = models.ParsingHistory(**parsing_history.model_dump())
    db.add(db_parsing_history)
    db.commit()
    db.refresh(db_parsing_history)

    return db_parsing_history


def set_ended_on_page_number(
        db: Session,
        db_parsing_history: models.ParsingHistory,
        ended_on_page_number: int,
) -> models.ParsingHistory:
    """Set ended_on_page_number for parsing_history"""
    db_parsing_history.ended_on_page_number = ended_on_page_number
    db.add(db_parsing_history)
    db.commit()
    db.refresh(db_parsing_history)

    return db_parsing_history


def set_is_stopped(
        db: Session,
        db_parsing_history: models.ParsingHistory,
        is_stopped: bool
) -> models.ParsingHistory:
    """Set is_stopped for parsing_history"""
    db_parsing_history.is_stopped = is_stopped
    db.add(db_parsing_history)
    db.commit()
    db.refresh(db_parsing_history)

    return db_parsing_history


def set_is_stopped_by_marketplace_id(db: Session, marketplace_id: int, is_stopped: bool) -> None:
    """Set is_stopped for all parsing_history of marketplace"""
    query: Update = update(models.ParsingHistory).values(is_stopped=is_stopped).where(
        models.ParsingHistory.sale_offers_page_id == models.SaleOffersPage.id
    ).where(
        models.SaleOffersPage.marketplace_id == marketplace_id
    )
    db.execute(query)
    db.commit()


def check_is_stopped_all_by_marketplace_id(db: Session, marketplace_id: int) -> bool:
    """Check is all is_stopped for parsing_history of marketplace"""
    return get_base_query(db=db).join(
        models.SaleOffersPage
    ).filter(
        models.SaleOffersPage.marketplace_id == marketplace_id,
        models.ParsingHistory.is_stopped.is_(False),
    ).first() is None
