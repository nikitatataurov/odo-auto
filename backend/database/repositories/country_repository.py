from sqlalchemy.orm import Session, Query

import schemas
from .. import models


def get_base_query(db: Session) -> Query:
    """Get base query for countries"""
    return db.query(models.Country)


def get_all(db: Session) -> list[models.Country]:
    """Get all countries"""
    return get_base_query(db=db).all()


def get_by_id(db: Session, id: int) -> models.Country | None:
    """Get country by id"""
    return get_base_query(db=db).filter(models.Country.id == id).first()


def get_count_all(db: Session) -> int:
    """Get count of all entries"""
    return get_base_query(db=db).count()


def get_by_code(db: Session, code: str) -> models.Country | None:
    """Get country by name"""
    return get_base_query(db=db).filter(models.Country.code == code).first()


def create(db: Session, country: schemas.CountryCreate) -> models.Country:
    """Create country from schema"""
    db_country: models.Country = models.Country(**country.model_dump())
    db.add(db_country)
    db.commit()
    db.refresh(db_country)

    return db_country
