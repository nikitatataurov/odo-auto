from celery import Celery

import value_objects
from config import settings
from database.session import SessionLocal
from services import parsing_service
from telegram.loggin_bot import TelegramLoggingBot

celery = Celery('tasks', broker=settings.CELERY_BROKER_URL, backend=settings.CELERY_RESULT_BACKEND)


@celery.task
def parse_marketplace(marketplace_name: str):
    parse: callable = parsing_service.get_parse_marketplace_function(marketplace_name=marketplace_name)

    telegram_logging_bot: TelegramLoggingBot = TelegramLoggingBot()
    telegram_logging_bot.log(f'Start parsing {marketplace_name}')
    try:
        with SessionLocal() as db:
            parse(db=db, telegram_logging_bot=telegram_logging_bot)
    except Exception as e:
        telegram_logging_bot.log(str(e))
    telegram_logging_bot.log(f'Ended parsing {marketplace_name}')
