from functools import lru_cache

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic_settings import BaseSettings

from config import Settings
from api.v1 import router as api_router
from auth import router as auth_router


@lru_cache()
def get_settings() -> BaseSettings:
    return Settings()


def include_routers(app: FastAPI) -> None:
    app.include_router(api_router)
    app.include_router(auth_router)


def add_origins(app: FastAPI) -> None:
    origins = [
        'http://localhost',
        'http://localhost:80',
    ]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )


def start_app() -> FastAPI:
    app: FastAPI = FastAPI(openapi_url='/api/v1/openapi.json')
    include_routers(app)
    add_origins(app)

    return app


app = start_app()
