import datetime

import bs4
from cachetools.func import ttl_cache
import requests

import value_objects
from value_objects import CurrencyName


def _get_ratio_to_rub_by_currency_name(currency_name: str) -> float:
    """Get ratio currency to rub"""
    requested_date_str: str = datetime.date.today().strftime('%d/%m/%Y')
    response = requests.get(f'{value_objects.CbrCurrencyUrl.DAILY.value}?date_req={requested_date_str}')
    xml = response.text
    soup = bs4.BeautifulSoup(xml.encode('cp1251'), 'xml')
    char_code_tag: bs4.Tag = soup.find(lambda tag: tag.name == 'CharCode' and currency_name in tag.text)

    return float(char_code_tag.find_parent().find('VunitRate').text.replace(',', '.'))


@ttl_cache(maxsize=128, ttl=86400)
def get_ratio_rate_usd_to_rub() -> float:
    """Get ratio rate for rub to usd converting"""
    return _get_ratio_to_rub_by_currency_name(CurrencyName.USD)


@ttl_cache(maxsize=128, ttl=86400)
def get_ratio_rate_kzt_to_rub() -> float:
    """Get ratio rate for rub to kzt converting"""
    return _get_ratio_to_rub_by_currency_name(CurrencyName.KZT)


@ttl_cache(maxsize=128, ttl=86400)
def get_ratio_rate_pln_to_rub() -> float:
    """Get ratio rate for rub to kzt converting"""
    return _get_ratio_to_rub_by_currency_name(CurrencyName.PLN)
