from fastapi import Depends

import value_objects
from database import models
from . import user_service


async def is_user(db_user: models.User | None = Depends(user_service.get_current_user)) -> bool:
    """Check user`s role is user"""
    if db_user is None:
        return False

    return value_objects.RoleName.USER in [role.name for role in db_user.roles]


async def is_admin(db_user: models.User | None = Depends(user_service.get_current_user)) -> bool:
    """Check user`s role is admin"""
    if db_user is None:
        return False

    return value_objects.RoleName.ADMIN in [role.name for role in db_user.roles]
