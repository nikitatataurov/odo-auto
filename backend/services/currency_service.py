from services import cbr_service


def convert_kzt_to_usd(kzt_amount: int) -> int:
    """Convert kzt to usd"""
    return int(cbr_service.get_ratio_rate_kzt_to_rub() / cbr_service.get_ratio_rate_usd_to_rub() * kzt_amount)


def convert_pln_to_usd(pln_amount: int) -> int:
    """Convert pln to usd"""
    return int(cbr_service.get_ratio_rate_pln_to_rub() / cbr_service.get_ratio_rate_usd_to_rub() * pln_amount)
