import datetime

from fastapi import Depends
from jose import jwt
from sqlalchemy.orm import Session

import exceptions
import schemas
from config import settings
from database import models
from database.repositories import user_repository, token_repository
from factories import token_type_factory
from . import role_service
from . import oauth2_scheme


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Verify password"""
    return settings.PWD_CONTEXT.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    """Get password hash"""
    return settings.PWD_CONTEXT.hash(password)


def authenticate_user(db: Session, username: str, password: str) -> models.User | None:
    """Authenticate user"""
    db_user: models.User = user_repository.get_by_username(db=db, username=username)

    if db_user is None or not verify_password(password, db_user.password):
        return None

    return db_user


def create_jwt_token(data: dict, expires_delta: datetime.timedelta | None = None) -> str:
    """Base creation method for jwt token"""
    to_encode: dict = data.copy()
    now: datetime.datetime = datetime.datetime.utcnow()
    expire: datetime.datetime = now + (expires_delta if expires_delta else datetime.timedelta(minutes=15))
    to_encode.update({"exp": expire.timestamp()})
    encoded_jwt: str = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.CRYPTOGRAPHIC_ALGORITHM)

    return encoded_jwt


def decode_token(token: str) -> dict:
    """Decode token"""
    return jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.CRYPTOGRAPHIC_ALGORITHM])


def create_access_token(username: str, user_id: int, db: Session) -> models.Token:
    """Create access token"""
    expires: datetime.timedelta = datetime.timedelta(minutes=1)
    token: str = create_jwt_token(
        data={'sub': username}, expires_delta=expires
    )

    return token_repository.create(
        db=db,
        token=schemas.TokenCreate(
            hash=token,
            type=token_type_factory.make_access().name,
            user_id=user_id
        )
    )


def create_refresh_token(username: str, user_id: int, db: Session) -> models.Token:
    """Create refresh token"""
    expires: datetime.timedelta = datetime.timedelta(minutes=settings.REFRESH_TOKEN_EXPIRE_MINUTES)
    token: str = create_jwt_token(
        data={'sub': username}, expires_delta=expires
    )

    return token_repository.create(
        db=db,
        token=schemas.TokenCreate(
            hash=token,
            type=token_type_factory.make_refresh().name,
            user_id=user_id
        )
    )


async def get_user_token(
        token: str = Depends(oauth2_scheme),
        is_user: bool = Depends(role_service.is_user)
) -> str:
    """Get active token of user"""
    if not is_user:
        raise exceptions.CREDENTIALS_EXCEPTION

    return token


async def get_admin_token(
        token: str = Depends(oauth2_scheme),
        is_admin: bool = Depends(role_service.is_admin)
) -> str:
    """Get active token of admin"""
    if not is_admin:
        raise exceptions.CREDENTIALS_EXCEPTION

    return token


def is_token_removed(token: str, db: Session) -> bool:
    """Check is token removed"""
    db_token: models.Token | None = token_repository.get_by_hash(db=db, hash=token)

    if db_token is None:
        return False

    if db_token.removed_at is not None:
        return True

    return False


def remove_token(token: str, db: Session) -> bool:
    """Remove token"""
    token_repository.set_removed_at(
        db=db,
        db_token=token_repository.get_by_hash(db=db, hash=token),
        removed_at=datetime.datetime.utcnow()
    )

    return True
