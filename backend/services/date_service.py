import datetime


def get_current_month_last_day() -> datetime.date:
    """Get the last day of the current month"""
    date: datetime.date = datetime.date.today()
    if date.month == 12:
        return date.replace(day=31)
    return date.replace(month=date.month+1, day=1) - datetime.timedelta(days=1)


def get_current_month_first_day() -> datetime.date:
    """Get the first day of the current month"""
    return datetime.date.today().replace(day=1)


def get_current_month_and_year() -> tuple[int, int]:
    """Get the current month number and year"""
    today: datetime.date = datetime.date.today()
    return today.month, today.year
