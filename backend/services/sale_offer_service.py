import datetime

from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import fuel_type_repository, car_repository, sale_offer_repository


def create_sale_offer(
        db: Session,
        fuel_type_name: value_objects.FuelTypeName,
        brand: str,
        model: str,
        production_year: int,
        engine_volume: int,
        mileage: int,
        price: int,
        marketplace_id: int,
        url: str
):
    fuel_type: models.FuelType | None = fuel_type_repository.get_by_name(
        db=db,
        name=fuel_type_name
    )

    car: schemas.CarCreate = schemas.CarCreate(
        brand=brand,
        model=model,
        production_year=production_year,
        engine_volume=engine_volume,
        fuel_type_id=fuel_type.id
    )

    car_db: models.Car = car_repository.get(
        db=db,
        **car.model_dump()
    )

    if car_db is None:
        car_db = car_repository.create(db=db, car=car)

    sale_offer: schemas.SaleOfferCreate = schemas.SaleOfferCreate(
        mileage=mileage,
        price=price,
        car_id=car_db.id,
        marketplace_id=marketplace_id,
        created_at=datetime.datetime.now(),
        url=url
    )

    sale_offer_db: models.SaleOffer = sale_offer_repository.get(
        db=db,
        **sale_offer.model_dump()
    )

    if sale_offer_db is None:
        sale_offer_repository.create(db=db, sale_offer=sale_offer)
