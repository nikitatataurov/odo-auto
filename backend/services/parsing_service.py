import re
import time
from typing import Optional
from urllib.parse import urljoin

from bs4 import BeautifulSoup, Tag
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from sqlalchemy.orm import Session

import schemas
import value_objects
from context_managers import WebdriverManager
from database import models
from database.repositories import marketplace_repository, sale_offers_page_repository, parsing_history_repository
from telegram.loggin_bot import TelegramLoggingBot
from . import fuel_type_service, sale_offer_service, currency_service, parsing_history_service


def stop_parsing_marketplace(
        db: Session,
        telegram_logging_bot: TelegramLoggingBot,
        marketplace_name: value_objects.MarketplaceName,
) -> bool:
    """Stop the parsing"""
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)
    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name} doesn`t exist')
        return False

    parsing_history_repository.set_is_stopped_by_marketplace_id(
        db=db,
        marketplace_id=marketplace.id,
        is_stopped=True
    )

    return True


def check_parsing_is_running(
        db: Session,
        telegram_logging_bot: TelegramLoggingBot,
        marketplace_name: value_objects.MarketplaceName,
) -> bool:
    """Check is any of marketplace sale offers page is parsing"""
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)
    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name} doesn`t exist')
        return False

    return not parsing_history_repository.check_is_stopped_all_by_marketplace_id(
        db=db,
        marketplace_id=marketplace.id
    )


def get_parse_marketplace_function(marketplace_name: str) -> callable:
    """Get parse marketplace function by marketplace name"""
    if value_objects.MarketplaceName.AUTOBUY_BY == marketplace_name:
        return parse_autobuy_by

    if value_objects.MarketplaceName.AUTOBY_BY == marketplace_name:
        return parse_autoby_by

    if value_objects.MarketplaceName.ASTER_KZ == marketplace_name:
        return parse_aster_kz

    if value_objects.MarketplaceName.KOLESA_KZ == marketplace_name:
        return parse_kolesa_kz

    if value_objects.MarketplaceName.OTOMOTO_PL == marketplace_name:
        return parse_otomoto_pl


def get_parsing_description_list(db: Session) -> list[schemas.ParsingDescription]:
    """Prepare description for all marketplaces"""
    marketplaces: list[models.Marketplace] = marketplace_repository.get_all(db=db)
    parsing_descriptions: list[schemas.ParsingDescription] = []
    for marketplace in marketplaces:
        marketplace_name: str = marketplace.name
        parsing_descriptions.append(
            schemas.ParsingDescription(
                marketplace_name=marketplace_name,
                start_parsing_route=f'/{marketplace_name}/start/',
                stop_parsing_route=f'/{marketplace_name}/stop/',
                is_running_parsing_route=f'/{marketplace_name}/is-running/'
            )
        )

    return parsing_descriptions


def parse_autobuy_by(db: Session, telegram_logging_bot: TelegramLoggingBot) -> None:
    """Parse autobuy.by and create entries to db"""
    marketplace_name: value_objects.MarketplaceName = value_objects.MarketplaceName.AUTOBUY_BY
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)

    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name.value} doesn`t exist')
        return

    for sale_offer_page in sale_offers_page_repository.get_all_by_marketplace_id(db=db, marketplace_id=marketplace.id):
        parsing_history: models.ParsingHistory = parsing_history_service.start_or_continue_parsing_history(
            db=db,
            sale_offers_page_id=sale_offer_page.id
        )

        ended_on_page_number: int = parsing_history.ended_on_page_number
        telegram_logging_bot.log(f'{marketplace_name}: page {ended_on_page_number} parsing was started')

        with WebdriverManager() as webdriver:
            if parsing_history.is_stopped:
                break

            url: str = urljoin(marketplace.url, f'{sale_offer_page.path}?{sale_offer_page.query_params}')
            webdriver.get(url)

            while True:
                try:
                    element = webdriver.find_element(By.CLASS_NAME, 's-more')
                except NoSuchElementException:
                    break
                element.click()
                time.sleep(5)

            html: str = webdriver.page_source

        soup: BeautifulSoup = BeautifulSoup(html, 'html.parser')
        for car_description in soup.find_all('div', {'class': 'i-car'}):
            path: str = car_description.parent['href']

            url: str | None = None
            if path is not None:
                url = urljoin(marketplace.url, path)

            _, model, *_ = map(
                lambda string: string.lower(),
                car_description.find(class_='cars__title-text').text.split()
            )

            car_params: list = [
                car_param.text for car_param in car_description.find_all('p', {'class': 'short__car-par'})
            ]
            engine_volume: int = int(float(car_params[0].split()[0]) * 1000)
            production_year: int = int(car_params[2].split()[0])
            mileage: int = int(car_params[3][:-3].replace(' ', ''))
            price: int = int(''.join(re.findall(r'\d+', car_description.find(class_='usd__text').text)))

            fuel_type_name: value_objects.FuelTypeName = fuel_type_service.get_fuel_type_by_ru_name(car_params[1])

            sale_offer_service.create_sale_offer(
                db=db,
                fuel_type_name=fuel_type_name,
                brand=sale_offer_page.brand,
                model=model,
                production_year=production_year,
                engine_volume=engine_volume,
                mileage=mileage,
                price=price,
                marketplace_id=marketplace.id,
                url=url
            )

        parsing_history_repository.set_is_stopped(
            db=db,
            db_parsing_history=parsing_history,
            is_stopped=True
        )


def parse_autoby_by(db: Session, telegram_logging_bot: TelegramLoggingBot) -> None:
    """Parse autoby.by and create entries to db"""
    marketplace_name: value_objects.MarketplaceName = value_objects.MarketplaceName.AUTOBY_BY
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)

    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name.value} doesn`t exist')
        return

    for sale_offer_page in sale_offers_page_repository.get_all_by_marketplace_id(db=db, marketplace_id=marketplace.id):
        parsing_history: models.ParsingHistory = parsing_history_service.start_or_continue_parsing_history(
            db=db,
            sale_offers_page_id=sale_offer_page.id
        )

        page: int = parsing_history.ended_on_page_number

        with WebdriverManager() as webdriver:
            while True:
                if parsing_history.is_stopped:
                    break

                url: str = urljoin(
                    marketplace.url,
                    f'{sale_offer_page.path}?{sale_offer_page.query_params.format(page=page)}'
                )
                telegram_logging_bot.log(f'{marketplace_name}: {url} parsing has been started ')

                webdriver.get(url)
                time.sleep(15)
                soup: BeautifulSoup = BeautifulSoup(webdriver.page_source, 'html.parser')

                car_descriptions: list[Tag] = soup.find_all('div', {'class': 'info-car'})

                pagination_span: Optional[list] = soup.find_all('span', {'class': 'pagination-page-total'})
                last_page: int = int(pagination_span[0].text.split()[1]) if pagination_span else 1

                if not car_descriptions or page > last_page:
                    parsing_history_repository.set_is_stopped(
                        db=db,
                        db_parsing_history=parsing_history,
                        is_stopped=True
                    )
                    break

                for car_description in car_descriptions:
                    path: str = car_description.find('a')['href']

                    url: str | None = None
                    if path is not None:
                        url = urljoin(marketplace.url, path)

                    _, model, *_ = map(
                        lambda string: string.lower(),
                        car_description.find(class_='info-car-title').text.split()
                    )

                    car_params: list = car_description.find(class_='info-car-list').text.strip().split(', ')
                    engine_volume: int = int(car_params[4].strip().split()[0])
                    production_year: int = int(car_params[0].split()[0])
                    mileage: int = int(car_params[3].strip().split()[0])
                    price: int = int(''.join(re.findall(r'\d+', car_description.find(class_='info-price').text)))
                    fuel_type_name: value_objects.FuelTypeName = fuel_type_service.get_fuel_type_by_ru_name(
                        car_params[-1].split()[-1]
                    )

                    sale_offer_service.create_sale_offer(
                        db=db,
                        fuel_type_name=fuel_type_name,
                        brand=sale_offer_page.brand,
                        model=model,
                        production_year=production_year,
                        engine_volume=engine_volume,
                        mileage=mileage,
                        price=price,
                        marketplace_id=marketplace.id,
                        url=url
                    )

                page += 1
                parsing_history_repository.set_ended_on_page_number(
                    db=db,
                    db_parsing_history=parsing_history,
                    ended_on_page_number=page
                )


def parse_aster_kz(db: Session, telegram_logging_bot: TelegramLoggingBot) -> None:
    """Parse aster.kz and create entries to db"""
    marketplace_name: value_objects.MarketplaceName = value_objects.MarketplaceName.ASTER_KZ
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)

    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name.value} doesn`t exist')
        return

    for sale_offer_page in sale_offers_page_repository.get_all_by_marketplace_id(db=db, marketplace_id=marketplace.id):
        parsing_history: models.ParsingHistory = parsing_history_service.start_or_continue_parsing_history(
            db=db,
            sale_offers_page_id=sale_offer_page.id
        )

        page: int = parsing_history.ended_on_page_number

        with WebdriverManager() as webdriver:
            while True:
                if parsing_history.is_stopped:
                    break

                url: str = urljoin(
                    marketplace.url,
                    f'{sale_offer_page.path}?{sale_offer_page.query_params.format(page=page)}'
                )

                telegram_logging_bot.log(f'{marketplace_name}: {url} parsing has been started ')

                webdriver.get(url)
                time.sleep(15)
                soup: BeautifulSoup = BeautifulSoup(webdriver.page_source, 'html.parser')

                car_descriptions: list[Tag] = soup.find_all('div', {'class': 'car fadeIn'})

                pagination_ul: Optional[list] = soup.find_all('ul', {'class': 'pagination'})
                last_page: int = int(
                    pagination_ul[0].find_all('li', {'class': 'page-item'})[-2].text
                ) if pagination_ul else 1

                if not car_descriptions or page > last_page:
                    parsing_history_repository.set_is_stopped(
                        db=db,
                        db_parsing_history=parsing_history,
                        is_stopped=True
                    )
                    break

                for car_description in car_descriptions:
                    if car_description.find(class_='promotion-item'):
                        continue

                    path: str = car_description.find(class_='car__text car-link')['href']

                    url: str | None = None
                    if path is not None:
                        url = urljoin(marketplace.url, path)

                    _, *model, production_year = map(
                        lambda string: string.lower(),
                        car_description.find(class_='marka-model').text.split()
                    )

                    model: str = ' '.join(model)

                    car_params: list = car_description.find(class_='car__text car-link').text.strip().split(', ')

                    if ' л' in car_params[0]:
                        continue

                    if not any(char.isdigit() for char in car_params[1]):
                        continue

                    engine_volume: int = int(float(''.join(re.findall(r'\d|\.', car_params[1]))) * 1000)
                    mileage: int = 0
                    if 'Новый' not in car_params[0]:
                        mileage: int = int(''.join(re.findall(r'\d+', car_params[0])))

                    price_in_kzt: int = int(''.join(re.findall(r'\d+', car_description.find(class_='price').text)))
                    fuel_type_name: value_objects.FuelTypeName = fuel_type_service.get_fuel_type_by_ru_name(
                        car_params[2]
                    )

                    sale_offer_service.create_sale_offer(
                        db=db,
                        fuel_type_name=fuel_type_name,
                        brand=sale_offer_page.brand,
                        model=model,
                        production_year=int(production_year),
                        engine_volume=engine_volume,
                        mileage=mileage,
                        price=currency_service.convert_kzt_to_usd(kzt_amount=price_in_kzt),
                        marketplace_id=marketplace.id,
                        url=url
                    )

                page += 1
                parsing_history_repository.set_ended_on_page_number(
                    db=db,
                    db_parsing_history=parsing_history,
                    ended_on_page_number=page
                )


def parse_kolesa_kz(db: Session, telegram_logging_bot: TelegramLoggingBot) -> None:
    """Parse kolesa.kz and create entries to db"""
    marketplace_name: value_objects.MarketplaceName = value_objects.MarketplaceName.KOLESA_KZ
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)

    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name.value} doesn`t exist')
        return

    for sale_offer_page in sale_offers_page_repository.get_all_by_marketplace_id(db=db, marketplace_id=marketplace.id):
        parsing_history: models.ParsingHistory = parsing_history_service.start_or_continue_parsing_history(
            db=db,
            sale_offers_page_id=sale_offer_page.id
        )

        page: int = parsing_history.ended_on_page_number

        with WebdriverManager() as webdriver:
            while True:
                if parsing_history.is_stopped:
                    break

                url: str = urljoin(marketplace.url, f'{sale_offer_page.path}')
                if page > 1:
                    url += f'?{sale_offer_page.query_params.format(page=page)}'

                telegram_logging_bot.log(f'{marketplace_name}: {url} parsing has been started ')

                webdriver.get(url)
                time.sleep(15)
                soup: BeautifulSoup = BeautifulSoup(webdriver.page_source, 'html.parser')

                car_descriptions: list[Tag] = soup.find_all('div', {'class': 'a-card__info'})

                pagination_div: Optional[list] = soup.find_all('div', {'class': 'pager'})
                last_page: int = int(pagination_div[0].find_all('li')[-1].text) if pagination_div else 1

                if not car_descriptions or page > last_page:
                    parsing_history_repository.set_is_stopped(
                        db=db,
                        db_parsing_history=parsing_history,
                        is_stopped=True
                    )
                    break

                for car_description in car_descriptions:
                    header: Tag = car_description.find(class_='a-card__header')

                    if header is None:
                        continue

                    a_tag: Tag = header.find(class_='a-card__link')
                    path: str = a_tag['href']

                    url: str | None = None
                    if path is not None:
                        url = urljoin(marketplace.url, path)

                    _, *model = map(
                        lambda string: string.lower(),
                        a_tag.text.split()
                    )
                    model: str = ' '.join(model)

                    content: Tag = car_description.find(class_='a-card__description')
                    car_params_string: str = content.text.strip()
                    car_params: list = car_params_string.split(',')
                    mileage_with_units: re.Match | None = re.search(r'\d* \d+ км', car_params_string)
                    if mileage_with_units is None:
                        continue

                    engine_volume_with_units: re.Match | None = re.search(r'\d*\.*\d+ л', car_params_string)
                    if engine_volume_with_units is None:
                        continue

                    mileage: int = int(mileage_with_units.group().split('км')[0].replace(' ', ''))
                    engine_volume: int = int(float(engine_volume_with_units.group().split('л')[0]) * 1000)

                    production_year: int = int(re.search(r'\d\d\d\d г\.', car_params_string).group().split()[0])

                    price_in_kzt: int = int(''.join(re.findall(r'\d+', header.find(class_='a-card__price').text)))

                    offset: int = 0
                    if str(production_year) in car_params[1]:
                        offset = 1

                    fuel_type_name: value_objects.FuelTypeName = fuel_type_service.get_fuel_type_by_ru_name(
                        car_params[3 + offset].strip()
                    )

                    sale_offer_service.create_sale_offer(
                        db=db,
                        fuel_type_name=fuel_type_name,
                        brand=sale_offer_page.brand,
                        model=model,
                        production_year=int(production_year),
                        engine_volume=engine_volume,
                        mileage=mileage,
                        price=currency_service.convert_kzt_to_usd(kzt_amount=price_in_kzt),
                        marketplace_id=marketplace.id,
                        url=url
                    )

                page += 1
                parsing_history_repository.set_ended_on_page_number(
                    db=db,
                    db_parsing_history=parsing_history,
                    ended_on_page_number=page
                )


def parse_otomoto_pl(db: Session, telegram_logging_bot: TelegramLoggingBot) -> None:
    """Parse otomoto.pl and create entries to db"""
    marketplace_name: value_objects.MarketplaceName = value_objects.MarketplaceName.OTOMOTO_PL
    marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace_name)

    if marketplace is None:
        telegram_logging_bot.log(f'Marketplace with name: {marketplace_name.value} doesn`t exist')
        return

    for sale_offer_page in sale_offers_page_repository.get_all_by_marketplace_id(db=db, marketplace_id=marketplace.id):
        parsing_history: models.ParsingHistory = parsing_history_service.start_or_continue_parsing_history(
            db=db,
            sale_offers_page_id=sale_offer_page.id
        )

        page: int = parsing_history.ended_on_page_number

        with WebdriverManager() as webdriver:
            while True:
                if parsing_history.is_stopped:
                    break

                url: str = urljoin(marketplace.url, f'{sale_offer_page.path}')
                if page > 1:
                    url += f'?{sale_offer_page.query_params.format(page=page)}'

                telegram_logging_bot.log(f'{marketplace_name}: {url} parsing has been started ')

                webdriver.get(url)

                time.sleep(15)

                soup: BeautifulSoup = BeautifulSoup(webdriver.page_source, 'html.parser')
                search_results_div: Optional[Tag] = soup.find('div', {'data-testid': 'search-results'})
                if search_results_div is None:
                    time.sleep(60)
                    webdriver.refresh()
                    soup: BeautifulSoup = BeautifulSoup(webdriver.page_source, 'html.parser')
                    search_results_div: Optional[Tag] = soup.find('div', {'data-testid': 'search-results'})

                car_descriptions: list[Tag] = search_results_div.find_all('section')

                pagination_ul: Optional[list] = soup.find_all('ul', {'class': 'pagination-list'})
                last_page: int = int(pagination_ul[0].find_all('li')[-2].text) if pagination_ul else 1

                if not car_descriptions or page > last_page:
                    parsing_history_repository.set_is_stopped(
                        db=db,
                        db_parsing_history=parsing_history,
                        is_stopped=True
                    )
                    break

                for car_description in car_descriptions:
                    h1_tag: Optional[Tag] = car_description.find('h1')
                    if h1_tag is None:
                        continue

                    head_div: Tag = h1_tag.parent

                    a_tag: Tag = head_div.find('a')
                    path: str = a_tag['href']

                    url: str | None = None
                    if path is not None:
                        url = urljoin(marketplace.url, path)

                    a_tag_text_list: list = list(
                        map(
                            lambda string: string.lower(),
                            a_tag.text.split()
                        )
                    )

                    _, model, *_ = a_tag_text_list

                    if model == 'klasa' or model == 'amg' or model == 'seria':
                        model = a_tag_text_list[2]

                    engine_volume_with_units: re.Match | None = re.search(r'\d* \d+ cm', head_div.find('p').text)
                    if engine_volume_with_units is None:
                        continue

                    engine_volume: int = int(engine_volume_with_units.group().split('cm')[0].replace(' ', ''))

                    milage_dd = car_description.find(
                        'dd', {'data-parameter': 'mileage'}
                    )
                    if milage_dd is None:
                        continue

                    mileage: int = int(
                        milage_dd.text.split('km')[0].replace(' ', '')
                    )

                    production_year: int = int(
                        car_description.find(
                            'dd', {'data-parameter': 'year'}
                        ).text.split('km')[0].replace(' ', '')
                    )

                    price_in_pln: int = int(car_description.find('h3').text.replace(' ', ''))

                    fuel_type_name: value_objects.FuelTypeName = fuel_type_service.get_fuel_type_by_pl_name(
                        car_description.find('dd', {
                            'data-parameter': 'fuel_type'}).text.strip().lower()
                    )

                    sale_offer_service.create_sale_offer(
                        db=db,
                        fuel_type_name=fuel_type_name,
                        brand=sale_offer_page.brand,
                        model=model,
                        production_year=int(production_year),
                        engine_volume=engine_volume,
                        mileage=mileage,
                        price=currency_service.convert_pln_to_usd(pln_amount=price_in_pln),
                        marketplace_id=marketplace.id,
                        url=url
                    )

                page += 1
                parsing_history_repository.set_ended_on_page_number(
                    db=db,
                    db_parsing_history=parsing_history,
                    ended_on_page_number=page
                )
