from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import parsing_history_repository


def start_or_continue_parsing_history(db: Session, sale_offers_page_id: int) -> models.ParsingHistory:
    """Get parsing history for sale offers page or create and set is stopped False"""
    parsing_history: models.ParsingHistory | None = parsing_history_repository.get_actual_by_sale_offers_page_id(
        db=db,
        sale_offers_page_id=sale_offers_page_id
    )

    if parsing_history is None:
        parsing_history = parsing_history_repository.create(
            db=db,
            parsing_history=schemas.ParsingHistoryCreate(sale_offers_page_id=sale_offers_page_id)
        )
    else:
        parsing_history_repository.set_is_stopped(db=db, db_parsing_history=parsing_history, is_stopped=False)

    return parsing_history
