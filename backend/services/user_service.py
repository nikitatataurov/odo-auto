from fastapi import Depends
from jose import JWTError
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import user_repository, role_repository
from database.session import get_db
from exceptions import CREDENTIALS_EXCEPTION
from . import auth_service
from . import oauth2_scheme


async def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)) -> models.User:
    """Get current user token if valid"""
    if auth_service.is_token_removed(token=token, db=db):
        raise CREDENTIALS_EXCEPTION

    try:
        payload: dict = auth_service.decode_token(token=token)

        username: str = payload.get("sub")
        if username is None:
            raise CREDENTIALS_EXCEPTION
    except JWTError:
        raise CREDENTIALS_EXCEPTION

    db_user: models.User = user_repository.get_by_username(db=db, username=username)
    if db_user is None:
        raise CREDENTIALS_EXCEPTION

    return db_user


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
    """Hash password, add common user role and create user model"""
    user.password = auth_service.get_password_hash(user.password)
    roles: list[models.Role] = [role_repository.get_user(db=db)]

    return user_repository.create(db=db, user=user, roles=roles)
