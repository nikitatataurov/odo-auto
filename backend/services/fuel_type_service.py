from value_objects import FuelTypeName


FUEL_TYPES_RU = {
    'бензин': FuelTypeName.GASOLINE,
    'газ-бензин': FuelTypeName.GASOLINE,
    'газ-бензиновый': FuelTypeName.GASOLINE,
    'бензиновый': FuelTypeName.GASOLINE,
    'гибрид': FuelTypeName.HYBRID,
    'гибридный': FuelTypeName.HYBRID,
    'газ-гибрид': FuelTypeName.HYBRID,
    'электро': FuelTypeName.ELECTRIC,
    'дизель': FuelTypeName.DIESEL,
    'газ-дизель': FuelTypeName.DIESEL,
    'СУГ': FuelTypeName.DIESEL,
    'водородный': FuelTypeName.HYDROGEN,
}

FUEL_TYPES_PL = {
    'benzyna': FuelTypeName.GASOLINE,
    'benzyna+cng': FuelTypeName.GASOLINE,
    'benzyna+lpg': FuelTypeName.GASOLINE,
    'petrol': FuelTypeName.GASOLINE,
    'hybryda': FuelTypeName.HYBRID,
    'hybryda plug-in': FuelTypeName.HYBRID,
    'elektryczny': FuelTypeName.ELECTRIC,
    'diesel': FuelTypeName.DIESEL,
    'wodór': FuelTypeName.HYDROGEN,
}


def get_fuel_type_by_ru_name(name: str) -> FuelTypeName:
    """Get FuelTypeName by Russian name"""
    return FUEL_TYPES_RU[name]


def get_fuel_type_by_pl_name(name: str) -> FuelTypeName:
    """Get FuelTypeName by Polish name"""
    return FUEL_TYPES_PL[name]
