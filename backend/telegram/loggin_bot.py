import telebot

from config import settings


class TelegramLoggingBot:
    """Telegram bot for logging"""

    def __init__(self):
        self.bot: telebot.TeleBot = telebot.TeleBot(settings.TELEGRAM_BOT_TOKEN)

    def log(self, message: str):
        self.bot.send_message(settings.TELEGRAM_CHANNEL_CHAT_ID, message)
