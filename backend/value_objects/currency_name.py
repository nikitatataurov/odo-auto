from enum import Enum


class CurrencyName(str, Enum):
    """Currency name by ISO-4217"""
    KZT: str = 'KZT'
    RUB: str = 'RUB'
    USD: str = 'USD'
    PLN: str = 'PLN'
