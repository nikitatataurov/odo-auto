from enum import Enum


class SeleniumConnection(str, Enum):
    """Selenium connections"""
    HUB: str = 'http://scrapper_selenium_hub:4444/wd/hub'
