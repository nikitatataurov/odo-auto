from enum import Enum


class CountryName(str, Enum):
    """Name of country"""
    KZ: str = 'Kazakhstan'
    BY: str = 'Belarus'
    PL: str = 'Poland'
