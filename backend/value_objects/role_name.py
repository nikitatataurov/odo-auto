from enum import Enum


class RoleName(str, Enum):
    """Role name"""
    USER: str = 'user'
    ADMIN: str = 'admin'
    TEST: str = 'test'
