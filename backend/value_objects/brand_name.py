from enum import Enum


class BrandName(str, Enum):
    """Name of country"""
    MERCEDES: str = 'mercedes'
    BMW: str = 'bmw'
    AUDI: str = 'audi'
    VOLKSWAGEN: str = 'volkswagen'
    RENAULT: str = 'renault'
    OPEL: str = 'opel'
    KIA: str = 'kia'
    HYUNDAI: str = 'hyundai'
    TOYOTA: str = 'toyota'
    MAZDA: str = 'mazda'
    HONDA: str = 'honda'
    NISSAN: str = 'nissan'
    LEXUS: str = 'lexus'
    SKODA: str = 'skoda'
    FORD: str = 'ford'
    TESLA: str = 'tesla'
