from enum import Enum


class FuelTypeName(str, Enum):
    """Fuel type name of vehicle"""
    GASOLINE: str = 'gasoline'
    DIESEL: str = 'diesel'
    HYBRID: str = 'hybrid'
    ELECTRIC: str = 'electric'
    HYDROGEN: str = 'hydrogen'
