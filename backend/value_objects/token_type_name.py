from enum import Enum


class TokenTypeName(str, Enum):
    """Token type name"""
    ACCESS: str = 'access'
    REFRESH: str = 'refresh'
