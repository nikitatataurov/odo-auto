from enum import Enum


class CbrCurrencyUrl(str, Enum):
    """Url to get currencies from cbr"""
    DAILY: str = 'https://www.cbr.ru/scripts/XML_daily.asp'
