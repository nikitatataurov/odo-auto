from enum import Enum


class MarketplaceName(str, Enum):
    """Name of marketplace"""
    AUTOBY_BY: str = 'autoby.by'
    AUTOBUY_BY: str = 'autobuy.by'
    ASTER_KZ: str = 'aster.kz'
    KOLESA_KZ: str = 'kolesa.kz'
    OTOMOTO_PL: str = 'otomoto.pl'
