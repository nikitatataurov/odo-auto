import datetime

from fastapi import Depends, APIRouter, responses, Request
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import user_repository
from database.session import get_db
from exceptions import CREDENTIALS_EXCEPTION
from factories import response_factory
from services import auth_service, user_service

router: APIRouter = APIRouter()


@router.post('/sign-up')
async def sign_up(
        user: schemas.UserCreate,
        db: Session = Depends(get_db)
) -> responses.JSONResponse:
    db_user: models.User = user_repository.get_by_username(db=db, username=user.username)
    if db_user is not None:
        return response_factory.make_result_false()

    _: models.User = user_service.create_user(db=db, user=user)

    return response_factory.make_result_true()


@router.post('/sign-in')
async def sign_in(
        user: schemas.UserSignIn,
        db: Session = Depends(get_db),
) -> schemas.TokenResponse:
    user = auth_service.authenticate_user(db, user.username, user.password)

    if user is None:
        raise CREDENTIALS_EXCEPTION

    return await response_factory.make_token_response(db=db, username=user.username, user_id=user.id)


@router.post('/sign-out')
async def sign_out(
        token: str = Depends(auth_service.get_user_token),
        db: Session = Depends(get_db),
) -> responses.JSONResponse:
    if auth_service.remove_token(token=token, db=db):
        return response_factory.make_result_true()

    raise CREDENTIALS_EXCEPTION


@router.post('/refresh-token')
async def refresh_token(
        request: Request,
        db: Session = Depends(get_db),
) -> schemas.TokenResponse:
    form: dict = await request.json()
    if form.get('grant_type') != 'refresh_token':
        raise CREDENTIALS_EXCEPTION

    token: str = form.get('refresh_token')
    payload: dict = auth_service.decode_token(token=token)

    if datetime.datetime.utcfromtimestamp(payload.get('exp')) <= datetime.datetime.utcnow():
        raise CREDENTIALS_EXCEPTION

    db_user: models.User = user_repository.get_by_username(db=db, username=payload.get('sub'))
    if db_user is not None:
        return await response_factory.make_token_response(db=db, username=db_user.username, user_id=db_user.id)

    raise CREDENTIALS_EXCEPTION
