from fastapi import APIRouter


from . import (
    auth_router,
    role_router,
)


router = APIRouter(prefix='/auth', tags=['auth'])


router.include_router(auth_router.router)
router.include_router(role_router.router, prefix='/roles', tags=['roles'])
