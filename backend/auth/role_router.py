from fastapi import APIRouter, Depends, responses

from factories import response_factory
from services import role_service

router = APIRouter()


@router.post('/is-admin')
async def is_admin(
        user_is_admin: bool = Depends(role_service.is_admin)
) -> responses.JSONResponse:
    return response_factory.make_result_true() if user_is_admin else response_factory.make_result_false()


@router.post('/is-user')
async def is_user(
        user_is_user: bool = Depends(role_service.is_user)
) -> responses.JSONResponse:
    return response_factory.make_result_true() if user_is_user else response_factory.make_result_false()
