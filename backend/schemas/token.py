from datetime import datetime

from pydantic import BaseModel

from .user import User
from value_objects import TokenTypeName


class TokenBase(BaseModel):
    """Base token pydantic-model"""
    hash: str
    type: TokenTypeName
    user_id: int
    removed_at: datetime | None = None


class TokenCreate(TokenBase):
    """Token-creation pydantic-model"""
    pass


class Token(TokenBase):
    """Token pydantic-model"""
    id: int
    user: User
    created_at: datetime

    class ConfigDict:
        from_attributes = True
