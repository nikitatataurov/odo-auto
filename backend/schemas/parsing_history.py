from datetime import datetime

from pydantic import BaseModel

from .sale_offers_page import SaleOffersPage


class ParsingHistoryBase(BaseModel):
    """Base parsing history pydantic-model"""
    sale_offers_page_id: int
    ended_on_page_number: int = 1
    is_stopped: bool = False


class ParsingHistoryCreate(ParsingHistoryBase):
    """Parsing history-creation pydantic-model"""
    pass


class ParsingHistory(ParsingHistoryBase):
    """Parsing history pydantic-model"""
    id: int
    sale_offers_page: SaleOffersPage
    created_at: datetime
    updated_at: datetime

    class ConfigDict:
        from_attributes = True
