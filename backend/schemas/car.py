from datetime import datetime

from pydantic import BaseModel

from value_objects import BrandName
from .fuel_type import FuelType


class CarBase(BaseModel):
    """Base car pydantic-model"""
    brand: BrandName
    model: str
    production_year: int
    engine_volume: int  # in cm3
    fuel_type_id: int


class CarCreate(CarBase):
    """Car-creation pydantic-model"""
    pass


class Car(CarBase):
    """Car pydantic-model"""
    id: int
    fuel_type: FuelType
    created_at: datetime

    class ConfigDict:
        from_attributes = True
