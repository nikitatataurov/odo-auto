from pydantic import BaseModel


class ParsingDescription(BaseModel):
    """Parsing description pydantic-model"""
    marketplace_name: str
    start_parsing_route: str
    stop_parsing_route: str
    is_running_parsing_route: str
