from datetime import datetime

from pydantic import BaseModel

from .marketplace import Marketplace
from .car import Car


class SaleOfferBase(BaseModel):
    """Base sale offer pydantic-model"""
    mileage: int
    price: int
    car_id: int
    marketplace_id: int
    created_at: datetime
    url: str | None = None


class SaleOfferCreate(SaleOfferBase):
    """Creation-sale offer pydantic-model"""
    pass


class SaleOffer(SaleOfferBase):
    """Sale offer pydantic-model"""
    id: int
    marketplace: Marketplace
    car: Car

    class ConfigDict:
        from_attributes = True
