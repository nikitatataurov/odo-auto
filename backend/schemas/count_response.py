from pydantic import BaseModel


class CountResponse(BaseModel):
    """Count response pydantic-model"""
    count: int
