from pydantic import BaseModel

from value_objects import CountryName


class CountryBase(BaseModel):
    """Base country pydantic-model"""
    code: str
    name: CountryName


class CountryCreate(CountryBase):
    """Country-creation pydantic-model"""
    pass


class Country(CountryBase):
    """Country pydantic-model"""
    id: int

    class ConfigDict:
        from_attributes = True
