from pydantic import BaseModel

from value_objects import MarketplaceName
from .country import Country


class MarketplaceBase(BaseModel):
    """Base marketplace pydantic-model"""
    name: MarketplaceName
    country_id: int
    url: str


class MarketplaceCreate(MarketplaceBase):
    """Marketplace-creation pydantic-model"""
    pass


class Marketplace(MarketplaceBase):
    """Marketplace pydantic-model"""
    id: int
    country: Country

    class ConfigDict:
        from_attributes = True
