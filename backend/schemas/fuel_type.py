from pydantic import BaseModel

from value_objects import FuelTypeName


class FuelTypeBase(BaseModel):
    """Base fuel type pydantic-model"""
    name: FuelTypeName


class FuelTypeCreate(FuelTypeBase):
    """Fuel type-creation pydantic-model"""
    pass


class FuelType(FuelTypeBase):
    """Fuel type pydantic-model"""
    id: int

    class ConfigDict:
        from_attributes = True
