from pydantic import BaseModel


class TokenResponse(BaseModel):
    """Token response pydantic-model"""
    access_token: str
    refresh_token: str
    is_admin: bool
