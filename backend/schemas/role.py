from pydantic import BaseModel

from value_objects import RoleName


class RoleBase(BaseModel):
    """Base role pydantic-model"""
    name: RoleName


class RoleCreate(RoleBase):
    """Role-creation pydantic-model"""
    pass


class Role(RoleBase):
    """Role pydantic-model"""
    id: int
