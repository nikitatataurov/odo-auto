from datetime import datetime

from fastapi import Form
from pydantic import BaseModel

from schemas import role


class UserBase(BaseModel):
    """Base user pydantic-model"""
    username: str


class UserCreate(UserBase):
    """User-creation pydantic-model"""
    password: str
    email: str

    @classmethod
    def as_form(
        cls,
        username: str = Form(),
        email: str = Form(),
        password: str = Form(),
    ) -> 'UserCreate':
        return cls(
            username=username,
            email=email,
            password=password
        )


class UserSignIn(UserBase):
    """User-sign in pydantic-model"""
    password: str


class User(UserBase):
    """User pydantic-mode"""
    id: int
    created_at: datetime
    email: str
    roles: list[role.Role]

    class ConfigDict:
        from_attributes = True
