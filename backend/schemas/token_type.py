from pydantic import BaseModel

from value_objects import TokenTypeName


class TokenType(BaseModel):
    """Token type pydantic-model"""
    name: TokenTypeName
