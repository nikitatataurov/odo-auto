from pydantic import BaseModel

from value_objects import BrandName
from .marketplace import Marketplace


class SaleOffersPageBase(BaseModel):
    """Base sale offers page pydantic-model"""
    marketplace_id: int
    brand: BrandName
    path: str
    query_params: str | None = None


class SaleOffersPageCreate(SaleOffersPageBase):
    """Sale offers page-creation pydantic-model"""
    pass


class SaleOffersPage(SaleOffersPageBase):
    """Sale offers page pydantic-model"""
    id: int
    marketplace: Marketplace

    class ConfigDict:
        from_attributes = True
