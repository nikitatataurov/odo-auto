from selenium import webdriver
from selenium.webdriver import Firefox, FirefoxOptions

import value_objects


class WebdriverManager:
    """Context manager for working with webdriver"""

    def __init__(self):
        self.webdriver = webdriver.Remote(
            value_objects.SeleniumConnection.HUB.value,
            options=self._get_options(),
        )

    def _get_options(self) -> FirefoxOptions:
        """Randomly preparing FirefoxOptions for webdriver"""
        options: FirefoxOptions = FirefoxOptions()
        # options.add_argument('excludeSwitches', ['enable-automation'])
        # options.add_argument('useAutomationExtension', False)
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--headless')
        options.add_argument('--disable-blink-features=AutomationControlled')

        return options

    def __enter__(self) -> Firefox:
        """Getting webdriver on enter"""
        return self.webdriver

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Exiting webdriver"""
        self.webdriver.quit()
