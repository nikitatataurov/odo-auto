from fastapi import APIRouter

from . import (
    country_router,
    car_router,
    fuel_type_router,
    marketplace_router,
    sale_offer_router,
    sale_offers_page_router,
    parsing_router,
)


router = APIRouter(prefix='/api/v1')


router.include_router(car_router.router, prefix='/cars', tags=['cars'])
router.include_router(country_router.router, prefix='/countries', tags=['countries'])
router.include_router(fuel_type_router.router, prefix='/fuel-types', tags=['fuel-types'])
router.include_router(marketplace_router.router, prefix='/marketplaces', tags=['marketplaces'])
router.include_router(parsing_router.router, prefix='/parsing', tags=['parsing'])
router.include_router(sale_offer_router.router, prefix='/sale-offers', tags=['sale-offers'])
router.include_router(sale_offers_page_router.router, prefix='/sale-offers-pages', tags=['sale-offers-pages'])
