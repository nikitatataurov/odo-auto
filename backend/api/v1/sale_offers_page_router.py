from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

import schemas
from database.repositories import marketplace_repository, sale_offers_page_repository
from database import models
from database.session import get_db
from services import auth_service


router: APIRouter = APIRouter()


@router.get('/')
def paginate_sale_offers_pages(
        params: Params = Depends(),
        db: Session = Depends(get_db),
) -> Page[schemas.SaleOffersPage]:
    return paginate(sale_offers_page_repository.get_base_query(db=db), params)


@router.get('/count/all/')
def read_sale_offers_pages_count(db: Session = Depends(get_db)) -> schemas.CountResponse:
    return schemas.CountResponse(count=sale_offers_page_repository.get_count_all(db=db))


@router.get('/{id}/')
def read_sale_offers_page_by_id(id: int, db: Session = Depends(get_db)) -> schemas.SaleOffersPage:
    db_sale_offers_page: models.SaleOffersPage | None = sale_offers_page_repository.get_by_id(db=db, id=id)
    if db_sale_offers_page is None:
        raise HTTPException(status_code=404, detail='Sale offers page not found')

    return db_sale_offers_page


@router.post('/', status_code=201)
def create_sale_offers_page(
        sale_offers_page: schemas.SaleOffersPageCreate,
        _: str = Depends(auth_service.get_admin_token),
        db: Session = Depends(get_db),
) -> schemas.SaleOffersPage:
    db_sale_offers_page: models.SaleOffersPage | None = sale_offers_page_repository.get_by_marketplace_id_and_brand(
        db=db,
        marketplace_id=sale_offers_page.marketplace_id,
        brand=sale_offers_page.brand
    )
    if db_sale_offers_page is not None:
        raise HTTPException(status_code=400, detail='Sale offers page exists')

    db_marketplace: models.Marketplace | None = marketplace_repository.get_by_id(
        db=db, id=sale_offers_page.marketplace_id
    )
    if db_marketplace is None:
        raise HTTPException(status_code=404, detail='Marketplace not found')

    return sale_offers_page_repository.create(db=db, sale_offers_page=sale_offers_page)
