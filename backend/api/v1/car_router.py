from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import car_repository
from database.session import get_db
from services import auth_service


router: APIRouter = APIRouter()


@router.get('/')
async def paginate_cars(params: Params = Depends(), db: Session = Depends(get_db)) -> Page[schemas.Car]:
    return paginate(car_repository.get_base_query(db=db), params)


@router.get('/count/all/')
def read_all_cars_count(db: Session = Depends(get_db)) -> schemas.CountResponse:
    return schemas.CountResponse(count=car_repository.get_count_all(db=db))


@router.get('/{id}/')
def read_car_by_id(id: int, db: Session = Depends(get_db)) -> schemas.Car:
    db_car: models.Car | None = car_repository.get_by_id(db=db, id=id)
    if db_car is None:
        raise HTTPException(status_code=404, detail='Car not found')

    return db_car


@router.post('/', status_code=201)
def create_car(
        car: schemas.CarCreate,
        _: str = Depends(auth_service.get_admin_token),
        db: Session = Depends(get_db),
) -> schemas.Car:
    try:
        db_car: models.Car | None = car_repository.get(db=db, **car.model_dump())
    except Exception as exception:
        raise HTTPException(status_code=500, detail=exception)

    if db_car is not None:
        raise HTTPException(status_code=400, detail='Car exists')

    return car_repository.create(db=db, car=car)
