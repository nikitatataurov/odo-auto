from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

import schemas
from database.repositories import country_repository
from database import models
from database.session import get_db
from services import auth_service


router: APIRouter = APIRouter()


@router.get('/all/')
async def read_all_countries(db: Session = Depends(get_db)) -> list[schemas.Country]:
    return country_repository.get_all(db=db)


@router.get('/count/all/')
def read_all_countries_count(db: Session = Depends(get_db)) -> schemas.CountResponse:
    return schemas.CountResponse(count=country_repository.get_count_all(db=db))


@router.get(path='/')
def paginate_countries(params: Params = Depends(), db: Session = Depends(get_db)) -> Page[schemas.Country]:
    return paginate(country_repository.get_base_query(db=db), params)


@router.get(path='/{id}/')
def read_country_by_id(id: int, db: Session = Depends(get_db)) -> schemas.Country:
    db_country: models.Country | None = country_repository.get_by_id(db=db, id=id)
    if db_country is None:
        raise HTTPException(status_code=404, detail='Country not found')

    return db_country


@router.post(path='/', status_code=201)
def create_country(
        country: schemas.CountryCreate,
        _: str = Depends(auth_service.get_admin_token),
        db: Session = Depends(get_db),
) -> schemas.Country:
    db_country: models.Country | None = country_repository.get_by_code(db=db, code=country.code)
    if db_country is not None:
        raise HTTPException(status_code=400, detail='Country exists')

    return country_repository.create(db=db, country=country)
