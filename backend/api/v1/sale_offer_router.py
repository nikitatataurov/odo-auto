from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import sale_offer_repository
from database.session import get_db
from services import auth_service


router: APIRouter = APIRouter()


@router.get('/')
def paginate_sale_offers(params: Params = Depends(), db: Session = Depends(get_db)) -> Page[schemas.SaleOffer]:
    return paginate(sale_offer_repository.get_base_query(db=db), params)


@router.get('/all/by-marketplace/{marketplace_name}/actual/')
def paginate_sale_offers_by_marketplace_actual(
        marketplace_name: value_objects.MarketplaceName,
        params: Params = Depends(),
        db: Session = Depends(get_db),
) -> Page[schemas.SaleOffer]:
    return paginate(
        sale_offer_repository.get_query_by_marketplace_for_current_month(db=db, marketplace_name=marketplace_name),
        params
    )


@router.get('/count/all/')
def read_all_sale_offers_count(db: Session = Depends(get_db)) -> schemas.CountResponse:
    return schemas.CountResponse(count=sale_offer_repository.get_count_all(db=db))


@router.get('/{id}/')
def read_sale_offer_by_id(id: int, db: Session = Depends(get_db)) -> schemas.SaleOffer:
    db_sale_offer: models.SaleOffer | None = sale_offer_repository.get_by_id(db=db, id=id)
    if db_sale_offer is None:
        raise HTTPException(status_code=404, detail='Sale offer not found')

    return db_sale_offer


@router.post('/', status_code=201)
def create_sale_offer(
        sale_offer: schemas.SaleOfferCreate,
        _: str = Depends(auth_service.get_admin_token),
        db: Session = Depends(get_db),
) -> schemas.SaleOffer:
    db_sale_offer: models.SaleOffer | None = sale_offer_repository.get(db=db, **sale_offer.model_dump())
    if db_sale_offer is not None:
        raise HTTPException(status_code=400, detail='Sale offer exists')

    return sale_offer_repository.create(db=db, sale_offer=sale_offer)
