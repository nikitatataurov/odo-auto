from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

import schemas
from database.repositories import marketplace_repository, country_repository
from database import models
from database.session import get_db
from services import auth_service


router: APIRouter = APIRouter()


@router.get('/all/')
def read_all_marketplaces(db: Session = Depends(get_db)) -> list[schemas.Marketplace]:
    return marketplace_repository.get_all(db=db)


@router.get('/')
def paginate_marketplaces(params: Params = Depends(), db: Session = Depends(get_db)) -> Page[schemas.Marketplace]:
    return paginate(marketplace_repository.get_base_query(db=db), params)


@router.get('/count/all/')
def read_all_marketplaces_count(db: Session = Depends(get_db)) -> schemas.CountResponse:
    return schemas.CountResponse(count=marketplace_repository.get_count_all(db=db))


@router.get('/{id}/')
def read_marketplace_by_id(id: int, db: Session = Depends(get_db)) -> schemas.Marketplace:
    marketplace: models.Marketplace = marketplace_repository.get_by_id(db=db, id=id)
    if marketplace is None:
        raise HTTPException(status_code=404, detail='Marketplace not found')

    return marketplace


@router.post('/', status_code=201)
def create_marketplace(
        marketplace: schemas.MarketplaceCreate,
        _: str = Depends(auth_service.get_admin_token),
        db: Session = Depends(get_db),
) -> schemas.Marketplace:
    db_marketplace: models.Marketplace = marketplace_repository.get_by_name(db=db, name=marketplace.name)
    if db_marketplace is not None:
        raise HTTPException(status_code=400, detail='Marketplace exists')

    db_country: models.Country = country_repository.get_by_id(db=db, id=marketplace.country_id)
    if db_country is None:
        raise HTTPException(status_code=404, detail='Country not found')

    return marketplace_repository.create(db=db, marketplace=marketplace)
