from fastapi import APIRouter, Depends, responses
from sqlalchemy.orm import Session
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate

import schemas
import value_objects
from database.repositories import parsing_history_repository
from database.session import get_db
from factories import response_factory
from services import parsing_service, auth_service
import tasks
from telegram.loggin_bot import TelegramLoggingBot

router: APIRouter = APIRouter(dependencies=[Depends(auth_service.get_admin_token)])


@router.get('/history')
def paginate_parsing_history(params: Params = Depends(), db: Session = Depends(get_db)) -> Page[schemas.ParsingHistory]:
    return paginate(parsing_history_repository.get_base_query(db=db), params)


@router.post('/{marketplace_name}/start/')
async def parse_marketplace(marketplace_name: value_objects.MarketplaceName) -> responses.JSONResponse:
    tasks.parse_marketplace.delay(marketplace_name=marketplace_name)

    return response_factory.make_result_true()


@router.post('/{marketplace_name}/stop/')
async def stop_parsing_marketplace(
        marketplace_name: value_objects.MarketplaceName,
        db: Session = Depends(get_db),
        telegram_logging_bot: TelegramLoggingBot = Depends(TelegramLoggingBot),
) -> responses.JSONResponse:
    return response_factory.make_result_bool(
        bool_value=parsing_service.stop_parsing_marketplace(
            db=db,
            telegram_logging_bot=telegram_logging_bot,
            marketplace_name=marketplace_name
        )
    )


@router.get('/{marketplace_name}/is-running/')
async def check_marketplace_parsing_is_running(
        marketplace_name: value_objects.MarketplaceName,
        db: Session = Depends(get_db),
        telegram_logging_bot: TelegramLoggingBot = Depends(TelegramLoggingBot)
) -> responses.JSONResponse:
    return response_factory.make_result_bool(
        bool_value=parsing_service.check_parsing_is_running(
            db=db,
            telegram_logging_bot=telegram_logging_bot,
            marketplace_name=marketplace_name
        )
    )


@router.get('/routes/info')
async def get_routes_info(db: Session = Depends(get_db)) -> list[schemas.ParsingDescription]:
    return parsing_service.get_parsing_description_list(db=db)
