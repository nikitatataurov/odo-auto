from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import fuel_type_repository
from database.session import get_db
from services import auth_service


router: APIRouter = APIRouter()


@router.get('/all/')
def read_all_fuel_types(db: Session = Depends(get_db)) -> list[schemas.FuelType]:
    return fuel_type_repository.get_all(db=db)


@router.get('/')
def paginate_fuel_types(params: Params = Depends(), db: Session = Depends(get_db)) -> Page[schemas.FuelType]:
    return paginate(fuel_type_repository.get_base_query(db=db), params)


@router.get('/count/all/')
def read_all_fuel_types_count(db: Session = Depends(get_db)) -> schemas.CountResponse:
    return schemas.CountResponse(count=fuel_type_repository.get_count_all(db=db))


@router.get('/{id}/')
def read_fuel_type_by_id(id: int, db: Session = Depends(get_db)) -> schemas.FuelType:
    db_fuel_type: models.FuelType = fuel_type_repository.get_by_id(db=db, id=id)
    if db_fuel_type is None:
        raise HTTPException(status_code=404, detail='Fuel type not found')

    return db_fuel_type


@router.post('/', status_code=201)
def create_fuel_type(
        fuel_type: schemas.FuelTypeCreate,
        _: str = Depends(auth_service.get_admin_token),
        db: Session = Depends(get_db),
) -> schemas.FuelType:
    db_fuel_type: models.FuelType = fuel_type_repository.get_by_name(db=db, name=fuel_type.name)
    if db_fuel_type is not None:
        raise HTTPException(status_code=400, detail='Fuel type exists')

    return fuel_type_repository.create(db=db, fuel_type=fuel_type)
