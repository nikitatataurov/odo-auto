"""Init

Revision ID: insert_data
Revises:
Create Date: 2024-02-26 18:44:39.615168

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa

from value_objects import BrandName, FuelTypeName, RoleName, MarketplaceName, CountryName


# revision identifiers, used by Alembic.
revision: str = 'insert_data'
down_revision: Union[str, None] = '25e5b0b9a24f'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    """Insert initial data to work with scrapper"""
    for id, fuel_type_name in enumerate(FuelTypeName, 1):
        op.execute(f"INSERT INTO fuel_type (id, name) VALUES ({id}, '{fuel_type_name.value}')")

    for id, role_name in enumerate(RoleName, 1):
        op.execute(f"INSERT INTO role (id, name) VALUES ({id}, '{role_name.value}')")

    by_id: int = 0
    kz_id: int = 1
    pl_id: int = 2
    op.execute(
        f"""
        INSERT INTO country (id, code, name) VALUES
        ({by_id}, '{CountryName.BY.name}', '{CountryName.BY.value}'),
        ({kz_id}, '{CountryName.KZ.name}', '{CountryName.KZ.value}'),
        ({pl_id}, '{CountryName.PL.name}', '{CountryName.PL.value}')
        """
    )

    autoby_by_id: int = 0
    autobuy_by_id: int = 1
    aster_kz_id: int = 2
    kolesa_kz_id: int = 3
    otomoto_pl_id: int = 4

    op.execute(
        f"""
        INSERT INTO marketplace (id, country_id, name, url) VALUES
        ({autoby_by_id}, {by_id}, '{MarketplaceName.AUTOBY_BY.value}', 'https://{MarketplaceName.AUTOBY_BY.value}'),
        ({autobuy_by_id}, {by_id}, '{MarketplaceName.AUTOBUY_BY.value}', 'https://{MarketplaceName.AUTOBUY_BY.value}'),
        ({aster_kz_id}, {kz_id}, '{MarketplaceName.ASTER_KZ.value}', 'https://{MarketplaceName.ASTER_KZ.value}'),
        ({kolesa_kz_id}, {kz_id}, '{MarketplaceName.KOLESA_KZ.value}', 'https://{MarketplaceName.KOLESA_KZ.value}'),
        ({otomoto_pl_id}, {pl_id}, '{MarketplaceName.OTOMOTO_PL.value}', 'https://{MarketplaceName.OTOMOTO_PL.value}')
        """
    )

    op.execute(
        f"""
        INSERT INTO sale_offers_page (marketplace_id, brand, path, query_params) VALUES
        ({aster_kz_id}, '{BrandName.MERCEDES.value}', '/cars/mercedes', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.BMW.value}', '/cars/bmw', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.AUDI.value}', '/cars/audi', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.VOLKSWAGEN.value}', '/cars/volkswagen', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.RENAULT.value}', '/cars/renault', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.OPEL.value}', '/cars/opel', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.KIA.value}', '/cars/kia', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.HYUNDAI.value}', '/cars/hyundai', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.TOYOTA.value}', '/cars/toyota', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.MAZDA.value}', '/cars/mazda', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.HONDA.value}', '/cars/honda', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.NISSAN.value}', '/cars/nissan', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.LEXUS.value}', '/cars/lexus', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.SKODA.value}', '/cars/skoda', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.FORD.value}', '/cars/ford', 'orderBy=UpdatedAsc&page={{page}}'),
        ({aster_kz_id}, '{BrandName.TESLA.value}', '/cars/tesla', 'orderBy=UpdatedAsc&page={{page}}')
        """
    )

    op.execute(
        f"""
        INSERT INTO sale_offers_page (marketplace_id, brand, path, query_params) VALUES 
        ({otomoto_pl_id}, '{BrandName.MERCEDES.value}', '/osobowe/mercedes-benz', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.BMW.value}', '/osobowe/bmw', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.AUDI.value}', '/osobowe/audi', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.VOLKSWAGEN.value}', '/osobowe/volkswagen', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.RENAULT.value}', '/osobowe/renault', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.OPEL.value}', '/osobowe/opel', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.KIA.value}', '/osobowe/kia', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.HYUNDAI.value}', '/osobowe/hyundai', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.TOYOTA.value}', '/osobowe/toyota', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.MAZDA.value}', '/osobowe/mazda', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.HONDA.value}', '/osobowe/honda', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.NISSAN.value}', '/osobowe/nissan', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.LEXUS.value}', '/osobowe/lexus', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.SKODA.value}', '/osobowe/skoda', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.FORD.value}', '/osobowe/ford', 'page={{page}}'),
        ({otomoto_pl_id}, '{BrandName.TESLA.value}', '/osobowe/tesla', 'page={{page}}')
        """
    )

    op.execute(
        f"""
        INSERT INTO sale_offers_page (marketplace_id, brand, path, query_params) VALUES 
        ({autoby_by_id}, '{BrandName.MERCEDES.value}', '/cars/mercedes/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.BMW.value}', '/cars/bmw/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.AUDI.value}', '/cars/audi/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.VOLKSWAGEN.value}', '/cars/volkswagen/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.RENAULT.value}', '/cars/renault/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.OPEL.value}', '/cars/opel/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.KIA.value}', '/cars/kia/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.HYUNDAI.value}', '/cars/hyundai/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.TOYOTA.value}', '/cars/toyota/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.MAZDA.value}', '/cars/mazda/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.HONDA.value}', '/cars/honda/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.NISSAN.value}', '/cars/nissan/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.LEXUS.value}', '/cars/lexus/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.SKODA.value}', '/cars/skoda/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.FORD.value}', '/cars/ford/', 'page={{page}}'),
        ({autoby_by_id}, '{BrandName.TESLA.value}', '/cars/tesla/', 'page={{page}}')
        """
    )


def downgrade() -> None:
    pass