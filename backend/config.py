from passlib.context import CryptContext
from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """Scrapper-app settings"""
    model_config = SettingsConfigDict(env_file='.env')

    SECRET_KEY: str = Field('SECRET_KEY')
    CRYPTOGRAPHIC_ALGORITHM: str = Field('CRYPTOGRAPHIC_ALGORITHM')

    POSTGRES_USER: str = Field('POSTGRES_USER')
    POSTGRES_PASSWORD: str = Field('POSTGRES_PASSWORD')
    POSTGRES_DB: str = Field('POSTGRES_DB')
    POSTGRES_DB_TEST: str = Field('POSTGRES_DB_TEST')

    ACCESS_TOKEN_EXPIRE_MINUTES: int = 5
    REFRESH_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 30

    PWD_CONTEXT: CryptContext = CryptContext(schemes=['bcrypt'], deprecated='auto')

    TELEGRAM_BOT_TOKEN: str = Field('TELEGRAM_BOT_TOKEN')
    TELEGRAM_CHANNEL_CHAT_ID: str = Field('TELEGRAM_CHANNEL_CHAT_ID')

    CELERY_BROKER_URL: str = Field('CELERY_BROKER_URL')
    CELERY_RESULT_BACKEND: str = Field('CELERY_RESULT_BACKEND')


settings: Settings = Settings()
