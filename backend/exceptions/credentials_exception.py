from fastapi import exceptions, status

CREDENTIALS_EXCEPTION = exceptions.HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail='Could not validate credentials',
    headers={'WWW-Authenticate': 'Bearer'},
)
