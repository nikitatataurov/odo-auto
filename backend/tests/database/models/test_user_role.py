from sqlalchemy.orm import Session

from database import models


def test_create_user(db: Session, created_role_admin: models.Role) -> None:
    db_user: models.User = models.User(
        username='test',
        email='test@mail.ru',
        password='testhashofpassword',
        roles=[created_role_admin]
    )

    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    user_role_from_db: models.UserRole | None = db.query(
        models.UserRole
    ).filter(models.UserRole.user_id == db_user.id and models.UserRole.role_id == created_role_admin.id).first()

    assert user_role_from_db is not None
