from sqlalchemy.orm import Session

from database import models


def test_create_parsing_history(db: Session, created_sale_offers_page: models.SaleOffersPage) -> None:
    db_parsing_history: models.ParsingHistory = models.ParsingHistory(
        sale_offers_page_id=created_sale_offers_page.id,
        ended_on_page_number=0
    )

    db.add(db_parsing_history)
    db.commit()
    db.refresh(db_parsing_history)

    parsing_history_from_db: models.ParsingHistory | None = db.query(
        models.ParsingHistory
    ).filter(models.ParsingHistory.id == db_parsing_history.id).first()

    assert parsing_history_from_db is not None
    assert parsing_history_from_db is db_parsing_history
