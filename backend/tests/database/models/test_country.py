from sqlalchemy.orm import Session

from database import models


def test_create_country(db: Session) -> None:
    db_country: models.Country = models.Country(
        code='by',
        name='Belorussia'
    )

    db.add(db_country)
    db.commit()
    db.refresh(db_country)

    country_from_db: models.Country | None = db.query(
        models.Country
    ).filter(models.Country.id == db_country.id).first()

    assert country_from_db is not None
    assert country_from_db is db_country
