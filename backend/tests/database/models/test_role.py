from sqlalchemy.orm import Session

from database import models


def test_create_role(db: Session) -> None:
    db_role: models.Role = models.Role(
        name='test'
    )

    db.add(db_role)
    db.commit()
    db.refresh(db_role)

    role_from_db: models.Role | None = db.query(
        models.Role
    ).filter(models.Role.id == db_role.id).first()

    assert role_from_db is not None
    assert role_from_db is db_role
