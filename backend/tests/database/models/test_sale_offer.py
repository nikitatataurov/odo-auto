import datetime

from sqlalchemy.orm import Session

from database import models


def test_create_sale_offer(db: Session, created_car: models.Car, created_marketplace: models.Marketplace) -> None:
    db_sale_offer: models.SaleOffer = models.SaleOffer(
        mileage=0,
        price=200000,
        created_at=datetime.datetime.now(),
        car_id=created_car.id,
        marketplace_id=created_marketplace.id
    )

    db.add(db_sale_offer)
    db.commit()
    db.refresh(db_sale_offer)

    sale_offer_from_db: models.SaleOffer | None = db.query(
        models.SaleOffer
    ).filter(models.SaleOffer.id == db_sale_offer.id).first()

    assert sale_offer_from_db is not None
    assert sale_offer_from_db is db_sale_offer
