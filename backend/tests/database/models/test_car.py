from sqlalchemy.orm import Session

from database import models


def test_create_car(db: Session, created_fuel_type: models.FuelType) -> None:
    db_car: models.Car = models.Car(
        brand='mercedes',
        model='e500',
        engine_volume=5000,
        production_year=2023,
        fuel_type_id=created_fuel_type.id
    )

    db.add(db_car)
    db.commit()
    db.refresh(db_car)

    car_from_db: models.Car | None = db.query(
        models.Car
    ).filter(
        models.Car.id == db_car.id
    ).first()

    assert car_from_db is not None
    assert car_from_db is db_car
