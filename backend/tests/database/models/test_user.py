from sqlalchemy.orm import Session

from database import models


def test_create_user(db: Session) -> None:
    db_user: models.User = models.User(
        username='test',
        email='test@mail.ru',
        password='testhashofpassword',
        roles=[]
    )

    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    user_from_db: models.User | None = db.query(
        models.User
    ).filter(models.User.id == db_user.id).first()

    assert user_from_db is not None
    assert user_from_db is db_user
