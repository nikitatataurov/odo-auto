from sqlalchemy.orm import Session

from database import models


def test_create_sale_offers_page(db: Session, created_marketplace: models.Marketplace) -> None:
    db_sale_offers_page: models.SaleOffersPage = models.SaleOffersPage(
        marketplace_id=created_marketplace.id,
        brand='mercedes',
        path='/cars/mercedes'
    )

    db.add(db_sale_offers_page)
    db.commit()
    db.refresh(db_sale_offers_page)

    sale_offers_page_from_db: models.SaleOffersPage | None = db.query(
        models.SaleOffersPage
    ).filter(
        models.SaleOffersPage.id == db_sale_offers_page.id
    ).first()

    assert sale_offers_page_from_db is not None
    assert sale_offers_page_from_db is db_sale_offers_page
