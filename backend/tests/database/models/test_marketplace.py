from sqlalchemy.orm import Session

from database import models


def test_create_marketplace(db: Session, created_country: models.Country) -> None:
    db_marketplace: models.Marketplace = models.Marketplace(
        name='auto.pl',
        country_id=created_country.id,
        url='http://auto.pl'
    )

    db.add(db_marketplace)
    db.commit()
    db.refresh(db_marketplace)

    marketplace_from_db: models.Marketplace | None = db.query(
        models.Marketplace
    ).filter(models.Marketplace.id == db_marketplace.id).first()

    assert marketplace_from_db is not None
    assert marketplace_from_db is db_marketplace
