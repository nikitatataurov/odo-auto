from sqlalchemy.orm import Session

from database import models


def test_create_fuel_type(db: Session) -> None:
    db_fuel_type: models.FuelType = models.FuelType(
        name='gasoline'
    )

    db.add(db_fuel_type)
    db.commit()
    db.refresh(db_fuel_type)

    fuel_type_from_db: models.FuelType | None = db.query(
        models.FuelType
    ).filter(models.FuelType.id == db_fuel_type.id).first()

    assert fuel_type_from_db is not None
    assert fuel_type_from_db is db_fuel_type
