from sqlalchemy.orm import Session

from database import models


def test_create_token(db: Session, created_user: models.User) -> None:
    db_token: models.Token = models.Token(
        hash='testhash',
        type='access',
        user_id=created_user.id
    )

    db.add(db_token)
    db.commit()
    db.refresh(db_token)

    token_from_db: models.Token | None = db.query(
        models.Token
    ).filter(models.Token.id == db_token.id).first()

    assert token_from_db is not None
    assert token_from_db is db_token
