import datetime

import pytest
from sqlalchemy.orm import Session

import value_objects
from database import models


@pytest.fixture(scope='function')
def created_country(db: Session) -> models.Country:
    country: value_objects.CountryName = value_objects.CountryName.BY
    db_country: models.Country = models.Country(
        code=country.name,
        name=country.value
    )

    db.add(db_country)
    db.commit()
    db.refresh(db_country)

    return db_country


@pytest.fixture(scope='function')
def created_fuel_type(db: Session) -> models.FuelType:
    db_fuel_type: models.FuelType = models.FuelType(
        name=value_objects.FuelTypeName.GASOLINE
    )

    db.add(db_fuel_type)
    db.commit()
    db.refresh(db_fuel_type)

    return db_fuel_type


@pytest.fixture(scope='function')
def created_marketplace(db: Session, created_country: models.Country) -> models.Marketplace:
    db_marketplace: models.Marketplace = models.Marketplace(
        name=value_objects.MarketplaceName.AUTOBY_BY,
        country_id=created_country.id,
        url='http://auto.by'
    )

    db.add(db_marketplace)
    db.commit()
    db.refresh(db_marketplace)

    return db_marketplace


@pytest.fixture(scope='function')
def created_car(db: Session, created_fuel_type: models.FuelType) -> models.Car:
    db_car: models.Car = models.Car(
        brand=value_objects.BrandName.MERCEDES,
        model='e500',
        engine_volume=5000,
        production_year=2023,
        fuel_type_id=created_fuel_type.id
    )

    db.add(db_car)
    db.commit()
    db.refresh(db_car)

    return db_car


@pytest.fixture(scope='function')
def created_sale_offer(
        db: Session,
        created_car: models.Car,
        created_marketplace: models.Marketplace,
) -> models.SaleOffer:
    db_sale_offer: models.SaleOffer = models.SaleOffer(
        mileage=0,
        price=200000,
        created_at=datetime.datetime.now(),
        car_id=created_car.id,
        marketplace_id=created_marketplace.id,
    )

    db.add(db_sale_offer)
    db.commit()
    db.refresh(db_sale_offer)

    return db_sale_offer


@pytest.fixture(scope='function')
def created_sale_offers_page(
        db: Session,
        created_marketplace: models.Marketplace,
) -> models.SaleOffersPage:
    db_sale_offers_page: models.SaleOffersPage = models.SaleOffersPage(
        marketplace_id=created_marketplace.id,
        brand=value_objects.BrandName.MERCEDES,
        path='/cars/mercedes'
    )

    db.add(db_sale_offers_page)
    db.commit()
    db.refresh(db_sale_offers_page)

    return db_sale_offers_page


@pytest.fixture(scope='function')
def created_parsing_history(
        db: Session,
        created_sale_offers_page: models.SaleOffersPage,
) -> models.ParsingHistory:
    db_parsing_history: models.ParsingHistory = models.ParsingHistory(
        sale_offers_page_id=created_sale_offers_page.id,
        ended_on_page_number=0
    )

    db.add(db_parsing_history)
    db.commit()
    db.refresh(db_parsing_history)

    return db_parsing_history


def _create_role(db: Session, name: str) -> models.Role:
    db_role: models.Role = models.Role(
        name=name
    )

    db.add(db_role)
    db.commit()
    db.refresh(db_role)

    return db_role


@pytest.fixture(scope='function')
def created_role_admin(db: Session) -> models.Role:
    return _create_role(db=db, name=value_objects.RoleName.ADMIN)


@pytest.fixture(scope='function')
def created_role_user(db: Session) -> models.Role:
    return _create_role(db=db, name=value_objects.RoleName.USER)


@pytest.fixture(scope='function')
def created_user(db: Session) -> models.User:
    db_user: models.User = models.User(
        username='test',
        email='test@mail.ru',
        password='testhashofpassword',
        roles=[]
    )

    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


@pytest.fixture(scope='function')
def created_token(db: Session, created_user: models.User) -> models.Token:
    db_token: models.Token = models.Token(
        hash='testhash',
        type='access',
        user_id=created_user.id
    )

    db.add(db_token)
    db.commit()
    db.refresh(db_token)

    return db_token
