import datetime

import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import sale_offer_repository


def test_get_all_empty(db: Session) -> None:
    db_sale_offer_list: list[models.SaleOffer] = sale_offer_repository.get_all(db=db)

    assert isinstance(db_sale_offer_list, list)
    assert len(db_sale_offer_list) == 0


def test_get_all_not_empty(db: Session, created_sale_offer: models.SaleOffer) -> None:
    db_sale_offer_list: list[models.SaleOffer] = sale_offer_repository.get_all(db=db)

    assert isinstance(db_sale_offer_list, list)
    assert len(db_sale_offer_list) == 1


def test_get_by_id_nonexistent(db: Session) -> None:
    db_sale_offer: models.SaleOffer | None = sale_offer_repository.get_by_id(db=db, id=1)

    assert db_sale_offer is None


def test_get_by_id_existing(db: Session, created_sale_offer: models.SaleOffer) -> None:
    db_sale_offer: models.SaleOffer | None = sale_offer_repository.get_by_id(db=db, id=created_sale_offer.id)

    assert db_sale_offer is not None
    assert db_sale_offer is created_sale_offer


def test_get_nonexistent(db: Session, created_car: models.Car, created_marketplace: models.Marketplace) -> None:
    db_sale_offer: models.SaleOffer | None = sale_offer_repository.get(
        db=db,
        mileage=0,
        price=200000,
        created_at=datetime.datetime.now(),
        car_id=created_car.id,
        marketplace_id=created_marketplace.id
    )

    assert db_sale_offer is None


def test_get_existing(db: Session, created_sale_offer: models.SaleOffer) -> None:
    db_sale_offer: models.SaleOffer | None = sale_offer_repository.get(
        db=db,
        mileage=created_sale_offer.mileage,
        price=created_sale_offer.price,
        created_at=created_sale_offer.created_at,
        car_id=created_sale_offer.car_id,
        marketplace_id=created_sale_offer.marketplace_id
    )

    assert db_sale_offer is not None
    assert db_sale_offer is created_sale_offer


def test_get_count_all_empty(db: Session) -> None:
    count: int = sale_offer_repository.get_count_all(db=db)

    assert count == 0


def test_get_count_all_not_empty(db: Session, created_sale_offer: models.SaleOffer) -> None:
    count: int = sale_offer_repository.get_count_all(db=db)

    assert count == 1


def test_create_nonexistent(db: Session, created_car: models.Car, created_marketplace: models.Marketplace) -> None:
    price: int = 200000
    db_sale_offer: models.SaleOffer = sale_offer_repository.create(
        db=db,
        sale_offer=schemas.SaleOfferCreate(
            mileage=0,
            price=price,
            created_at=datetime.datetime.now(),
            car_id=created_car.id,
            marketplace_id=created_marketplace.id,
        )
    )

    assert db_sale_offer is not None
    assert db_sale_offer.price == price


def test_create_existing(db: Session, created_sale_offer: models.SaleOffer) -> None:
    with pytest.raises(IntegrityError):
        _: models.SaleOffer = sale_offer_repository.create(
            db=db,
            sale_offer=schemas.SaleOfferCreate(
                mileage=created_sale_offer.mileage,
                price=created_sale_offer.price,
                created_at=created_sale_offer.created_at,
                car_id=created_sale_offer.car_id,
                marketplace_id=created_sale_offer.marketplace_id
            )
        )
