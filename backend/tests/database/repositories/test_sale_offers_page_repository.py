import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import sale_offers_page_repository


def test_get_all_empty(db: Session) -> None:
    db_sale_offers_page_list: list[models.SaleOffersPage] = sale_offers_page_repository.get_all(db=db)

    assert isinstance(db_sale_offers_page_list, list)
    assert len(db_sale_offers_page_list) == 0


def test_get_all_not_empty(db: Session, created_sale_offers_page: models.SaleOffersPage) -> None:
    db_sale_offers_page_list: list[models.SaleOffersPage] = sale_offers_page_repository.get_all(db=db)

    assert isinstance(db_sale_offers_page_list, list)
    assert len(db_sale_offers_page_list) == 1


def test_get_by_marketplace_id_and_brand_nonexistent(db: Session) -> None:
    db_sale_offers_page: models.SaleOffersPage | None = sale_offers_page_repository.get_by_marketplace_id_and_brand(
        db=db,
        marketplace_id=1,
        brand=value_objects.BrandName.MERCEDES
    )

    assert db_sale_offers_page is None


def test_get_by_marketplace_id_and_brand_existing(db: Session, created_sale_offers_page: models.SaleOffersPage) -> None:
    db_sale_offers_page: models.SaleOffersPage | None = sale_offers_page_repository.get_by_marketplace_id_and_brand(
        db=db,
        marketplace_id=created_sale_offers_page.marketplace_id,
        brand=created_sale_offers_page.brand
    )

    assert db_sale_offers_page is not None
    assert db_sale_offers_page is created_sale_offers_page


def test_get_count_all_empty(db: Session) -> None:
    count: int = sale_offers_page_repository.get_count_all(db=db)

    assert count == 0


def test_get_count_all_not_empty(db: Session, created_sale_offers_page: models.SaleOffer) -> None:
    count: int = sale_offers_page_repository.get_count_all(db=db)

    assert count == 1


def test_create_nonexistent(db: Session, created_marketplace: models.Marketplace) -> None:
    path: str = '/cars/mercedes'
    db_sale_offers_page: models.SaleOffersPage = sale_offers_page_repository.create(
        db=db,
        sale_offers_page=schemas.SaleOffersPageCreate(
            marketplace_id=created_marketplace.id,
            brand=value_objects.BrandName.MERCEDES,
            path=path
        )
    )

    assert db_sale_offers_page is not None
    assert db_sale_offers_page.path == path


def test_create_existing(db: Session, created_sale_offers_page: models.SaleOffersPage) -> None:
    with pytest.raises(IntegrityError):
        _: models.SaleOffersPage = sale_offers_page_repository.create(
            db=db,
            sale_offers_page=schemas.SaleOffersPageCreate(
                marketplace_id=created_sale_offers_page.marketplace_id,
                brand=created_sale_offers_page.brand,
                path=created_sale_offers_page.path
            )
        )
