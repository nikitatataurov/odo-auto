from sqlalchemy.orm import Session

from database import models
from database.repositories import role_repository


def test_get_by_name_nonexistent(db: Session) -> None:
    db_role: models.Role | None = role_repository.get_by_name(db=db, name='nonexistent')

    assert db_role is None


def test_get_by_name_existing(db: Session, created_role_admin: models.Role) -> None:
    db_role: models.Role | None = role_repository.get_by_name(db=db, name=created_role_admin.name)

    assert db_role is not None
    assert db_role is created_role_admin


def test_get_admin_nonexistent(db: Session) -> None:
    db_role: models.Role | None = role_repository.get_admin(db=db)

    assert db_role is None


def test_get_admin_existing(db: Session, created_role_admin: models.Role) -> None:
    db_role: models.Role | None = role_repository.get_admin(db=db)

    assert db_role is not None
    assert db_role is created_role_admin


def test_get_user_nonexistent(db: Session) -> None:
    db_role: models.Role | None = role_repository.get_user(db=db)

    assert db_role is None


def test_get_user_existing(db: Session, created_role_user: models.Role) -> None:
    db_role: models.Role | None = role_repository.get_user(db=db)

    assert db_role is not None
    assert db_role is created_role_user
