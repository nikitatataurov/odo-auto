import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import car_repository


def test_get_all_empty(db: Session) -> None:
    db_car_list: list[models.Car] = car_repository.get_all(db=db)

    assert isinstance(db_car_list, list)
    assert len(db_car_list) == 0


def test_get_all_not_empty(db: Session, created_car: models.Car) -> None:
    db_car_list: list[models.Car] = car_repository.get_all(db=db)

    assert isinstance(db_car_list, list)
    assert len(db_car_list) == 1


def test_get_by_id_nonexistent(db: Session) -> None:
    db_car: models.Car | None = car_repository.get_by_id(db=db, id=1)

    assert db_car is None


def test_get_by_id_existing(db: Session, created_car: models.Car) -> None:
    db_car: models.Car | None = car_repository.get_by_id(db=db, id=created_car.id)

    assert db_car is not None
    assert db_car is created_car


def test_get_nonexistent(db: Session, created_fuel_type: models.FuelType) -> None:
    db_car: models.Car | None = car_repository.get(
        db=db,
        brand=value_objects.BrandName.MERCEDES,
        model='e500',
        engine_volume=5000,
        production_year=2023,
        fuel_type_id=created_fuel_type.id
    )

    assert db_car is None


def test_get_existing(db: Session, created_car: models.Car) -> None:
    db_car: models.Car | None = car_repository.get(
        db=db,
        brand=created_car.brand,
        model=created_car.model,
        engine_volume=created_car.engine_volume,
        production_year=created_car.production_year,
        fuel_type_id=created_car.fuel_type_id
    )

    assert db_car is not None
    assert db_car is created_car


def test_get_count_all_empty(db: Session) -> None:
    count: int = car_repository.get_count_all(db=db)

    assert count == 0


def test_get_count_all_not_empty(db: Session, created_car: models.SaleOffer) -> None:
    count: int = car_repository.get_count_all(db=db)

    assert count == 1


def test_create_nonexistent(db: Session, created_fuel_type: models.FuelType) -> None:
    brand: str = value_objects.BrandName.MERCEDES
    db_car: models.Car = car_repository.create(
        db=db,
        car=schemas.CarCreate(
            brand=brand,
            model='e500',
            engine_volume=5000,
            production_year=2023,
            fuel_type_id=created_fuel_type.id
        )
    )

    assert db_car is not None
    assert db_car.brand == brand


def test_create_existing(db: Session, created_car: models.Car) -> None:
    with pytest.raises(IntegrityError):
        _: models.Car = car_repository.create(
            db=db,
            car=schemas.CarCreate(
                brand=created_car.brand,
                model=created_car.model,
                engine_volume=created_car.engine_volume,
                production_year=created_car.production_year,
                fuel_type_id=created_car.fuel_type_id
            )
        )
