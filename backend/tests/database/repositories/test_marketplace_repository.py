import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import marketplace_repository


def test_get_all_empty(db: Session) -> None:
    db_marketplace_list: list[models.Marketplace] = marketplace_repository.get_all(db=db)

    assert isinstance(db_marketplace_list, list)
    assert len(db_marketplace_list) == 0


def test_get_all_not_empty(db: Session, created_marketplace: list[models.Marketplace]) -> None:
    db_marketplace_list: list[models.Marketplace] = marketplace_repository.get_all(db=db)

    assert db_marketplace_list is not None
    assert len(db_marketplace_list) == 1


def test_get_by_name_nonexistent(db: Session) -> None:
    db_marketplace: models.Marketplace | None = marketplace_repository.get_by_name(db=db, name='auto.by')

    assert db_marketplace is None


def test_get_by_name_existing(db: Session, created_marketplace: models.Marketplace) -> None:
    db_marketplace: models.Marketplace | None = marketplace_repository.get_by_name(db=db, name=created_marketplace.name)

    assert db_marketplace is not None
    assert db_marketplace is created_marketplace


def test_get_count_all_empty(db: Session) -> None:
    count: int = marketplace_repository.get_count_all(db=db)

    assert count == 0


def test_get_count_all_not_empty(db: Session, created_marketplace: models.SaleOffer) -> None:
    count: int = marketplace_repository.get_count_all(db=db)

    assert count == 1


def test_create_nonexistent(db: Session, created_country: models.Country) -> None:
    name: str = value_objects.MarketplaceName.AUTOBY_BY
    db_marketplace: models.Marketplace = marketplace_repository.create(
        db=db,
        marketplace=schemas.MarketplaceCreate(
            name=name,
            country_id=created_country.id,
            url='http://auto.by'
        )
    )

    assert db_marketplace is not None
    assert db_marketplace.name == name


def test_create_existing(db: Session, created_marketplace: models.Marketplace) -> None:
    with pytest.raises(IntegrityError):
        _: models.Marketplace = marketplace_repository.create(
            db=db,
            marketplace=schemas.MarketplaceCreate(
                name=created_marketplace.name,
                country_id=created_marketplace.country_id,
                url=created_marketplace.url
            )
        )
