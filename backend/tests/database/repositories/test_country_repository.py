import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import country_repository


def test_get_all_empty(db: Session) -> None:
    db_country_list: list[models.Country] = country_repository.get_all(db=db)

    assert isinstance(db_country_list, list)
    assert len(db_country_list) == 0


def test_get_all_not_empty(db: Session, created_country: models.Country) -> None:
    db_country_list: list[models.Country] = country_repository.get_all(db=db)

    assert db_country_list is not None
    assert len(db_country_list) == 1


def test_get_by_id_nonexistent(db: Session) -> None:
    db_country: models.Country | None = country_repository.get_by_id(db=db, id=1)

    assert db_country is None


def test_get_by_id_existing(db: Session, created_country: models.Country) -> None:
    db_country: models.Country | None = country_repository.get_by_id(db=db, id=created_country.id)

    assert db_country is not None
    assert db_country is created_country


def test_get_by_code_nonexistent(db: Session) -> None:
    db_country: models.Country | None = country_repository.get_by_code(db=db, code='by')

    assert db_country is None


def test_get_by_code_existing(db: Session, created_country: models.Country) -> None:
    db_country: models.Country | None = country_repository.get_by_code(db=db, code=created_country.code)

    assert db_country is not None
    assert db_country is created_country


def test_get_count_all_empty(db: Session) -> None:
    count: int = country_repository.get_count_all(db=db)

    assert count == 0


def test_get_count_all_not_empty(db: Session, created_country: models.SaleOffer) -> None:
    count: int = country_repository.get_count_all(db=db)

    assert count == 1


def test_create_nonexistent(db: Session) -> None:
    country: value_objects.CountryName = value_objects.CountryName.BY
    code: str = country.name
    db_country: models.Country = country_repository.create(
        db=db,
        country=schemas.CountryCreate(
            code=code,
            name=country
        )
    )

    assert db_country is not None
    assert db_country.code == code


def test_create_existing(db: Session, created_country: models.Country) -> None:
    with pytest.raises(IntegrityError):
        _: models.Country = country_repository.create(
            db=db,
            country=schemas.CountryCreate(
                code=created_country.code,
                name=created_country.name
            )
        )
