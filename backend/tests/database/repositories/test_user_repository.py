import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import user_repository


def test_get_all_empty(db: Session) -> None:
    db_user_list: list[models.User] = user_repository.get_all(db=db)

    assert isinstance(db_user_list, list)
    assert len(db_user_list) == 0


def test_get_all_not_empty(db: Session, created_user: list[models.User]) -> None:
    db_user_list: list[models.User] = user_repository.get_all(db=db)

    assert db_user_list is not None
    assert len(db_user_list) == 1


def test_get_by_name_nonexistent(db: Session) -> None:
    db_user: models.User | None = user_repository.get_by_username(db=db, username='test')

    assert db_user is None


def test_get_by_name_existing(db: Session, created_user: models.User) -> None:
    db_user: models.User | None = user_repository.get_by_username(db=db, username=created_user.username)

    assert db_user is not None
    assert db_user is created_user


def test_create_nonexistent(db: Session, created_role_user: models.Role) -> None:
    username: str = 'test'
    db_user: models.User = user_repository.create(
        db=db,
        user=schemas.UserCreate(
            username=username,
            email='test@mail.ru',
            password='testhashofpassword',
        ),
        roles=[created_role_user]
    )

    assert db_user is not None
    assert db_user.username == username


def test_create_existing(db: Session, created_user: models.User, created_role_user: models.Role) -> None:
    with pytest.raises(IntegrityError):
        _: models.User = user_repository.create(
            db=db,
            user=schemas.UserCreate(
                username=created_user.username,
                email=created_user.email,
                password=created_user.password
            ),
            roles=[created_role_user]
        )
