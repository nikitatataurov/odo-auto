import datetime

from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import token_repository


def test_get_by_hash_nonexistent(db: Session) -> None:
    db_token: models.Token | None = token_repository.get_by_hash(db=db, hash='testhash')

    assert db_token is None


def test_get_by_hash_existing(db: Session, created_token: models.Token) -> None:
    db_token: models.Token | None = token_repository.get_by_hash(db=db, hash=created_token.hash)

    assert db_token is not None
    assert db_token is created_token


def test_create_nonexistent(db: Session, created_user: models.User) -> None:
    hash: str = 'testhash'
    db_token: models.Token = token_repository.create(
        db=db,
        token=schemas.TokenCreate(
            hash=hash,
            type='access',
            user_id=created_user.id
        )
    )

    assert db_token is not None
    assert db_token.hash == hash


def test_set_removed(db: Session, created_token: models.Token) -> None:
    removed_at: datetime.datetime = datetime.datetime.now()
    db_token: models.Token = token_repository.set_removed_at(db=db, db_token=created_token, removed_at=removed_at)

    assert db_token is created_token
    assert db_token.removed_at == removed_at
