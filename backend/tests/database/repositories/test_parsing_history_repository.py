import pytest
from sqlalchemy.orm import Session

import schemas
from database import models
from database.repositories import parsing_history_repository


def test_get_all_empty(db: Session) -> None:
    db_parsing_history_list: list[models.ParsingHistory] = parsing_history_repository.get_all(db=db)

    assert isinstance(db_parsing_history_list, list)
    assert len(db_parsing_history_list) == 0


def test_get_all_not_empty(db: Session, created_parsing_history: models.ParsingHistory) -> None:
    db_parsing_history_list: list[models.ParsingHistory] = parsing_history_repository.get_all(db=db)

    assert db_parsing_history_list is not None
    assert len(db_parsing_history_list) == 1


def test_get_by_id_nonexistent(db: Session) -> None:
    db_parsing_history: models.ParsingHistory | None = parsing_history_repository.get_by_id(db=db, id=1)

    assert db_parsing_history is None


def test_get_by_id_existing(db: Session, created_parsing_history: models.ParsingHistory) -> None:
    db_parsing_history: models.ParsingHistory | None = parsing_history_repository.get_by_id(
        db=db,
        id=created_parsing_history.id
    )

    assert db_parsing_history is not None
    assert db_parsing_history is created_parsing_history


def test_get_created_in_current_month_by_sale_offers_page_id_nonexistent(db: Session) -> None:
    db_parsing_history: models.ParsingHistory | None = parsing_history_repository.get_actual_by_sale_offers_page_id(
        db=db,
        sale_offers_page_id=1
    )

    assert db_parsing_history is None


def test_get_created_in_current_month_by_sale_offers_page_id_existing(
        db: Session,
        created_parsing_history: models.ParsingHistory,
) -> None:
    db_parsing_history: models.ParsingHistory | None = parsing_history_repository.get_actual_by_sale_offers_page_id(
        db=db,
        sale_offers_page_id=created_parsing_history.id
    )

    assert db_parsing_history is not None
    assert db_parsing_history is created_parsing_history


def test_create_nonexistent(db: Session, created_sale_offers_page: models.SaleOffersPage) -> None:
    sale_offers_page_id: int = created_sale_offers_page.id
    db_parsing_history: models.ParsingHistory = parsing_history_repository.create(
        db=db,
        parsing_history=schemas.ParsingHistoryCreate(
            sale_offers_page_id=sale_offers_page_id,
            ended_on_page_number=0
        )
    )

    assert db_parsing_history is not None
    assert db_parsing_history.sale_offers_page_id == sale_offers_page_id


def test_set_ended_on_page_number(db: Session, created_parsing_history: models.ParsingHistory) -> None:
    ended_on_page_number: int = 3
    db_parsing_history: models.ParsingHistory = parsing_history_repository.set_ended_on_page_number(
        db=db,
        db_parsing_history=created_parsing_history,
        ended_on_page_number=ended_on_page_number
    )

    assert db_parsing_history is created_parsing_history
    assert db_parsing_history.ended_on_page_number == ended_on_page_number


def test_set_is_stopped(db: Session, created_parsing_history: models.ParsingHistory) -> None:
    is_stopped: bool = False
    db_parsing_history: models.ParsingHistory = parsing_history_repository.set_is_stopped(
        db=db,
        db_parsing_history=created_parsing_history,
        is_stopped=is_stopped,
    )

    assert db_parsing_history is created_parsing_history
    assert db_parsing_history.is_stopped == is_stopped


@pytest.mark.parametrize('is_stopped', [True, False])
def test_set_is_stopped_by_marketplace_id(
        db: Session,
        created_parsing_history: models.ParsingHistory,
        is_stopped: bool
) -> None:
    parsing_history_repository.set_is_stopped_by_marketplace_id(
        db=db,
        marketplace_id=created_parsing_history.sale_offers_page.marketplace_id,
        is_stopped=is_stopped
    )

    assert created_parsing_history.is_stopped is is_stopped


def test_check_is_stopped_all_by_marketplace_id(
        db: Session,
        created_parsing_history: models.ParsingHistory,
) -> None:
    is_stopped: bool = parsing_history_repository.check_is_stopped_all_by_marketplace_id(
        db=db,
        marketplace_id=created_parsing_history.sale_offers_page.marketplace_id,
    )

    assert created_parsing_history.is_stopped is is_stopped
