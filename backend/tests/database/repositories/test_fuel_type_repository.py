import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import fuel_type_repository


def test_get_all_empty(db: Session) -> None:
    db_fuel_type_list: list[models.FuelType] = fuel_type_repository.get_all(db=db)

    assert isinstance(db_fuel_type_list, list)
    assert len(db_fuel_type_list) == 0


def test_get_all_not_empty(db: Session, created_fuel_type: models.FuelType) -> None:
    db_fuel_type_list: list[models.FuelType] = fuel_type_repository.get_all(db=db)

    assert db_fuel_type_list is not None
    assert len(db_fuel_type_list) == 1


def test_get_by_id_nonexistent(db: Session) -> None:
    db_fuel_type: models.FuelType | None = fuel_type_repository.get_by_id(db=db, id=1)

    assert db_fuel_type is None


def test_get_by_id_existing(db: Session, created_fuel_type: models.FuelType) -> None:
    db_fuel_type: models.FuelType | None = fuel_type_repository.get_by_id(db=db, id=created_fuel_type.id)

    assert db_fuel_type is not None
    assert db_fuel_type is created_fuel_type


def test_get_by_name_nonexistent(db: Session) -> None:
    db_fuel_type: models.FuelType | None = fuel_type_repository.get_by_name(db=db, name='gasoline')

    assert db_fuel_type is None


def test_get_by_name_existing(db: Session, created_fuel_type: models.FuelType) -> None:
    db_fuel_type: models.FuelType | None = fuel_type_repository.get_by_name(db=db, name=created_fuel_type.name)

    assert db_fuel_type is not None
    assert db_fuel_type is created_fuel_type


def test_get_count_all_empty(db: Session) -> None:
    count: int = fuel_type_repository.get_count_all(db=db)

    assert count == 0


def test_get_count_all_not_empty(db: Session, created_fuel_type: models.SaleOffer) -> None:
    count: int = fuel_type_repository.get_count_all(db=db)

    assert count == 1


def test_create_nonexistent(db: Session) -> None:
    name: str = value_objects.FuelTypeName.GASOLINE
    db_fuel_type: models.FuelType = fuel_type_repository.create(
        db=db,
        fuel_type=schemas.FuelTypeCreate(
            name=name
        )
    )

    assert db_fuel_type is not None
    assert db_fuel_type.name == name


def test_create_existing(db: Session, created_fuel_type: models.FuelType) -> None:
    with pytest.raises(IntegrityError):
        _: models.FuelType = fuel_type_repository.create(
            db=db,
            fuel_type=schemas.FuelTypeCreate(
                name=created_fuel_type.name
            )
        )
