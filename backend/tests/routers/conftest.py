import datetime

import httpx
import pytest

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

import schemas
import value_objects
from database import models
from database.repositories import (
    car_repository,
    country_repository,
    fuel_type_repository,
    marketplace_repository,
    sale_offer_repository, sale_offers_page_repository,
)
from services import auth_service


@pytest.fixture(scope='function')
def country_create_list() -> list[schemas.CountryCreate]:
    by_country: value_objects.CountryName = value_objects.CountryName.BY
    pl_country: value_objects.CountryName = value_objects.CountryName.PL
    kz_country: value_objects.CountryName = value_objects.CountryName.KZ
    return [
        schemas.CountryCreate(
            code=by_country.name,
            name=by_country
        ),
        schemas.CountryCreate(
            code=pl_country.name,
            name=pl_country
        ),
        schemas.CountryCreate(
            code=kz_country.name,
            name=kz_country
        ),
    ]


@pytest.fixture(scope='function')
def created_countries(db: Session, country_create_list: list[schemas.CountryCreate]) -> list[models.Country]:
    db_country_list: [models.Country] = []
    for country_create in country_create_list:
        db_country_list.append(
            country_repository.create(db=db, country=country_create)
        )

    return db_country_list


@pytest.fixture(scope='function')
def marketplace_create_list(created_countries: list[models.Country]) -> list[schemas.MarketplaceCreate]:
    return [
        schemas.MarketplaceCreate(
            name=value_objects.MarketplaceName.AUTOBY_BY,
            country_id=created_countries[1].id,
            url='https://auto.by'
        ),
        schemas.MarketplaceCreate(
            name=value_objects.MarketplaceName.OTOMOTO_PL,
            country_id=created_countries[0].id,
            url='https://auto.pl'
        ),
        schemas.MarketplaceCreate(
            name=value_objects.MarketplaceName.ASTER_KZ,
            country_id=created_countries[2].id,
            url='https://auto.kz'
        ),
    ]


@pytest.fixture(scope='function')
def created_marketplaces(
        db: Session,
        marketplace_create_list: list[schemas.MarketplaceCreate],
) -> list[models.Marketplace]:
    db_marketplace_list: [models.Marketplace] = []
    for marketplace_create in marketplace_create_list:
        db_marketplace_list.append(
            marketplace_repository.create(db=db, marketplace=marketplace_create)
        )

    return db_marketplace_list


@pytest.fixture(scope='function')
def fuel_type_create_list() -> list[schemas.FuelTypeCreate]:
    return [
        schemas.FuelTypeCreate(
            name=value_objects.FuelTypeName.DIESEL
        ),
        schemas.FuelTypeCreate(
            name=value_objects.FuelTypeName.GASOLINE
        ),
        schemas.FuelTypeCreate(
            name=value_objects.FuelTypeName.HYBRID
        )
    ]


@pytest.fixture(scope='function')
def created_fuel_types(db: Session, fuel_type_create_list: list[schemas.FuelTypeCreate]) -> list[models.FuelType]:
    db_fuel_type_list: [models.FuelType] = []
    for fuel_type_create in fuel_type_create_list:
        db_fuel_type_list.append(
            fuel_type_repository.create(db=db, fuel_type=fuel_type_create)
        )

    return db_fuel_type_list


@pytest.fixture(scope='function')
def car_create_list(created_fuel_types: list[models.FuelType]) -> list[schemas.CarCreate]:
    return [
        schemas.CarCreate(
            brand=value_objects.BrandName.MERCEDES,
            model='e300',
            production_year=2023,
            engine_volume=3000,
            fuel_type_id=created_fuel_types[0].id
        ),
        schemas.CarCreate(
            brand=value_objects.BrandName.BMW,
            model='m3',
            production_year=2023,
            engine_volume=3000,
            fuel_type_id=created_fuel_types[1].id
        ),
        schemas.CarCreate(
            brand=value_objects.BrandName.AUDI,
            model='a9',
            production_year=2023,
            engine_volume=3000,
            fuel_type_id=created_fuel_types[2].id
        )
    ]


@pytest.fixture(scope='function')
def created_cars(db: Session, car_create_list: list[schemas.CarCreate]) -> list[models.Car]:
    db_car_list: list[models.Car] = []
    for car_create in car_create_list:
        db_car_list.append(
            car_repository.create(db=db, car=car_create)
        )

    return db_car_list


@pytest.fixture(scope='function')
def sale_offer_create_list(
        created_marketplaces: list[models.Marketplace],
        created_cars: list[models.Car],
) -> list[schemas.SaleOfferCreate]:
    return [
        schemas.SaleOfferCreate(
            mileage=100000,
            price=20000,
            car_id=created_cars[0].id,
            marketplace_id=created_marketplaces[0].id,
            created_at=datetime.datetime.now()
        ),
        schemas.SaleOfferCreate(
            mileage=200000,
            price=60000,
            car_id=created_cars[1].id,
            marketplace_id=created_marketplaces[1].id,
            created_at=datetime.datetime.now()
        ),
        schemas.SaleOfferCreate(
            mileage=300000,
            price=80000,
            car_id=created_cars[2].id,
            marketplace_id=created_marketplaces[2].id,
            created_at=datetime.datetime.now()
        ),
    ]


@pytest.fixture(scope='function')
def created_sale_offers(db: Session, sale_offer_create_list: list[schemas.SaleOfferCreate]) -> list[models.SaleOffer]:
    db_sale_offer_list: list[models.SaleOffer] = []
    for sale_offer_create in sale_offer_create_list:
        db_sale_offer_list.append(
            sale_offer_repository.create(db=db, sale_offer=sale_offer_create)
        )

    return db_sale_offer_list


@pytest.fixture(scope='function')
def sale_offers_page_create_list(
        created_marketplaces: list[models.Marketplace],
) -> list[schemas.SaleOffersPageCreate]:
    return [
        schemas.SaleOffersPageCreate(
            marketplace_id=created_marketplaces[0].id,
            brand=value_objects.BrandName.MERCEDES,
            path='/mercedes',
            query_params='order=asc&sorted=true'
        ),
        schemas.SaleOffersPageCreate(
            marketplace_id=created_marketplaces[1].id,
            brand=value_objects.BrandName.BMW,
            path='/bmw',
            query_params='order=asc&sorted=true'
        ),
        schemas.SaleOffersPageCreate(
            marketplace_id=created_marketplaces[2].id,
            brand=value_objects.BrandName.AUDI,
            path='/audi'
        ),
    ]


@pytest.fixture(scope='function')
def created_sale_offers_pages(
        db: Session,
        sale_offers_page_create_list: list[schemas.SaleOffersPageCreate],
) -> list[models.SaleOffersPage]:
    db_sale_offers_page_list: list[models.SaleOffersPage] = []
    for sale_offers_page_create in sale_offers_page_create_list:
        db_sale_offers_page_list.append(
            sale_offers_page_repository.create(db=db, sale_offers_page=sale_offers_page_create)
        )

    return db_sale_offers_page_list


def _create_role(db: Session, name: str) -> models.Role:
    db_role: models.Role = models.Role(
        name=name
    )

    db.add(db_role)
    db.commit()
    db.refresh(db_role)

    return db_role


@pytest.fixture(scope='function')
def created_role_admin(db: Session) -> models.Role:
    return _create_role(db=db, name=value_objects.RoleName.ADMIN)


@pytest.fixture(scope='function')
def created_role_user(db: Session) -> models.Role:
    return _create_role(db=db, name=value_objects.RoleName.USER)


@pytest.fixture(scope='function')
def user_credentials() -> dict:
    return {'username': 'test', 'email': 'test@mail.ru', 'password': 'test'}


@pytest.fixture(scope='function')
def admin_user_credentials() -> dict:
    return {'username': 'admin', 'email': 'admin@mail.ru', 'password': 'admin'}


@pytest.fixture(scope='function')
def created_user(db: Session, user_credentials: dict, created_role_user: models.Role) -> models.User:
    db_user: models.User = models.User(
        username=user_credentials['username'],
        email=user_credentials['email'],
        password=auth_service.get_password_hash(user_credentials['password']),
        roles=[created_role_user]
    )

    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


@pytest.fixture(scope='function')
def access_token(client: TestClient, created_user: models.User, user_credentials: dict) -> str:
    response: httpx.Response = client.post(
        '/auth/sign-in',
        json=user_credentials,
    )

    return response.json()['access_token']


@pytest.fixture(scope='function')
def created_admin_user(
        db: Session,
        admin_user_credentials: dict,
        created_role_user: models.Role,
        created_role_admin: models.Role
) -> models.User:
    db_user: models.User = models.User(
        username=admin_user_credentials['username'],
        email=admin_user_credentials['email'],
        password=auth_service.get_password_hash(admin_user_credentials['password']),
        roles=[created_role_user, created_role_admin]
    )

    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    return db_user


@pytest.fixture(scope='function')
def admin_access_token(client: TestClient, created_admin_user: models.User, admin_user_credentials: dict) -> str:
    response: httpx.Response = client.post(
        '/auth/sign-in',
        json=admin_user_credentials
    )

    return response.json()['access_token']


@pytest.fixture(scope='function')
def refresh_token(client: TestClient, created_user: models.User, user_credentials: dict) -> str:
    response: httpx.Response = client.post(
        '/auth/sign-in',
        json=user_credentials
    )

    return response.json()['refresh_token']


@pytest.fixture(scope='function')
def expired_refresh_token() -> str:
    return (
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
        '.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTY5NTg0Njc2MC4zNzAxNTd9'
        '.K-5H0525PhqioTBZEIIrPROwvJSVyQYbFPhdDB4FynU'
    )
