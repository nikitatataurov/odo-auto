import httpx
from fastapi.testclient import TestClient

import schemas
from database import models


def test_paginate_cars_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/cars')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 0


def test_paginate_cars_not_empty(client: TestClient, created_cars: list[models.Car]) -> None:
    response: httpx.Response = client.get('/api/v1/cars')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 3


def test_read_count_all_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/cars/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 0


def test_read_count_all_not_empty(client: TestClient, created_cars: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/cars/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 3


def test_read_nonexistent_car(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/cars/1')

    assert response.status_code == 404
    assert response.json()['detail'] == 'Car not found'


def test_read_existing_car(client: TestClient, created_cars: list[models.Car]) -> None:
    created_car: models.Car = created_cars[0]
    response: httpx.Response = client.get(f'/api/v1/cars/{created_car.id}')

    assert response.status_code == 200
    assert response.json()['brand'] == created_car.brand


def test_create_nonexistent_car(
        client: TestClient,
        car_create_list: list[schemas.CarCreate],
        admin_access_token: str,
) -> None:
    car_create: schemas.CarCreate = car_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/cars',
        json=car_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 201
    assert response.json()['brand'] == car_create.brand


def test_create_existing_car(
        client: TestClient,
        car_create_list: list[schemas.CarCreate],
        created_cars: list[models.Car],
        admin_access_token: str,
) -> None:
    car_create: schemas.CarCreate = car_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/cars',
        json=car_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['detail'] == 'Car exists'


def test_create_car_without_token(
        client: TestClient,
        car_create_list: list[schemas.CarCreate],
) -> None:
    car_create: schemas.CarCreate = car_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/cars',
        json=car_create.model_dump(),
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_create_car_without_admin_token(
        client: TestClient,
        car_create_list: list[schemas.CarCreate],
        access_token: str,
) -> None:
    car_create: schemas.CarCreate = car_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/cars',
        json=car_create.model_dump(),
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'
