import httpx
from fastapi.testclient import TestClient

import schemas
from database import models


def test_read_all_marketplaces_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces/all')

    assert response.status_code == 200
    assert len(response.json()) == 0


def test_read_all_marketplaces_not_empty(client: TestClient, created_marketplaces: list[models.Marketplace]) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces/all')

    assert response.status_code == 200
    assert len(response.json()) == 3


def test_paginate_marketplaces_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 0


def test_paginate_marketplaces_not_empty(client: TestClient, created_marketplaces: list[models.Marketplace]) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 3


def test_read_count_all_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 0


def test_read_count_all_not_empty(client: TestClient, created_marketplaces: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 3


def test_read_nonexistent_marketplace(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/marketplaces/1')

    assert response.status_code == 404
    assert response.json()['detail'] == 'Marketplace not found'


def test_read_existing_marketplace(client: TestClient, created_marketplaces: list[models.Marketplace]) -> None:
    created_marketplace: models.Marketplace = created_marketplaces[0]
    response: httpx.Response = client.get(f'/api/v1/marketplaces/{created_marketplace.id}')

    assert response.status_code == 200
    assert response.json()['name'] == created_marketplace.name


def test_create_nonexistent_marketplace(
        client: TestClient,
        marketplace_create_list: list[schemas.MarketplaceCreate],
        admin_access_token: str,
) -> None:
    marketplace_create: schemas.MarketplaceCreate = marketplace_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/marketplaces',
        json=marketplace_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 201
    assert response.json()['name'] == marketplace_create.name


def test_create_existing_marketplace(
        client: TestClient,
        marketplace_create_list: list[schemas.MarketplaceCreate],
        created_marketplaces: list[models.Marketplace],
        admin_access_token: str,
) -> None:
    marketplace_create: schemas.MarketplaceCreate = marketplace_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/marketplaces',
        json=marketplace_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['detail'] == 'Marketplace exists'


def test_create_marketplace_without_token(
        client: TestClient,
        marketplace_create_list: list[schemas.MarketplaceCreate],
) -> None:
    marketplace_create: schemas.MarketplaceCreate = marketplace_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/marketplaces',
        json=marketplace_create.model_dump(),
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_create_marketplace_without_admin_token(
        client: TestClient,
        marketplace_create_list: list[schemas.MarketplaceCreate],
        access_token: str,
) -> None:
    marketplace_create: schemas.MarketplaceCreate = marketplace_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/marketplaces',
        json=marketplace_create.model_dump(),
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'
