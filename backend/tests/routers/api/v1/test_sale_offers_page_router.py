import httpx
from fastapi.testclient import TestClient

import schemas
from database import models


def test_paginate_sale_offers_pages_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers-pages')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 0


def test_paginate_sale_offers_pages_not_empty(
        client: TestClient,
        created_sale_offers_pages: list[models.SaleOffersPage],
) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers-pages')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 3


def test_read_count_all_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers-pages/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 0


def test_read_count_all_not_empty(client: TestClient, created_sale_offers_pages: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers-pages/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 3


def test_read_nonexistent_sale_offers_page(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers-pages/1')

    assert response.status_code == 404
    assert response.json()['detail'] == 'Sale offers page not found'


def test_read_existing_sale_offers_page(
        client: TestClient,
        created_sale_offers_pages: list[models.SaleOffersPage],
) -> None:
    created_sale_offers_page: models.SaleOffersPage = created_sale_offers_pages[0]
    response: httpx.Response = client.get(f'/api/v1/sale-offers-pages/{created_sale_offers_page.id}')

    assert response.status_code == 200
    assert response.json()['brand'] == created_sale_offers_page.brand


def test_create_nonexistent_sale_offers_page(
        client: TestClient,
        sale_offers_page_create_list: list[schemas.SaleOffersPageCreate],
        admin_access_token: str,
) -> None:
    sale_offers_page_create: schemas.SaleOffersPageCreate = sale_offers_page_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/sale-offers-pages',
        json=sale_offers_page_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 201
    assert response.json()['brand'] == sale_offers_page_create.brand


def test_create_existing_sale_offers_page(
        client: TestClient,
        sale_offers_page_create_list: list[schemas.SaleOffersPageCreate],
        created_sale_offers_pages: list[models.SaleOffersPage],
        admin_access_token: str,
) -> None:
    sale_offers_page_create: schemas.SaleOffersPageCreate = sale_offers_page_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/sale-offers-pages',
        json=sale_offers_page_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['detail'] == 'Sale offers page exists'


def test_create_sale_offers_page_without_token(
        client: TestClient,
        sale_offers_page_create_list: list[schemas.SaleOffersPageCreate],
) -> None:
    sale_offers_page_create: schemas.SaleOffersPageCreate = sale_offers_page_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/sale-offers-pages',
        json=sale_offers_page_create.model_dump(),
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_create_sale_offers_page_without_admin_token(
        client: TestClient,
        sale_offers_page_create_list: list[schemas.SaleOffersPageCreate],
        access_token: str,
) -> None:
    sale_offers_page_create: schemas.SaleOffersPageCreate = sale_offers_page_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/sale-offers-pages',
        json=sale_offers_page_create.model_dump(),
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'
