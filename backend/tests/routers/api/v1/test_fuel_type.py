import httpx
from fastapi.testclient import TestClient

import schemas
from database import models


def test_read_all_fuel_types_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types/all')

    assert response.status_code == 200
    assert len(response.json()) == 0


def test_read_all_fuel_types_not_empty(client: TestClient, created_fuel_types: list[models.FuelType]) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types/all')

    assert response.status_code == 200
    assert len(response.json()) == 3


def test_paginate_fuel_types_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 0


def test_paginate_fuel_types_not_empty(client: TestClient, created_fuel_types: list[models.FuelType]) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 3


def test_read_count_all_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 0


def test_read_count_all_not_empty(client: TestClient, created_fuel_types: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 3


def test_read_nonexistent_fuel_type(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/fuel-types/1')

    assert response.status_code == 404
    assert response.json()['detail'] == 'Fuel type not found'


def test_read_existing_fuel_type(client: TestClient, created_fuel_types: list[models.FuelType]) -> None:
    created_fuel_type: models.FuelType = created_fuel_types[0]
    response: httpx.Response = client.get(f'/api/v1/fuel-types/{created_fuel_type.id}')

    assert response.status_code == 200
    assert response.json()['name'] == created_fuel_type.name


def test_create_nonexistent_fuel_type(
        client: TestClient,
        fuel_type_create_list: list[schemas.FuelTypeCreate],
        admin_access_token: str,
) -> None:
    fuel_type_create: schemas.FuelTypeCreate = fuel_type_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/fuel-types',
        json=fuel_type_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 201
    assert response.json()['name'] == fuel_type_create.name


def test_create_existing_fuel_type(
        client: TestClient,
        fuel_type_create_list: list[schemas.FuelTypeCreate],
        created_fuel_types: list[models.FuelType],
        admin_access_token: str,
) -> None:
    fuel_type_create: schemas.FuelTypeCreate = fuel_type_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/fuel-types',
        json=fuel_type_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['detail'] == 'Fuel type exists'


def test_create_fuel_type_without_token(
        client: TestClient,
        fuel_type_create_list: list[schemas.FuelTypeCreate],
) -> None:
    fuel_type_create: schemas.FuelTypeCreate = fuel_type_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/fuel-types',
        json=fuel_type_create.model_dump(),
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_create_fuel_type_without_admin_token(
        client: TestClient,
        fuel_type_create_list: list[schemas.FuelTypeCreate],
        access_token: str,
) -> None:
    fuel_type_create: schemas.FuelTypeCreate = fuel_type_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/fuel-types',
        json=fuel_type_create.model_dump(),
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'
