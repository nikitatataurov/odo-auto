import httpx
from fastapi.testclient import TestClient

import schemas
from database import models


def test_paginate_sale_offers_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 0


def test_read_count_all_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 0


def test_read_count_all_not_empty(client: TestClient, created_sale_offers: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 3


def test_paginate_sale_offers_not_empty(client: TestClient, created_sale_offers: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 3


def test_read_nonexistent_sale_offer(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/sale-offers/1')

    assert response.status_code == 404
    assert response.json()['detail'] == 'Sale offer not found'


def test_read_existing_sale_offer(client: TestClient, created_sale_offers: list[models.SaleOffer]) -> None:
    created_sale_offer: models.SaleOffer = created_sale_offers[0]
    response: httpx.Response = client.get(f'/api/v1/sale-offers/{created_sale_offer.id}')

    assert response.status_code == 200
    assert response.json()['car_id'] == created_sale_offer.car_id


def test_create_nonexistent_sale_offer(
        client: TestClient,
        sale_offer_create_list: list[schemas.SaleOfferCreate],
        admin_access_token: str,
) -> None:
    sale_offer_create: schemas.SaleOfferCreate = sale_offer_create_list[0]
    sale_offer_create_json: dict = {
        **sale_offer_create.model_dump(exclude={'created_at'}),
        'created_at': sale_offer_create.created_at.isoformat()
    }

    response: httpx.Response = client.post(
        url='/api/v1/sale-offers',
        json=sale_offer_create_json,
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 201
    assert response.json()['car_id'] == sale_offer_create.car_id


def test_create_existing_sale_offer(
        client: TestClient,
        sale_offer_create_list: list[schemas.SaleOfferCreate],
        created_sale_offers: list[models.SaleOffer],
        admin_access_token: str,
) -> None:
    sale_offer_create: schemas.SaleOfferCreate = sale_offer_create_list[0]
    sale_offer_create_json: dict = {
        **sale_offer_create.model_dump(exclude={'created_at'}),
        'created_at': sale_offer_create.created_at.isoformat()
    }

    response: httpx.Response = client.post(
        url='/api/v1/sale-offers',
        json=sale_offer_create_json,
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['detail'] == 'Sale offer exists'


def test_create_sale_offer_without_token(
        client: TestClient,
        sale_offer_create_list: list[schemas.SaleOfferCreate],
) -> None:
    sale_offer_create: schemas.SaleOfferCreate = sale_offer_create_list[0]
    sale_offer_create_json: dict = {
        **sale_offer_create.model_dump(exclude={'created_at'}),
        'created_at': sale_offer_create.created_at.isoformat()
    }

    response: httpx.Response = client.post(
        url='/api/v1/sale-offers',
        json=sale_offer_create_json,
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_create_sale_offer_without_admin_token(
        client: TestClient,
        sale_offer_create_list: list[schemas.SaleOfferCreate],
        access_token: str,
) -> None:
    sale_offer_create: schemas.SaleOfferCreate = sale_offer_create_list[0]
    sale_offer_create_json: dict = {
        **sale_offer_create.model_dump(exclude={'created_at'}),
        'created_at': sale_offer_create.created_at.isoformat()
    }

    response: httpx.Response = client.post(
        url='/api/v1/sale-offers',
        json=sale_offer_create_json,
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'
