import httpx
from fastapi.testclient import TestClient

import schemas
from database import models


def test_read_all_countries_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/countries/all')

    assert response.status_code == 200
    assert len(response.json()) == 0


def test_read_all_countries_not_empty(client: TestClient, created_countries: list[models.Country]) -> None:
    response: httpx.Response = client.get('/api/v1/countries/all')

    assert response.status_code == 200
    assert len(response.json()) == 3


def test_paginate_countries_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/countries')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 0


def test_paginate_countries_not_empty(client: TestClient, created_countries: list[models.Country]) -> None:
    response: httpx.Response = client.get('/api/v1/countries')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'items' in response_json
    assert len(response_json['items']) == 3


def test_read_count_all_empty(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/countries/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 0


def test_read_count_all_not_empty(client: TestClient, created_countries: list[models.SaleOffer]) -> None:
    response: httpx.Response = client.get('/api/v1/countries/count/all')
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'count' in response_json
    assert response_json['count'] == 3


def test_read_nonexistent_country(client: TestClient) -> None:
    response: httpx.Response = client.get('/api/v1/countries/1')

    assert response.status_code == 404
    assert response.json()['detail'] == 'Country not found'


def test_read_existing_country(client: TestClient, created_countries: list[models.Country]) -> None:
    created_country: models.Country = created_countries[0]
    response: httpx.Response = client.get(f'/api/v1/countries/{created_country.id}')

    assert response.status_code == 200
    assert response.json()['name'] == created_country.name


def test_create_nonexistent_country(
        client: TestClient,
        country_create_list: list[schemas.CountryCreate],
        admin_access_token: str,
) -> None:
    country_create: schemas.CountryCreate = country_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/countries',
        json=country_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 201
    assert response.json()['name'] == country_create.name


def test_create_existing_country(
        client: TestClient,
        country_create_list: list[schemas.CountryCreate],
        created_countries: list[models.Country],
        admin_access_token: str,
) -> None:
    country_create: schemas.CountryCreate = country_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/countries',
        json=country_create.model_dump(),
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['detail'] == 'Country exists'


def test_create_country_without_token(
        client: TestClient,
        country_create_list: list[schemas.CountryCreate],
) -> None:
    country_create: schemas.CountryCreate = country_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/countries',
        json=country_create.model_dump(),
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_create_country_without_admin_token(
        client: TestClient,
        country_create_list: list[schemas.CountryCreate],
        access_token: str,
) -> None:
    country_create: schemas.CountryCreate = country_create_list[0]
    response: httpx.Response = client.post(
        url='/api/v1/countries',
        json=country_create.model_dump(),
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'
