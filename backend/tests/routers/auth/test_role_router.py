import httpx
from fastapi.testclient import TestClient


def test_is_admin_true(client: TestClient, admin_access_token: str) -> None:
    response: httpx.Response = client.post(
        '/auth/roles/is-admin',
        headers={'Authorization': f'Bearer {admin_access_token}'}
    )

    assert response.status_code == 200
    assert response.json()['result'] is True


def test_is_admin_false(client: TestClient, access_token: str) -> None:
    response: httpx.Response = client.post(
        '/auth/roles/is-admin',
        headers={'Authorization': f'Bearer {access_token}'}
    )

    assert response.status_code == 400
    assert response.json()['result'] is False
