import httpx
import pytest
from fastapi.testclient import TestClient
from jose import ExpiredSignatureError

from database import models


def test_sign_up_success(client: TestClient, user_credentials: dict, created_role_user: models.Role) -> None:
    response: httpx.Response = client.post(
        '/auth/sign-up',
        json=user_credentials
    )

    assert response.status_code == 200
    assert response.json()['result'] is True


def test_sign_up_fail(client: TestClient, created_user: models.User, user_credentials: dict) -> None:
    response: httpx.Response = client.post(
        '/auth/sign-up',
        json=user_credentials
    )
    assert response.status_code == 400
    assert response.json()['result'] is False


def test_sign_in_success(client: TestClient, created_user: models.User, user_credentials: dict) -> None:
    response: httpx.Response = client.post(
        '/auth/sign-in',
        json=user_credentials
    )
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'access_token' in response_json
    assert 'refresh_token' in response_json


def test_sign_in_fail(client: TestClient) -> None:
    response: httpx.Response = client.post('/auth/sign-in', json={'username': 'test', 'password': 'badpassword'})

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'


def test_sign_out_success(client: TestClient, access_token: str) -> None:
    response: httpx.Response = client.post('/auth/sign-out', headers={'Authorization': f'Bearer {access_token}'})

    assert response.status_code == 200
    assert response.json()['result'] is True


def test_sign_out_fail(client: TestClient) -> None:
    response: httpx.Response = client.post('/auth/sign-out', headers={'Authorization': 'Bearer badtoken'})

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'


def test_refresh_token_success(client: TestClient, refresh_token: str) -> None:
    response: httpx.Response = client.post(
        '/auth/refresh-token',
        json={'grant_type': 'refresh_token', 'refresh_token': refresh_token},
    )
    response_json: dict = response.json()

    assert response.status_code == 200
    assert 'access_token' in response_json
    assert 'refresh_token' in response_json


def test_refresh_token_bad_grant_type(client: TestClient) -> None:
    response: httpx.Response = client.post(
        '/auth/refresh-token',
        json={'grant_type': 'access_token'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'


def test_refresh_token_expired(client: TestClient, expired_refresh_token: str) -> None:
    with pytest.raises(ExpiredSignatureError):
        _ = client.post(
            '/auth/refresh-token',
            json={'grant_type': 'refresh_token', 'refresh_token': expired_refresh_token}
        )
