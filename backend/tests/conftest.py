import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from database.base import Base
from database.session import get_db
from factories import db_connection_factory
from main import app


engine = create_engine(url=db_connection_factory.make_test_connection_string(), pool_pre_ping=True)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db


@pytest.fixture(autouse=True, scope='function')
def prepare_database() -> None:
    """Create db tables and drop after each test"""
    with engine.begin() as connection:
        Base.metadata.create_all(bind=connection)
    yield
    with engine.begin() as connection:
        Base.metadata.drop_all(bind=connection)


@pytest.fixture(autouse=True, scope='function')
def client() -> TestClient:
    """Get test client with overrode dependencies"""
    app.dependency_overrides[get_db] = override_get_db
    return TestClient(app)


@pytest.fixture(scope='function')
def db() -> Session:
    """Get session for db-rows creating"""
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()
