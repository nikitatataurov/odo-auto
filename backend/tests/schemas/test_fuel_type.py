import pytest
from pydantic import ValidationError

import schemas
from value_objects import FuelTypeName


def test_fuel_type_create() -> None:
    name: str = FuelTypeName.GASOLINE.value
    fuel_type_create: schemas.FuelTypeCreate = schemas.FuelTypeCreate(
        name=name
    )

    assert fuel_type_create is not None
    assert fuel_type_create.name == name


def test_fuel_type() -> None:
    name: str = FuelTypeName.GASOLINE
    fuel_type: schemas.FuelType = schemas.FuelType(
        id=1,
        name=name
    )

    assert fuel_type is not None
    assert fuel_type.name == name


def test_fuel_type_incorrect_name() -> None:
    with pytest.raises(ValidationError):
        _: schemas.FuelType = schemas.FuelType(
            id=1,
            name='incorrectname'
        )
