import schemas
import value_objects


def test_country_create() -> None:
    country: value_objects.CountryName = value_objects.CountryName.BY
    code: str = country.name
    country_create: schemas.CountryCreate = schemas.CountryCreate(
        code=code,
        name=country
    )

    assert country_create is not None
    assert country_create.code == code


def test_country() -> None:
    country: value_objects.CountryName = value_objects.CountryName.BY
    code: str = country.name
    country: schemas.Country = schemas.Country(
        id=1,
        code=code,
        name=country
    )

    assert country is not None
    assert country.code == code
