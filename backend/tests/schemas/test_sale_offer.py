import datetime

import schemas


def test_sale_offer_create() -> None:
    car_id: int = 1
    sale_offer_create: schemas.SaleOfferCreate = schemas.SaleOfferCreate(
        mileage=100000,
        price=20000,
        car_id=1,
        marketplace_id=car_id,
        created_at=datetime.datetime.now()
    )

    assert sale_offer_create is not None
    assert sale_offer_create.car_id == car_id


def test_sale_offer(car: schemas.Car, marketplace: schemas.Marketplace) -> None:
    car_id: int = car.id
    sale_offer: schemas.SaleOffer = schemas.SaleOffer(
        id=1,
        mileage=100000,
        price=20000,
        car_id=car_id,
        marketplace_id=marketplace.id,
        created_at=datetime.datetime.now(),
        car=car,
        marketplace=marketplace
    )

    assert sale_offer is not None
    assert sale_offer.car_id == car_id
