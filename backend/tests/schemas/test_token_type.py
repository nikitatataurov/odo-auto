import schemas


def test_token_type(user: schemas.User) -> None:
    name: str = 'access'
    token_type: schemas.TokenType = schemas.TokenType(
        name=name
    )

    assert token_type is not None
    assert token_type.name == name
