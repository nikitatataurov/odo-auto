import schemas


def test_role_create() -> None:
    name: str = 'test'
    role_create: schemas.RoleCreate = schemas.RoleCreate(
        name=name,
    )

    assert role_create is not None
    assert role_create.name == name


def test_role() -> None:
    name: str = 'test'
    role: schemas.Role = schemas.Role(
        id=1,
        name=name,
    )

    assert role is not None
    assert role.name == name
