import datetime

import schemas


def test_token_create(user: schemas.User) -> None:
    hash: str = 'testhash'
    token_create: schemas.TokenCreate = schemas.TokenCreate(
        hash=hash,
        type='access',
        user_id=user.id,
    )

    assert token_create is not None
    assert token_create.hash == hash


def test_token(user: schemas.User) -> None:
    hash: str = 'testhash'
    token: schemas.Token = schemas.Token(
        id=1,
        hash=hash,
        type='access',
        user_id=user.id,
        user=user,
        created_at=datetime.datetime.now()
    )

    assert token is not None
    assert token.hash == hash
