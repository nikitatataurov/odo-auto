import datetime

import schemas


def test_parsing_history_create() -> None:
    ended_on_page_number: int = 1
    parsing_history_create: schemas.ParsingHistoryCreate = schemas.ParsingHistoryCreate(
        sale_offers_page_id=1,
        ended_on_page_number=ended_on_page_number
    )

    assert parsing_history_create is not None
    assert parsing_history_create.ended_on_page_number == ended_on_page_number


def test_parsing_history(sale_offers_page: schemas.SaleOffersPage) -> None:
    ended_on_page_number: int = 1
    parsing_history: schemas.ParsingHistory = schemas.ParsingHistory(
        id=1,
        sale_offers_page_id=sale_offers_page.id,
        sale_offers_page=sale_offers_page,
        ended_on_page_number=ended_on_page_number,
        created_at=datetime.datetime.now(),
        updated_at=datetime.datetime.now()
    )

    assert parsing_history is not None
    assert parsing_history.ended_on_page_number == ended_on_page_number
