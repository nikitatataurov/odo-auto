import datetime

import schemas


def test_create_car_create() -> None:
    brand: str = 'mercedes'
    car_create: schemas.CarCreate = schemas.CarCreate(
        brand=brand,
        model='e500',
        engine_volume=5000,
        production_year=2023,
        fuel_type_id=1
    )

    assert car_create is not None
    assert car_create.brand == brand


def test_create_car(fuel_type: schemas.FuelType) -> None:
    brand: str = 'mercedes'
    car: schemas.Car = schemas.Car(
        id=1,
        brand=brand,
        model='e300',
        production_year=2023,
        engine_volume=3000,
        fuel_type_id=fuel_type.id,
        fuel_type=fuel_type,
        created_at=datetime.datetime.now()
    )

    assert car is not None
    assert car.brand == brand
