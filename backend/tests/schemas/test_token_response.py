import schemas


def test_token_response(user: schemas.User) -> None:
    access_token: str = 'testaccesstoken'
    refresh_token: str = 'testrefreshtoken'
    token_response: schemas.TokenResponse = schemas.TokenResponse(
        access_token=access_token,
        refresh_token=refresh_token,
        is_admin=False,
    )

    assert token_response is not None
    assert token_response.access_token == access_token
    assert token_response.refresh_token == refresh_token
