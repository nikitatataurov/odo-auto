import schemas


def test_sale_offers_page_create() -> None:
    marketplace_id: int = 1
    sale_offers_page_create: schemas.SaleOffersPageCreate = schemas.SaleOffersPageCreate(
        marketplace_id=marketplace_id,
        brand='mercedes',
        path='/cars/mercedes'
    )

    assert sale_offers_page_create is not None
    assert sale_offers_page_create.marketplace_id == marketplace_id


def test_sale_offers_page(marketplace: schemas.Marketplace) -> None:
    marketplace_id: int = marketplace.id
    sale_offers_page: schemas.SaleOffersPage = schemas.SaleOffersPage(
        id=1,
        marketplace_id=marketplace_id,
        marketplace=marketplace,
        brand='mercedes',
        path='/cars/mercedes'
    )

    assert sale_offers_page is not None
    assert sale_offers_page.marketplace_id == marketplace_id
