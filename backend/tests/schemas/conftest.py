import datetime

import pytest

import schemas
import value_objects


@pytest.fixture(scope='function')
def country() -> schemas.Country:
    country: value_objects.CountryName = value_objects.CountryName.BY
    return schemas.Country(
        id=1,
        code=country.name,
        name=country
    )


@pytest.fixture(scope='function')
def marketplace(country: schemas.Country) -> schemas.Marketplace:
    return schemas.Marketplace(
        id=1,
        name=value_objects.MarketplaceName.AUTOBY_BY,
        country_id=country.id,
        url='https://auto.by',
        country=country
    )


@pytest.fixture(scope='function')
def fuel_type() -> schemas.FuelType:
    return schemas.FuelType(
        id=1,
        name=value_objects.FuelTypeName.GASOLINE
    )


@pytest.fixture(scope='function')
def car(fuel_type: schemas.FuelType) -> schemas.Car:
    return schemas.Car(
        id=1,
        brand=value_objects.BrandName.MERCEDES,
        model='e300',
        production_year=2023,
        engine_volume=3000,
        fuel_type_id=fuel_type.id,
        fuel_type=fuel_type,
        created_at=datetime.datetime.now()
    )


@pytest.fixture(scope='function')
def sale_offer(car: schemas.Car, marketplace: schemas.Marketplace) -> schemas.SaleOffer:
    return schemas.SaleOffer(
        id=1,
        mileage=100000,
        price=20000,
        car_id=car.id,
        marketplace_id=marketplace.id,
        created_at=datetime.datetime.now(),
        car=car,
        marketplace=marketplace
    )


@pytest.fixture(scope='function')
def sale_offers_page(marketplace: schemas.Marketplace) -> schemas.SaleOffersPage:
    return schemas.SaleOffersPage(
        id=1,
        marketplace_id=marketplace.id,
        marketplace=marketplace,
        brand=value_objects.BrandName.MERCEDES,
        path='/cars/mercedes'
    )


@pytest.fixture(scope='function')
def user() -> schemas.User:
    return schemas.User(
        id=1,
        username='test',
        email='test@mail.ru',
        created_at=datetime.datetime.now(),
        roles=[]
    )
