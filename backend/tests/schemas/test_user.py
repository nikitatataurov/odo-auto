import datetime

import schemas


def test_user_create() -> None:
    username: str = 'test'
    user_create: schemas.UserCreate = schemas.UserCreate(
        username=username,
        email='test@mail.ru',
        password='testhashofpassword'
    )

    assert user_create is not None
    assert user_create.username == username


def test_user_sign_in() -> None:
    username: str = 'test'
    user_create: schemas.UserSignIn = schemas.UserSignIn(
        username=username,
        password='testhashofpassword'
    )

    assert user_create is not None
    assert user_create.username == username


def test_user() -> None:
    username: str = 'test'
    user: schemas.User = schemas.User(
        id=1,
        username=username,
        email='test@mail.ru',
        created_at=datetime.datetime.now(),
        roles=[]
    )

    assert user is not None
    assert user.username == username
