import schemas
import value_objects


def test_marketplace_create() -> None:
    name: str = value_objects.MarketplaceName.AUTOBY_BY
    marketplace_create: schemas.MarketplaceCreate = schemas.MarketplaceCreate(
        name=name,
        country_id=1,
        url='https://auto.by'
    )

    assert marketplace_create is not None
    assert marketplace_create.name == name


def test_marketplace(country: schemas.Country) -> None:
    name: str = value_objects.MarketplaceName.AUTOBY_BY
    marketplace: schemas.Marketplace = schemas.Marketplace(
        id=1,
        name=name,
        country_id=country.id,
        url='https://auto.by',
        country=country
    )

    assert marketplace is not None
    assert marketplace.name == name
