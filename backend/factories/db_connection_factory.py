from config import settings


def _get_connection_string_base() -> str:
    """Get base for connection string"""
    return f'postgresql://{settings.POSTGRES_USER}:{settings.POSTGRES_PASSWORD}@scrapper_postgres:5432'


def make_connection_string() -> str:
    """Make connection string to db"""
    return f'{_get_connection_string_base()}/{settings.POSTGRES_DB}'


def make_test_connection_string() -> str:
    return f'{_get_connection_string_base()}/{settings.POSTGRES_DB_TEST}'
