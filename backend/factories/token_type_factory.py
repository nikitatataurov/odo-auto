import schemas
from value_objects import TokenTypeName


def make_access() -> schemas.TokenType:
    """Make access"""
    return schemas.TokenType(name=TokenTypeName.ACCESS)


def make_refresh() -> schemas.TokenType:
    """Make refresh"""
    return schemas.TokenType(name=TokenTypeName.REFRESH)
