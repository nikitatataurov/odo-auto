from fastapi import responses, status
from sqlalchemy.orm import Session

import schemas
from database.repositories import user_repository
from services import auth_service, role_service


def make_result_true() -> responses.JSONResponse:
    """Make json-response with result: True"""
    return responses.JSONResponse(content={'result': True}, status_code=status.HTTP_200_OK)


def make_result_false() -> responses.JSONResponse:
    """Make json-response with result: False"""
    return responses.JSONResponse(content={'result': False}, status_code=status.HTTP_400_BAD_REQUEST)


async def make_token_response(db: Session, username: str, user_id: int) -> schemas.TokenResponse:
    return schemas.TokenResponse(
        access_token=auth_service.create_access_token(username=username, user_id=user_id, db=db).hash,
        refresh_token=auth_service.create_refresh_token(username=username, user_id=user_id, db=db).hash,
        is_admin=await role_service.is_admin(db_user=user_repository.get_by_username(db=db, username=username))
    )


def make_result_bool(bool_value: bool) -> responses.JSONResponse:
    """Make json-response by boolean result"""
    return responses.JSONResponse(content={'result': bool_value}, status_code=status.HTTP_200_OK)
