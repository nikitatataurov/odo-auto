'use client'

import axios from "axios";
import React, {useEffect, useState} from "react";
import CommonButton from "@/components/UI/CommonButton";
import {useTranslation} from "react-i18next";

type Props = {
    item: IParsingDescription;
}

export default function ParsingPanelItem({item}: Props) {
    const {t} = useTranslation("parsing");

    const [isRunning, setIsRunning] = useState<boolean>(false)
    const startParsing = (e: React.MouseEvent) => {
        e.preventDefault();
        setIsRunning(true);
        axios.post(
            "/api/parsing/start/",
            {route: item.start_parsing_route}
        );
    }

    const stopParsing = (e: React.MouseEvent) => {
        e.preventDefault();
        setIsRunning(false);
        axios.post(
            "/api/parsing/stop/",
            {route: item.stop_parsing_route}
        );
    }


    useEffect(
        () => {
            (
                async () => setIsRunning(
                    (
                        await axios.post(
                            "/api/parsing/is-running/", {route: item.is_running_parsing_route})).data?.result
                )
            )();
        },
        []
    )

    return (
        <div
            className="mx-auto mt-16 max-w-2xl rounded-3xl ring-1 ring-gray-200 sm:mt-20 lg:mx-0 lg:flex lg:max-w-none">
            <div className="p-8 sm:p-10 lg:flex-auto lg:justify-between text-center">
                <h3 className="text-2xl font-bold tracking-tight text-base-color-900">{item.marketplace_name}</h3>
                <h5 className="text-sm tracking-tight text-base-color">
                    {t(isRunning ? "is_running" : "stopped")}
                </h5>
                <div>
                    <CommonButton key={"start"} content={t("start_parsing")} onClick={startParsing} isDisabled={isRunning}/>
                    <CommonButton key={"stop"} content={t("stop_parsing")} onClick={stopParsing} isDisabled={!isRunning}/>
                </div>
            </div>
        </div>
    );
}
