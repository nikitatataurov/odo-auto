import {TFunction} from "i18next";

type Props = {
    parsingHistory: IParsingHistory;
    t: TFunction;
}

export default function ParsingHistoryTableRow({ parsingHistory, t }: Props) {
    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" >
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{parsingHistory.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{parsingHistory.sale_offers_page.marketplace.name}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{parsingHistory.sale_offers_page.brand}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{parsingHistory.ended_on_page_number}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{t(parsingHistory.is_stopped ? "yes" : "no")}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{parsingHistory.created_at}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{parsingHistory.created_at}</td>
    </tr>
}
