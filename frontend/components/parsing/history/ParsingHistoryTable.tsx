import ParsingHistoryTableRow from "@/components/parsing/history/ParsingHistoryTableRow";
import {TFunction} from "i18next";

type Props = {
    parsingHistories: IParsingHistory[];
    t: TFunction;
}

export default function ParsingHistoryTable({parsingHistories, t}: Props) {
    return <div className="flex flex-col">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                    <table className="min-w-full text-left text-sm font-light">
                        <thead className="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" className="px-6 py-4 text-center">{t("id")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("marketplace")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("brand")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("ended_on_page")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("is_stopped")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("created_at")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("updated_at")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {parsingHistories.map(
                            (parsingHistory: IParsingHistory) =>
                                <ParsingHistoryTableRow key={parsingHistory.id} parsingHistory={parsingHistory} t={t}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
}
