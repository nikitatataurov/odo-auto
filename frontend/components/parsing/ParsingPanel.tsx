import {ParsingService} from "@/services/ParsingService";
import React from "react";
import ParsingPanelItem from "@/components/parsing/ParsingPanelItem";
import {TFunction} from "i18next";


type Props = {
    t: TFunction;
}

export default async function ParsingPanel({ t }: Props) {
    const items: IParsingDescription[] = await ParsingService.getRoutesInfo();

    return (
        <div className="bg-white py-24 sm:py-32">
            <div className="mx-auto max-w-7xl px-6 lg:px-8">
                <div className="mx-auto max-w-2xl sm:text-center">
                    <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">{t("parsing_panel")}</h2>
                </div>
                <div className="flex flex-col sm:flex-row sm:flex-wrap justify-between">
                    {
                        items.map(
                            item => <ParsingPanelItem key={item.marketplace_name} item={item} />
                        )
                    }
                </div>
            </div>

        </div>
    );
}
