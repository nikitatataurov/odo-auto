type Props = {
    content: string,
    onClick: (e: React.MouseEvent) => void,
    isDisabled: boolean,
}

export default function CommonButton({content, onClick, isDisabled}: Props) {
    return (
        <button type="button" onClick={onClick}
                disabled={isDisabled}
                className={`text-first-additional-color bg-base-color hover:bg-second-additional-color hover:text-first-additional-color rounded-md px-3 py-2 m-5 text-sm font-medium disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none`}
                aria-current="page">{ content }
        </button>
    );
}
