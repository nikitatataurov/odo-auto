import React from "react";
import {useTranslation} from "react-i18next";

type Props = {
    options: any[];
    setId: (id: number) => void;
}


const defaultValue: string = "DEFAULT"
export default function CreationFormSelect({options, setId}: Props) {
    const {t} = useTranslation('ui');

    return <select defaultValue={defaultValue}
        className="peer h-full w-full rounded-[7px]" onChange={e => setId(Number(e.target.value))}>
        <option disabled value={defaultValue}> -- {t("select_an_option")} -- </option>
        {
            options.map(
                option => <option key={option.id}
                                     value={option.id}>{option.name}</option>
            )
        }
    </select>
}