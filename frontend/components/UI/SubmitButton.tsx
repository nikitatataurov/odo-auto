type Props = {
    content: string,
}

export default function SubmitButton({ content }: Props) {
    return <button type="submit"
                   className="rounded-md bg-base-color px-5 py-3 mt-30 text-sm font-semibold text-white shadow-sm hover:bg-base-color focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-main-color-300">
        { content }
    </button>
}