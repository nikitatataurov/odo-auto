import Link from "next/link";
import React from "react";

type Props = {
    page: number,
    isSelected: boolean,
}

export default function PageNumberItem({ page, isSelected }: Props) {
    return <Link href={`?page=${page}`} aria-current="page"
               className={`relative inline-flex items-center px-4 py-2 text-sm font-semibold ${isSelected ? "z-10 bg-base-color px-4 py-2 text-white" : "text-base-color"}`}
    >
        { page }
    </Link>
}