import React from "react";
import {useRouter} from "next/navigation";

type Props = {
    content: string,
    path: string,
}

export default function PushToButton({ content, path }: Props) {
    const router = useRouter();

    const pushTo = (e: React.MouseEvent) => {
        e.preventDefault();
        router.push(path);
    }

    return <button type="button" onClick={pushTo}
                   className="rounded-md border-solid border-2 border-base-color bg-first-additional-color px-5 py-3 mt-30 text-sm font-semibold text-base-color shadow-sm hover:bg-base-color hover:text-first-additional-color focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-">
        {content}
    </button>
}