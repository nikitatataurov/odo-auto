import React from "react";
import Link from "next/link";
import PageNumberItem from "@/components/UI/PageNumberItem";

type Props = {
    page: number,
    pages: number,
}


export default function PaginationPanel({ page, pages }: Props) {
    const pageNumberItems: React.JSX.Element[] = [];
    const firstPage: number = 1;
    const numbers: number[] = [];
    pages = pages === 0 ? 1 : pages; // pages value is minimum 1

    for (let number = firstPage + 1; number < pages; number++) {
        numbers.push(number);
    }
    let differenceLeft = page - firstPage + 1;
    if (differenceLeft > 5) {
        differenceLeft = 5;
    }

    let differenceRight = pages - page;
    if (differenceRight > 3) {
        differenceRight = 2;
    }

    for (let pageNumber of numbers.slice(page - differenceLeft, page + differenceRight)) {
        pageNumberItems.push(<PageNumberItem page={pageNumber} isSelected={pageNumber === page} />)
    }

    if (page - differenceLeft >= firstPage) {
        pageNumberItems.unshift(<div>...</div>)
    }
    if (page + 5 <= pages) {
        pageNumberItems.push(<div>...</div>)
    }

    return <div>
        <nav className="isolate inline-flex -space-x-px rounded-md shadow-sm" aria-label="PaginationPanel">
            <Link href={`?page=${page - 1}`}
               className={`${page === 1 ? 'invisible' : ''} relative inline-flex items-center rounded-l-md px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0`}>
                <span className="sr-only">Previous</span>
                <svg className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fillRule="evenodd"
                          d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z"
                          clipRule="evenodd"/>
                </svg>
            </Link>
            <PageNumberItem page={firstPage} isSelected={firstPage === page}/>
            {
                pageNumberItems.map((pageNumberItem => pageNumberItem))
            }
            { firstPage !== pages && <PageNumberItem page={pages} isSelected={pages === page}/> }

            <Link href={`?page=${page + 1}`}
               className={`${page === pages ? 'invisible' : ''} relative inline-flex items-center rounded-r-md px-2 py-2 text-gray-400 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-20 focus:outline-offset-0`}>
                <span className="sr-only">Next</span>
                <svg className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fillRule="evenodd"
                          d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z"
                          clipRule="evenodd"/>
                </svg>
            </Link>
        </nav>
    </div>
}