'use client'

import {useRouter} from "next/navigation";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context";

type Props = {
    fuelType: IFuelType,
}

export default function FuelTypeTableRow({ fuelType }: Props) {
    const router: AppRouterInstance = useRouter()

    const showDetail = () => router.push(`/fuel-types/${fuelType.id}`)

    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" onClick={showDetail}>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{fuelType.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{fuelType.name}</td>
    </tr>
}
