'use client'

import {useRouter} from "next/navigation";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context";

type Props = {
    saleOffer: ISaleOffer,
}

export default function SaleOfferTableRow({ saleOffer }: Props) {
    const router: AppRouterInstance = useRouter();

    const car: ICar = saleOffer.car;

    const showDetail = () => router.push(`/sale-offers/${saleOffer.id}`)

    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" onClick={showDetail}>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{saleOffer.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffer.marketplace.name}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.brand}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.model}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.production_year}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.engine_volume}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.fuel_type.name}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffer.mileage}</td>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{saleOffer.price}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffer.url}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffer.created_at}</td>
    </tr>
}
