import SaleOfferTableRow from "@/components/saleOffers/SaleOfferTableRow";
import {TFunction} from "i18next";

type Props = {
    saleOffers: ISaleOffer[];
    t: TFunction;
}

export default function SaleOfferTable({saleOffers, t}: Props) {
    return <div className="flex flex-col">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                    <table className="min-w-full text-left text-sm font-light">
                        <thead className="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" className="px-6 py-4 text-center">{t("id")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("marketplace")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("brand")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("model")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("production_year")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("engine_volume")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("fuel_type")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("mileage")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("price")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("url")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("created_at")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {saleOffers.map(
                            (saleOffer: ISaleOffer) =>
                                <SaleOfferTableRow key={saleOffer.id} saleOffer={saleOffer}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
}
