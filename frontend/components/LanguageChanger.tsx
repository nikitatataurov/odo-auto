'use client';

import { useRouter } from 'next/navigation';
import { usePathname } from 'next/navigation';
import { useTranslation } from 'react-i18next';
import React from "react";


export default function LanguageChanger() {
    const { i18n } = useTranslation();
    const currentLocale = i18n.language;
    const router = useRouter();
    const currentPathname = usePathname();

    const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const newLocale = e.target.value;
        // set cookie for next-i18n-router
        const days = 30;
        const date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        const expires = date.toUTCString();
        document.cookie = `NEXT_LOCALE=${newLocale};expires=${expires};path=/`;

        // redirect to the new locale path
        if (
            currentLocale !== newLocale
        ) {
            router.push(
                currentPathname.replace(`/${currentLocale}`, `/${newLocale}`)
            );
            router.refresh();
        }
    };

    return (
        <select className={"appearance-none bg-base-color text-first-additional-color hover:bg-second-additional-color hover:text-first-additional-color rounded-md px-3 py-2 text-sm font-medium focus:ring-0 focus:border-transparent"} onChange={handleChange} value={currentLocale} >
            <option value="en" className={"bg-base-color hover:bg-second-additional-color"}>English</option>
            <option value="ru" className={"bg-base-color hover:bg-second-additional-color"}>Русский</option>
        </select>
    );
}