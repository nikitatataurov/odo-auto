import {SaleOfferService} from "@/services/SaleOfferService";
import {CarService} from "@/services/CarService";
import {FuelTypeService} from "@/services/FuelTypeService";
import {SaleOffersPageService} from "@/services/SaleOffersPageService";
import {MarketplaceService} from "@/services/MarketplaceService";
import {CountryService} from "@/services/CountryService";
import StatisticsDashboardItem from "@/components/statistics/StatisticsDashboardItem";
import {TFunction} from "i18next";


type Props = {
    t: TFunction;
}

export default async function StatisticsDashboard({ t }: Props) {
    const items: IStatisticsDashboardItem[] = [
        {name: t("car_count"), count: await CarService.getCountAll()},
        {name: t("fuel_types_count"), count: await FuelTypeService.getCountAll()},
        {name: t("countries_count"), count: await CountryService.getCountAll()},
        {name: t("marketplaces_count"), count: await MarketplaceService.getCountAll()},
        {name: t("sale_offers_count"), count: await SaleOfferService.getCountAll()},
        {name: t("sale_offers_pages_count"), count: await SaleOffersPageService.getCountAll()},
    ]

    return (
        <div className="bg-white py-24 sm:py-32">
            <div className="mx-auto max-w-7xl px-6 lg:px-8">
                <div className="mx-auto max-w-2xl sm:text-center">
                    <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">{t("statistics_dashboard")}</h2>
                </div>
                <div className="flex flex-col sm:flex-row sm:flex-wrap justify-between ">
                    {
                        items.map(
                            item => <StatisticsDashboardItem name={item.name} count={item.count} />
                        )
                    }
                </div>
            </div>

        </div>
    );
}
