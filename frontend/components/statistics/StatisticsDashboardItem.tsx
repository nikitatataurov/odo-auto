
type Props = {
    name: string,
    count?: number,
}
export default async function StatisticsDashboardItem({ name, count }: Props) {
    return (
        <div
            className="mx-auto mt-16 max-w-2xl rounded-3xl ring-1 ring-gray-200 sm:mt-20 lg:mx-0 lg:flex lg:max-w-none">
            <div className="p-8 sm:p-10 lg:flex-auto text-center">
                <h3 className="text-2xl pb-3 font-bold tracking-tight text-gray-900">{name}</h3>
                {count}
            </div>
        </div>
    );
}
