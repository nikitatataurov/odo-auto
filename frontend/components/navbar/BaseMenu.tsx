import Link from "next/link";
import React from "react";


type Props = {
    navItems: INavItem[];
    selectedMenuItem: INavItem | null;
    showSubMenu: (navItem: INavItem) => void;
}

export default function BaseMenu({ navItems, selectedMenuItem, showSubMenu }: Props) {
    return (
        <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
            <div className="hidden sm:ml-6 sm:block">
                <div className="flex space-x-4">
                    {
                        navItems.map(
                            navItem => {
                                const isActive = selectedMenuItem?.label === navItem.label;

                                return <Link
                                    key={navItem.label}
                                    href={navItem.href}
                                    className={`text-first-additional-color hover:bg-second-additional-color hover:text-first-additional-color rounded-md px-3 py-2 text-sm font-medium ${isActive ? ' border border-second-additional-color rounded-md ' : ''}`}
                                    aria-current="page"
                                    onClick={() => showSubMenu(navItem)}
                                >
                                    {navItem.label}
                                </Link>
                            }
                        )
                    }
                </div>
            </div>
        </div>
    );
}