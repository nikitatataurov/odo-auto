import {signIn, signOut, useSession} from "next-auth/react";

import Link from "next/link";
import LanguageChanger from "@/components/LanguageChanger";
import React from "react";
import {useTranslation} from "react-i18next";

type Props = {
    pathname: string;
}

export default function AuthPanel({pathname}: Props) {
    const { t } = useTranslation('auth');
    const { data: session } = useSession();

    return (
        <div
            className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
            { !session?.user ?
                <>
                    <button onClick={() => signIn()}
                            className={`text-first-additional-color hover:bg-second-additional-color hover:text-first-additional-color rounded-md px-3 py-2 text-sm font-medium ${pathname === "/auth/sign-in" ? ' border border-second-additional-color rounded-md ' : ''}`}
                            aria-current="page">{t("sign_in")}
                    </button>
                    <Link href="/auth/sign-up"
                          className={`text-first-additional-color hover:bg-second-additional-color hover:text-first-additional-color rounded-md px-3 py-2 text-sm font-medium ${pathname === "/auth/sign-up" ? ' border border-second-additional-color rounded-md ' : ''}`}
                          aria-current="page">{t("sign_up")}
                    </Link>
                </>
                :
                <button onClick={() => signOut()}
                        className={`text-first-additional-color hover:bg-second-additional-color hover:text-first-additional-color rounded-md px-3 py-2 text-sm font-medium`}
                        aria-current="page">{t("sign_out")}
                </button>
            }
            <LanguageChanger />
        </div>
    );
}
