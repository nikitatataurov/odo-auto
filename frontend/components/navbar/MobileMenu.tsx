import React from "react";
import Link from "next/link";


type Props = {
    pathname: string;
    navItems: INavItem[];
}

export default function MobileMenu({pathname, navItems}: Props) {
    return (
        <div className="sm:hidden" id="mobile-menu">
            <div className="space-y-1 px-2 pb-3 pt-2">
                {
                    navItems.map(
                        navItem => {
                            const isActive = pathname == navItem.href;

                            return <Link
                                key={navItem.label}
                                href={navItem.href}
                                className={`text-first-additional-color hover:bg-base-color hover:text-white block rounded-md px-3 py-2 text-base font-medium ${isActive ? ' border border-second-additional-color rounded-md ' : ''}`}
                                aria-current="page"
                            >
                                {navItem.label}
                            </Link>
                        }
                    )
                }
            </div>
        </div>
    );
}