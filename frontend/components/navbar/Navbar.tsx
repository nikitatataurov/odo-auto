'use client';

import React, {useEffect, useState} from "react";
import MobileMenu from "@/components/navbar/MobileMenu";
import MobileMenuButton from "@/components/navbar/MobileMenuButton";
import BaseMenu from "@/components/navbar/BaseMenu";
import AuthPanel from "@/components/navbar/AuthPanel";
import SubBaseMenu from "@/components/navbar/SubBaseMenu";
import {useTranslation} from "react-i18next";


type Props = {
    pathname: string;
}

export default function Navbar({pathname}: Props) {
    const { t } = useTranslation('nav');
    const navItems: INavItem[] = [
        {label: t("base_models"), href: "#", parent: null},
        {label: t("cars"), href: "/cars", parent: t("base_models")},
        {label: t("countries"), href: "/countries", parent: t("base_models")},
        {label: t("fuel_types"), href: "/fuel-types", parent: t("base_models")},
        {label: t("marketplaces"), href: "/marketplaces", parent: t("base_models")},
        {label: t("sale_offers"), href: "/sale-offers", parent: t("base_models")},
        {label: t("sale_offers_pages"), href: "/sale-offers-pages", parent: t("base_models")},
        {label: t("parsing"), href: "#", parent: null},
        {label: t("statistics"), href: "/statistics", parent: t("parsing")},
        {label: t("parsing_panel"), href: "/parsing/panel", parent: t("parsing")},
        {label: t("parsing_histories"), href: "/parsing/history", parent: t("parsing")},
    ]

    const [isShowMobileMenu, setIsShowMobileMenu] = useState<boolean>(false);
    const [selectedMenuItem, setSelectedMenuItem] = useState<INavItem | null>(null);
    const [currentSubMenuItems, setCurrentSubMenuItems] = useState<INavItem[]>([]);
    
    const baseMenuItems: INavItem[] = navItems.filter( navItem => navItem.parent === null);
    const subMenuItems: INavItem[] = navItems.filter( navItem => navItem.parent !== null);
    
    const showSubMenu = (navItem: INavItem) => {
        if (navItem.parent === null) {
            setSelectedMenuItem(navItem);
            setCurrentSubMenuItems(navItems.filter(item => item.parent === navItem.label));
        }

        if (selectedMenuItem && navItem.label === selectedMenuItem.label) {
            setSelectedMenuItem(null);
            setCurrentSubMenuItems([]);
        }

        if (navItem?.parent !== null) {
            setCurrentSubMenuItems([])
            setSelectedMenuItem(navItem);
        }
    };

    return (
        <nav className="bg-base-color">
            <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                <div className="relative flex h-16 items-center justify-between">
                    <MobileMenuButton onClick={() => setIsShowMobileMenu(!isShowMobileMenu)}/>
                    <BaseMenu navItems={baseMenuItems} selectedMenuItem={selectedMenuItem} showSubMenu={showSubMenu}/>
                    <AuthPanel pathname={pathname} />
                </div>
            </div>
            {
                currentSubMenuItems.length > 0
                && <SubBaseMenu pathname={pathname} subMenuItems={currentSubMenuItems} showSubMenu={showSubMenu} />
            }
            {isShowMobileMenu && <MobileMenu pathname={pathname} navItems={subMenuItems}/>}
        </nav>
    );
}