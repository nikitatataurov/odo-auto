import Link from "next/link";
import React from "react";

type Props = {
    pathname: string,
    subMenuItems: INavItem[],
    showSubMenu: (navItem: INavItem) => void;
}

export default function SubBaseMenu({ pathname, subMenuItems, showSubMenu }: Props) {
    return (
        <div className="hidden sm:block container mx-auto py-3 border-double border-t-2 border-second-additional-color"
             id="mobile-menu">
            <div className="flex flex-1 items-center justify-content">
                {
                    subMenuItems.map(
                        navItem => {
                            const isActive = pathname == navItem.href;
                            return <Link
                                key={navItem.label}
                                href={navItem.href}
                                className={`text-first-additional-color hover:text-white block rounded-md px-3 py-2 text-base font-medium ${isActive ? ' border border-second-additional-color rounded-md ' : ''}`}
                                aria-current="page"
                                onClick={() => showSubMenu(navItem)}
                            >
                                {navItem.label}
                            </Link>
                        }
                    )
                }
            </div>
        </div>
    )
}