'use client'

import {useRouter} from "next/navigation";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context.shared-runtime";

type Props = {
    saleOffersPage: ISaleOffersPage,
}

export default function SaleOffersPageTableRow({ saleOffersPage }: Props) {
    const router: AppRouterInstance = useRouter();

    const showDetail = () => router.push(`/sale-offers-pages/${saleOffersPage.id}`);

    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" onClick={showDetail}>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{saleOffersPage.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffersPage.marketplace.name}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffersPage.brand}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffersPage.path}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{saleOffersPage.query_params}</td>
    </tr>
}
