import SaleOffersPageTableRow from "@/components/saleOffersPages/SaleOffersPageTableRow";
import {TFunction} from "i18next";

type Props = {
    saleOffersPages: ISaleOffersPage[];
    t: TFunction;
}

export default function SaleOffersPageTable({saleOffersPages, t}: Props) {
    return <div className="flex flex-col ">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                    <table className="min-w-full text-left text-sm font-light">
                        <thead className="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" className="px-6 py-4 text-center">{t("id")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("marketplace")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("brand")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("path")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("query_params")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {saleOffersPages.map(
                            (saleOffersPage: ISaleOffersPage) =>
                                <SaleOffersPageTableRow key={saleOffersPage.id} saleOffersPage={saleOffersPage}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
}
