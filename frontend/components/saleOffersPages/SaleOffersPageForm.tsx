"use client";

import React, {useEffect, useState} from "react";
import SubmitButton from "@/components/UI/SubmitButton";
import PushToButton from "@/components/UI/PushToButton";
import CreationFormSelect from "@/components/UI/CreationFormSelect";
import useCreate from "@/hooks/useCreate";
import axios from "axios";
import {useTranslation} from "react-i18next";


export default function SaleOffersPageForm() {
    const { t } = useTranslation(['sale-offers-pages']);

    const [brand, setBrand] = useState<string>("");
    const [path, setPath] = useState<string>("");
    const [queryParams, setQueryParams] = useState<string>("");
    const [marketplaceId, setMarketplaceId] = useState<number>();


    const [marketplaces, setMarketplaces] = useState<IMarketplace[]>([]);


    useEffect(
        () => {
            (async () => {
                setMarketplaces((await axios.get("/api/marketplaces/all")).data);
            })();
        },
        []
    )

    const reset = () => {
        setBrand("");
        setPath("");
        setQueryParams("");
    }

    const prepareFormData = (): ISaleOffersPageCreate => {
        return {
            brand: brand,
            path: path,
            query_params: queryParams,
            marketplace_id: Number(marketplaceId),
        };
    }

    return <form className="flex justify-center" onSubmit={
        e => {
            e.preventDefault();
            useCreate("/api/sale-offers-pages/create", prepareFormData(), reset);
        }
    }>
        <div className="space-y-12">
            <div>
                <h2 className="text-xl font-semibold leading-7 text-gray-900 mt-36 text-base-color">{t("sale_offers_page_creation_form")}</h2>
                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                    <div className="sm:col-span-4">
                        <label htmlFor="brand"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("brand")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="brand" id="brand"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={brand}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setBrand(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="path"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("path")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="path" id="path"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={path}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPath(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="queryParams" className="block text-sm font-medium leading-6 text-gray-900">{t("query_params")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="queryParams" id="queryParams"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={queryParams}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setQueryParams(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="marketplaceId" className="block text-sm font-medium leading-6 text-gray-900">{t("marketplace")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <CreationFormSelect options={marketplaces} setId={setMarketplaceId}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="mt-6 flex items-center justify-end gap-x-6">
            <PushToButton content={t("cancel")} path={"/sale-offers-pages"} />
            <SubmitButton content={t("create")} />
        </div>
    </form>
}
