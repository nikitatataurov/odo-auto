'use client'

import {useRouter} from "next/navigation";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context";

type Props = {
    marketplace: IMarketplace,
}

export default function MarketplaceTableRow({ marketplace }: Props) {
    const router: AppRouterInstance = useRouter();

    const showDetail = () => router.push(`/marketplaces/${marketplace.id}`);

    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" onClick={showDetail}>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{marketplace.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{marketplace.name}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{marketplace.url}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{marketplace.country.name}</td>
    </tr>
}
