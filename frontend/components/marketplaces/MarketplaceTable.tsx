import MarketplaceTableRow from "@/components/marketplaces/MarketplaceTableRow";
import {TFunction} from "i18next";

type Props = {
    marketplaces: IMarketplace[];
    t: TFunction;
}

export default function MarketplaceTable({marketplaces, t}: Props) {
    return <div className="flex flex-col ">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                    <table className="min-w-full text-left text-sm font-light">
                        <thead className="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" className="px-6 py-4 text-center">{t("id")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("name")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("url")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("country_name")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {marketplaces.map(
                            (marketplace: IMarketplace) =>
                                <MarketplaceTableRow key={marketplace.id} marketplace={marketplace}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
}
