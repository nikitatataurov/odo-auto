"use client";

import React, {useEffect, useState} from "react";
import {CountryService} from "@/services/CountryService";
import SubmitButton from "@/components/UI/SubmitButton";
import PushToButton from "@/components/UI/PushToButton";
import CreationFormSelect from "@/components/UI/CreationFormSelect";
import useCreate from "@/hooks/useCreate";
import axios from "axios";
import {useTranslation} from "react-i18next";


export default function MarketplaceForm() {
    const { t } = useTranslation(['marketplaces']);

    const [name, setName] = useState<string>("");
    const [url, setUrl] = useState<string>("");
    const [countryId, setCountryId] = useState<number>();

    const [countries, setCountries] = useState<ICountry[]>([]);


    useEffect(
        () => {
            const fetchData = async () => {
                setCountries((await axios.get("/api/countries/all")).data);
            }

            fetchData();
        },
        []
    );

    const reset = () => {
        setName("");
        setUrl("");
    }

    const prepareFormData = (): IModelCreate => {
        return {
            name: name,
            url: url,
            country_id: Number(countryId),
        };
    }

    return <form className="flex justify-center" onSubmit={
        e => {
            e.preventDefault();
            useCreate("/api/marketplaces/create", prepareFormData(), reset);
        }
    }>
        <div className="space-y-12">
            <div>
                <h2 className="text-xl font-semibold leading-7 text-gray-900 mt-36 text-base-color">{t("marketplace_creation_form")}</h2>
                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                    <div className="sm:col-span-4">
                        <label htmlFor="name"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("name")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="name" id="name"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={name}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="url"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("url")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="url" id="url"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={url}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUrl(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="countryId" className="block text-sm font-medium leading-6 text-gray-900">{t("country_name")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <CreationFormSelect options={countries} setId={setCountryId}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="mt-6 flex items-center justify-end gap-x-6">
            <PushToButton content={t("cancel")} path={"/marketplaces"} />
            <SubmitButton content={t("create")} />
        </div>
    </form>
}
