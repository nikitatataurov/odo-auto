"use client"

import React from "react";
import Navbar from "@/components/navbar/Navbar";
import {usePathname} from "next/navigation";


export default function Header() {
    const pathname: string = usePathname();

    return (
        <Navbar pathname={pathname} />
    )
}
