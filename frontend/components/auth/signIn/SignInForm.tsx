'use client'

import SubmitButton from "@/components/UI/SubmitButton";
import {FormEvent, useState} from "react";
import {signIn} from "next-auth/react";
import {useTranslation} from "react-i18next";

export default function SignInForm() {
    const { t } = useTranslation('auth');

    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    async function onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();
        await signIn("credentials", {
                username: username,
                password: password,
                redirect: true,
                callbackUrl: "/",
            }
        )
    }

    return <form className="flex justify-center" onSubmit={onSubmit}>
        <div className="space-y-12">
            <div>
                <h2 className="text-xl font-semibold leading-7 text-gray-900 mt-36 text-base-color">{t("credentials")}</h2>
                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                    <div className="sm:col-span-4">
                        <label htmlFor="username"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("username")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="username" id="username" value={username} onChange={event => setUsername(event.target.value)}
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>
                        <label htmlFor="password"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("password")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="password" name="password" id="password" value={password} onChange={event => setPassword(event.target.value)}
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="mt-6 flex items-center justify-end gap-x-6">
            <SubmitButton content={t("sign_in")}/>
        </div>
    </form>
}