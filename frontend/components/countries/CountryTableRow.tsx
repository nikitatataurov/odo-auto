'use client'

import {useRouter} from "next/navigation";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context";

type Props = {
    country: ICountry,
}

export default function CountryTableRow({ country }: Props) {
    const router: AppRouterInstance = useRouter();

    const showDetail = () => router.push(`/countries/${country.id}`);

    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" onClick={showDetail}>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{country.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{country.code}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{country.name}</td>
    </tr>
}
