import {TFunction} from "i18next";

type Props = {
    country: ICountry;
    t: TFunction;
}

export default function CountryItem({ country, t }: Props) {
    return <div className="bg-white py-24 sm:py-32">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-2xl sm:text-center">
          <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">{t("country_description")}</h2>
        </div>
        <div className="mx-auto mt-16 max-w-2xl rounded-3xl ring-1 ring-gray-200 sm:mt-20 lg:mx-0 lg:flex lg:max-w-none">
          <div className="p-8 sm:p-10 lg:flex-auto">
            <h3 className="text-2xl font-bold tracking-tight text-gray-900 text-center">{country.name} ({country.code})</h3>
          </div>
        </div>
      </div>
    </div>
}