'use client'

import {useRouter} from "next/navigation";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context.shared-runtime";

type Props = {
    car: ICar,
}

export default function CarTableRow({ car }: Props) {
    const router: AppRouterInstance = useRouter()

    const showDetail = () => router.push(`/cars/${car.id}`)

    return <tr className="border-b dark:border-neutral-500 hover:bg-main-color-100 cursor-pointer" onClick={showDetail}>
        <td className="whitespace-nowrap px-6 py-4 font-medium text-center">{car.id}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.brand}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.model}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.engine_volume}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.production_year}</td>
        <td className="whitespace-nowrap px-6 py-4 text-center">{car.fuel_type.name}</td>
    </tr>
}
