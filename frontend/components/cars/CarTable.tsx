import CarTableRow from "@/components/cars/CarTableRow";
import {TFunction} from "i18next";

type Props = {
    cars: ICar[];
    t: TFunction;
}

export default function CarTable({cars, t}: Props) {
    return <div className="flex flex-col ">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                <div className="overflow-hidden">
                    <table className="min-w-full text-left text-sm font-light">
                        <thead className="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" className="px-6 py-4 text-center">{t("id")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("brand")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("model")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("engine_volume")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("production_year")}</th>
                            <th scope="col" className="px-6 py-4 text-center">{t("fuel_type")}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {cars.map(
                            (car: ICar) =>
                                <CarTableRow key={car.id} car={car}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
}
