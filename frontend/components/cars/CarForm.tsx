"use client";

import React, {useEffect, useState} from "react";
import SubmitButton from "@/components/UI/SubmitButton";
import PushToButton from "@/components/UI/PushToButton";
import CreationFormSelect from "@/components/UI/CreationFormSelect";
import useCreate from "@/hooks/useCreate";
import axios from "axios";
import {useTranslation} from "react-i18next";


export default function CarForm() {
    const { t } = useTranslation(['cars']);

    const [brand, setBrand] = useState<string>("");
    const [model, setModel] = useState<string>("");
    const [productionYear, setProductionYear] = useState<string>("");
    const [engineVolume, setEngineVolume] = useState<string>("");
    const [fuelTypeId, setFuelTypeId] = useState<number>();


    const [fuelTypes, setFuelTypes] = useState<IFuelType[]>([]);


    useEffect(
        () => {
            (async () => {
                setFuelTypes((await axios.get("/api/fuel-types/all")).data);
            })();
        },
        []
    )

    const reset = () => {
        setBrand("");
        setModel("");
        setProductionYear("");
        setEngineVolume("");
    }

    const prepareFormData = (): ICarCreate => {
        return {
            brand: brand,
            model: model,
            production_year: Number(productionYear),
            engine_volume: Number(engineVolume),
            fuel_type_id: Number(fuelTypeId),
        };
    }

    return <form className="flex justify-center" onSubmit={
        e => {
            e.preventDefault();
            useCreate("/api/cars/create", prepareFormData(), reset);
        }
    }>
        <div className="space-y-12">
            <div>
                <h2 className="text-xl font-semibold leading-7 text-gray-900 mt-36 text-base-color">{t("car_creation_form")}</h2>
                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                    <div className="sm:col-span-4">
                        <label htmlFor="brand"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("brand")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="brand" id="brand"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={brand}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setBrand(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="model"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("model")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="model" id="model"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={model}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setModel(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="productionYear" className="block text-sm font-medium leading-6 text-gray-900">{t("production_year")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="productionYear" id="productionYear"
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={productionYear}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setProductionYear(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="engineVolume"
                               className="block text-sm font-medium leading-6 text-gray-900">{t("engine_volume")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <input required type="text" name="engineVolume" id=""
                                       className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                       value={engineVolume}
                                       onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEngineVolume(e.target.value)}
                                />
                            </div>
                        </div>
                        <label htmlFor="fuelTypeId" className="block text-sm font-medium leading-6 text-gray-900">{t("fuel_type")}*</label>
                        <div className="mt-2">
                            <div
                                className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                <CreationFormSelect options={fuelTypes} setId={setFuelTypeId}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="mt-6 flex items-center justify-end gap-x-6">
            <PushToButton content={t("cancel")} path={"/cars"} />
            <SubmitButton content={t("create")} />
        </div>
    </form>
}
