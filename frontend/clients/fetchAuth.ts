import { getServerSession } from "next-auth";
import {authOptions} from "@/app/api/auth/[...nextauth]/route";
import {BACKEND_URL} from "@/valueObjects/BackendUrl";


async function refreshToken(refreshToken: string) {
    const response = await fetch(BACKEND_URL.BASE + "/auth/refresh-token", {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
          refresh_token: refreshToken,
          grant_type: "refresh_token"
        }),
    });

    return (await response.json()).access_token;
}

async function getApiV1(url: string, access_token: string | null | undefined) {
    return await fetch(BACKEND_URL.API_V1 + url, {
        method: "GET",
        headers: {
          "Accept": 'application/json',
          "Content-Type": 'application/json',
          "Authorization": `Bearer ${access_token}`,
        }
    });
}

export async function fetchAuthGet(url: string) {
    const session = await getServerSession(authOptions);

    let response = await getApiV1(url, session?.user.access_token);

    if (response.status == 401) {
        if (session) {
          session.user.access_token = await refreshToken(session?.user.refresh_token ?? "");
        }

        response = await getApiV1(url, session?.user.access_token);
    }

    return response;
}


async function postApiV1(url: string, body: string, access_token: string | null | undefined) {
    return await fetch(BACKEND_URL.API_V1 + url, {
        method: "POST",
        headers: {
          "Accept": 'application/json',
          "Content-Type": 'application/json',
          "Authorization": `Bearer ${access_token}`,
        },
        body: body,
    });
}

export async function fetchAuthPost(url: string, body: string) {
    const session = await getServerSession(authOptions);
    let response = await postApiV1(url, body, session?.user.access_token);

    if (response.status == 401) {
        if (session) {
          session.user.access_token = await refreshToken(session?.user.refresh_token ?? "");
        }

        response = await postApiV1(url, body, session?.user.access_token);
    }

    return response;
}