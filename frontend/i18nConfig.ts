const i18nConfig = {
    locales: ['en', 'ru'],
    defaultLocale: 'en',
    prefixDefault: '/',
}

export default i18nConfig;
