interface ICountryCreate extends IModelCreate {
    code: string;
    name: string;
}

interface ICountry extends ICountryCreate {
    id: number;
}
