interface IParsingDescription {
    marketplace_name: string;
    start_parsing_route: string;
    stop_parsing_route: string;
    is_running_parsing_route: string;
}
