interface ISaleOffersPageCreate extends IModelCreate {
    marketplace_id: number;
    brand: string;
    path: string;
    query_params: string | null;
}

interface ISaleOffersPage extends ISaleOffersPageCreate {
    id: number;
    marketplace: IMarketplace;
}
