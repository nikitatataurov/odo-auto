interface IMarketplaceCreate extends IModelCreate {
    name: string;
    country_id: number;
    url: string;
}

interface IMarketplace extends IMarketplaceCreate {
    id: number;
    country: ICountry;
}