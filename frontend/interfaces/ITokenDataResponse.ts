interface ITokenDataResponse {
    access_token: string;
    refresh_token: string;
}