interface IStatisticsDashboardItem {
    name: string;
    count?: number;
}
