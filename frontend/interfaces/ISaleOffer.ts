interface ISaleOfferCreate extends IModelCreate {
    mileage: number;
    price: number;
    car_id: number;
    marketplace_id: number;
    created_at: string;
    url?: URL;
}

interface ISaleOffer extends ISaleOfferCreate {
    id: number;
    marketplace: IMarketplace;
    car: ICar;
}
