interface INavItem {
    label: string;
    href: string;
    parent: string | null;
}
