interface IParsingHistoryCreate extends IModelCreate {
    sale_offers_page_id: number;
    ended_on_page_number: number;
    is_stopped: boolean;
}

interface IParsingHistory extends IParsingHistoryCreate {
    id: number;
    sale_offers_page: ISaleOffersPage;
    created_at: string;
    updated_at: string;
}