interface ICarCreate extends IModelCreate {
    brand: string;
    model: string;
    production_year: number;
    engine_volume: number;
    fuel_type_id: number;
}

interface ICar extends ICarCreate {
    id: number;
    fuel_type: IFuelType;
    created_at: string;
}
