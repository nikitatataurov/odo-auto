interface IFuelTypeCreate extends IModelCreate {
    name: string;
}

interface IFuelType extends IFuelTypeCreate {
    id: number;
}