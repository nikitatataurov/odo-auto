import { NextResponse } from 'next/server'
import {MarketplaceService} from "@/services/MarketplaceService";

export async function POST(request: Request) {
    let response;

    try {
        const data: IMarketplaceCreate = await request.json();
        response= await MarketplaceService.create(data);
    } catch (error: any) {
        response = error.response;
    }

    return NextResponse.json({ data: response.data, status: response.status});
}