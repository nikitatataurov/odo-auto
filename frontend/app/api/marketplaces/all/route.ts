import { NextResponse } from 'next/server'
import { MarketplaceService } from "@/services/MarketplaceService";

export async function GET() {
    return NextResponse.json(await MarketplaceService.getAll())
}