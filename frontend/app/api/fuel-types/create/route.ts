import { NextResponse } from 'next/server'
import {FuelTypeService} from "@/services/FuelTypeService";

export async function POST(request: Request) {
    let response;

    try {
        const data: IFuelTypeCreate = await request.json();
        response= await FuelTypeService.create(data);
    } catch (error: any) {
        response = error.response;
    }

    return NextResponse.json({ data: response.data, status: response.status});
}