import { NextResponse } from 'next/server'
import { FuelTypeService } from "@/services/FuelTypeService";

export async function GET() {
    return NextResponse.json(await FuelTypeService.getAll())
}