import { NextResponse } from 'next/server'
import { SaleOffersPageService } from "@/services/SaleOffersPageService";

export async function POST(request: Request) {
    let response;

    try {
        const data: ISaleOffersPageCreate = await request.json();
        response= await SaleOffersPageService.create(data);
    } catch (error: any) {
        response = error.response;
    }

    return NextResponse.json({ data: response.data, status: response.status});
}
