import { NextResponse } from 'next/server'
import { CarService } from "@/services/CarService";

export async function POST(request: Request) {
    let response;

    try {
        const data: ICarCreate = await request.json();
        response = await CarService.create(data);
    } catch (error: any) {
        response = error.response;
    }

    return NextResponse.json({ data: response.data, status: response.status});
}
