import { NextResponse } from 'next/server'
import {AuthService} from "@/services/AuthService";

export async function POST(request: Request) {
    const response = await AuthService.signUp(await request.json());

    return NextResponse.json({ data: response.data, status: response.status });
}
