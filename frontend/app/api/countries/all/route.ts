import { NextResponse } from 'next/server'
import { CountryService } from "@/services/CountryService";

export async function GET() {
    return NextResponse.json(await CountryService.getAll());
}
