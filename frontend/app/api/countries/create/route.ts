import { NextResponse } from 'next/server'
import {CountryService} from "@/services/CountryService";

export async function POST(request: Request) {
    let response;

    try {
        const data: ICountryCreate = await request.json();
        response= await CountryService.create(data);
    } catch (error: any) {
        response = error.response;
    }

    return NextResponse.json({ data: response.data, status: response.status});
}
