import { NextResponse } from 'next/server'
import {ParsingService} from "@/services/ParsingService";

export async function POST(request: Request) {
    const response = await ParsingService.postToRoute((await request.json()).route);

    return NextResponse.json({ result: response.data?.result });
}
