import {ParsingService} from "@/services/ParsingService";
import {NextResponse} from "next/server";

export async function POST(request: Request) {
    const response = await ParsingService.getToRoute((await request.json()).route);

    return NextResponse.json({ result: response.data?.result });
}