import SaleOfferTable from "@/components/saleOffers/SaleOfferTable";
import {SaleOfferService} from "@/services/SaleOfferService";
import {Metadata} from "next";
import PaginationPanel from "@/components/UI/PaginationPanel";
import initTranslations from "@/app/i18n";



const i18nNamespaces = ["sale-offers"];

type Props = {
    searchParams: { [key: string]: string | string[] | undefined },
    params: {
        locale: string,
    },
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title"),
        description: t("description"),
    };
}


export default async function SaleOffersPage({ searchParams, params: { locale } }: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const page: number = Number(searchParams?.page) || 1;
    const pageData: IPage<ISaleOffer> = await SaleOfferService.getPageData(page);

    const saleOffers: ISaleOffer[] = pageData.items;

    return <div>
        <SaleOfferTable saleOffers={saleOffers} t={t} />
        <div className="flex justify-center m-6">
            <PaginationPanel page={pageData.page} pages={pageData.pages}/>
        </div>
    </div>
}