import {SaleOfferService} from "@/services/SaleOfferService";
import SaleOfferItem from "@/components/saleOffers/SaleOfferItem";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["sale-offers"];

type Props = {
    params: {
        id: number,
        locale: string,
    };
};


export async function generateMetadata(
    { params: { id, locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const saleOffer: ISaleOffer = await SaleOfferService.getById(id);

    const car: ICar = saleOffer.car;

    return {
        title: t("title_id", {carBrand: car.brand, carModel: car.model, marketplaceName: saleOffer.marketplace.name}),
        description: t("description_id")
    };
}


export default async function SaleOfferPage({ params: { id, locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const saleOffer: ISaleOffer = await SaleOfferService.getById(id);

    return <SaleOfferItem saleOffer={saleOffer} t={t}></SaleOfferItem>
}