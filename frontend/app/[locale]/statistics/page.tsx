import {Metadata} from "next";
import React from "react";
import StatisticsDashboard from "@/components/statistics/StatisticsDashboard";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["statistics"];

type Props = {
    params: {
        locale: string,
    };
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title"),
        description: t("description"),
    };
}

export default async function SaleOffersPagesPage({ params: { locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);
    return (
        <div>
            <StatisticsDashboard t={t}/>
        </div>
    );
}