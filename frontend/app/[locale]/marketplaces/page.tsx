import MarketplaceTable from "@/components/marketplaces/MarketplaceTable";
import {MarketplaceService} from "@/services/MarketplaceService";
import {Metadata} from "next";
import Link from "next/link";
import PaginationPanel from "@/components/UI/PaginationPanel";
import {RoleService} from "@/services/RoleService";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["marketplaces"];

type Props = {
    searchParams: { [key: string]: string | string[] | undefined };
    params: {
        locale: string,
    };
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title"),
        description: t("description"),
    };
}


export default async function MarketplacesPage({ searchParams, params: { locale } }: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const page: number = Number(searchParams?.page) || 1;
    const pageData: IPage<IMarketplace> = await MarketplaceService.getPageData(page);

    const isAdmin: boolean = await RoleService.isAdmin();

    const marketplaces: IMarketplace[] = pageData.items;

    return <div>
        { isAdmin && <div className="flex justify-center mt-6">
            <Link href="/marketplaces/create"
                  className="rounded-md bg-base-color px-5 py-3 mt-30 text-sm font-semibold text-white shadow-sm hover:bg-base-color focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-main-color-300">{t("create_marketplace")}</Link>
        </div>
        }
        <MarketplaceTable marketplaces={marketplaces} t={t} />
        <div className="flex justify-center m-6">
            <PaginationPanel page={pageData.page} pages={pageData.pages}/>
        </div>
    </div>
}