import {MarketplaceService} from "@/services/MarketplaceService";
import MarketplaceItem from "@/components/marketplaces/MarketplaceItem";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";
import {CarService} from "@/services/CarService";


const i18nNamespaces = ["marketplaces"];

type Props = {
    params: {
        id: number,
        locale: string,
    };
};


export async function generateMetadata(
    { params: { id, locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);
    const marketplace: IMarketplace = await MarketplaceService.getById(id);
    return {
        title: t("title_id", {marketplaceName: marketplace.name}),
        description: t("description_id")
    };
}



export default async function MarketplacePage({ params: { id, locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const marketplace: IMarketplace = await MarketplaceService.getById(id);

    return <MarketplaceItem marketplace={marketplace} t={t}></MarketplaceItem>
}