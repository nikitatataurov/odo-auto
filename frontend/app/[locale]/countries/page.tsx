import CountryTable from "@/components/countries/CountryTable";
import {CountryService} from "@/services/CountryService";
import {Metadata} from "next";
import Link from "next/link";
import PaginationPanel from "@/components/UI/PaginationPanel";
import {RoleService} from "@/services/RoleService";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["countries"];

type Props = {
    searchParams: { [key: string]: string | string[] | undefined },
    params: {
        locale: string,
    },
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces)

    return {
        title: t("title"),
        description: t("description"),
    }
}

export default async function CountriesPage({ searchParams, params: { locale } }: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const page: number = Number(searchParams?.page) || 1;
    const pageData: IPage<ICountry> = await CountryService.getPageData(page);

    const isAdmin: boolean = await RoleService.isAdmin();

    const countries: ICountry[] = pageData.items;

    return <div>
        { isAdmin && <div className="flex justify-center mt-6">
            <Link href="/countries/create"
                  className="rounded-md bg-base-color px-5 py-3 mt-30 text-sm font-semibold text-white shadow-sm hover:bg-base-color focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-main-color-300">{t("create_country")}</Link>
        </div>
        }
        <CountryTable countries={countries} t={t} />
        <div className="flex justify-center m-6">
            <PaginationPanel page={pageData.page} pages={pageData.pages}/>
        </div>
    </div>
}