import {CountryService} from "@/services/CountryService";
import CountryItem from "@/components/countries/CountryItem";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";
import {CarService} from "@/services/CarService";


const i18nNamespaces = ["countries"];

type Props = {
    params: {
        id: number,
        locale: string,
    };
};


export async function generateMetadata(
    { params: { id, locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);
    const country: ICountry = await CountryService.getById(id);

    return {
        title: t("title_id", {countryName:country.name, countryCode: country.code}),
        description: t("description_id")
    };
}


export default async function CountryPage({ params: { id, locale } }: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces)

    const country: ICountry = await CountryService.getById(id)

    return <CountryItem country={country} t={t}></CountryItem>
}