import SaleOffersPageForm from "@/components/saleOffersPages/SaleOffersPageForm";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["sale-offers-pages"];

type Props = {
    params: {
        locale: string,
    };
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title_create"),
        description: t("description_create"),
    };
}

export default function SaleOffersPageCreatePage() {
    return <SaleOffersPageForm />
}