import {SaleOffersPageService} from "@/services/SaleOffersPageService";
import SaleOffersPageItem from "@/components/saleOffersPages/SaleOffersPageItem";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["sale-offers-pages"];

type Props = {
    params: {
        id: number,
        locale: string,
    };
};


export async function generateMetadata(
    { params: { id, locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const saleOffersPage: ISaleOffersPage = await SaleOffersPageService.getById(id);

    return {
        title: t("title_id", {marketplaceName: saleOffersPage.marketplace.name, carBrand: saleOffersPage.brand}),
        description: t("description_id")
    };
}


export default async function SaleOffersPagePage({ params: { id, locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const saleOffersPage: ISaleOffersPage = await SaleOffersPageService.getById(id);

    return <SaleOffersPageItem saleOffersPage={saleOffersPage} t={t}></SaleOffersPageItem>
}