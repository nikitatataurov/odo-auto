import {Metadata} from "next";
import React from "react";
import FuelTypeForm from "@/components/fuelTypes/FuelTypeForm";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["fuel-types"];

type Props = {
    params: {
        locale: string,
    };
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title_create"),
        description: t("description_create"),
    };
}

export default function FuelTypeCreatePage() {
    return <FuelTypeForm />
}