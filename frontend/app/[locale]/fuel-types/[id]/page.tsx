import {FuelTypeService} from "@/services/FuelTypeService";
import FuelTypeItem from "@/components/fuelTypes/FuelTypeItem";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["fuel-types"];

type Props = {
    params: {
        id: number,
        locale: string,
    };
};


export async function generateMetadata(
    { params: { id, locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);
    const fuelType: IFuelType = await FuelTypeService.getById(id);

    return {
        title: t("title_id", {fuelTypeName: fuelType.name}),
        description: t("description_id")
    };
}


export default async function FuelTypePage({ params: { id, locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const fuelType: IFuelType = await FuelTypeService.getById(id);

    return <FuelTypeItem fuelType={fuelType} t={t}></FuelTypeItem>
}