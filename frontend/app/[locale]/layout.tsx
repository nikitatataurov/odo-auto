import './globals.css';
import type {Metadata} from 'next';
import Header from "@/components/Header";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import Providers from "@/providers/Providers";
import {ReactNode} from "react";
import i18nConfig from "@/i18nConfig";
import {dir} from "i18next";
import initTranslations from "@/app/i18n";


type Props = {
    children: ReactNode;
    params: {
        locale: string,
    };
}

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, ['layout'])

    return {
        title: t("title"),
        description: t("description"),
    }
}

export function generateStaticParams() {
    return i18nConfig.locales.map(locale => ({ locale }));
}

export default async function RootLayout({children, params: {locale}}: Props) {

    return (
        <html lang={locale} dir={dir(locale)}>
        <body className="">
        <Providers locale={locale}>
            <Header/>
            <ToastContainer/>
            <main className="container mx-auto">
                {children}
            </main>
        </Providers>
        </body>
        </html>
    );
}
