import ParsingHistoryTable from "@/components/parsing/history/ParsingHistoryTable";
import {ParsingHistoryService} from "@/services/ParsingHistoryService";
import {Metadata} from "next";
import PaginationPanel from "@/components/UI/PaginationPanel";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["parsing"];

type Props = {
    searchParams: { [key: string]: string | string[] | undefined };
    params: {
        locale: string,
    };
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title_history"),
        description: t("description_history"),
    };
}

export default async function ParsingHistoryPage({ searchParams, params: { locale } }: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces)

    const page: number = Number(searchParams?.page) || 1;
    const pageData: IPage<IParsingHistory> = await ParsingHistoryService.getPageData(page);

    const parsingHistories: IParsingHistory[] = pageData.items;

    return <div>
        <ParsingHistoryTable parsingHistories={parsingHistories} t={t}/>
        <div className="flex justify-center m-6">
            <PaginationPanel page={pageData.page} pages={pageData.pages}/>
        </div>
    </div>
}