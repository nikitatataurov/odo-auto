import {Metadata} from "next";
import ParsingPanel from "@/components/parsing/ParsingPanel";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["parsing"];

type Props = {
    params: {
        locale: string,
    };
};

export async function generateMetadata(
    { params: { locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return {
        title: t("title_panel"),
        description: t("description_panel"),
    };
}

export default async function ParsingPanelPage({ params: { locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    return <ParsingPanel t={t}/>
}