import {CarService} from "@/services/CarService";
import CarItem from "@/components/cars/CarItem";
import {Metadata} from "next";
import initTranslations from "@/app/i18n";


const i18nNamespaces = ["cars"];

type Props = {
    params: {
        id: number,
        locale: string,
    };
};


export async function generateMetadata(
    { params: { id, locale } }: Props,
): Promise<Metadata> {
    const {t} = await initTranslations(locale, i18nNamespaces);
    const car: ICar = await CarService.getById(id);

    return {
        title: t("title_id", {carBrand: car.brand, carModel: car.model}),
        description: t("description_id")
    };
}


export default async function CarPage({ params: { id, locale }}: Props) {
    const {t} = await initTranslations(locale, i18nNamespaces);

    const car: ICar = await CarService.getById(id);

    return <CarItem car={car} t={t}></CarItem>
}