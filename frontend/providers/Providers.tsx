"use client";

import { SessionProvider } from "next-auth/react";
import React, { ReactNode } from "react";
import TranslationsProvider from "@/providers/TranslationsProvider";
import initTranslations from "@/app/i18n";


const i18nNamespaces = [
    "layout", "nav", "auth", "cars", "countries", "fuel-types", "marketplaces", "sale-offers-pages", "sale-offers",
    "ui", "statistics", "parsing",
];

type Props = {
    children: ReactNode;
    locale: string;
}


const Providers = async ({children, locale}: Props) => {
    const {resources} = await initTranslations(locale, i18nNamespaces)

    return <SessionProvider>
        <TranslationsProvider
            namespaces={i18nNamespaces}
            locale={locale}
            resources={resources}
        >
            {children}
        </TranslationsProvider>
    </SessionProvider>;
};

export default Providers;
