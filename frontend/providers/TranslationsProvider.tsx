'use client';

import { I18nextProvider } from 'react-i18next';
import initTranslations from '@/app/i18n';
import { createInstance } from 'i18next';
import React, {ReactNode} from "react";

type Props = {
    children: ReactNode;
    locale: string;
    namespaces: Array<string>;
    resources: object;
}

const TranslationsProvider = (
    {
        children,
        locale,
        namespaces,
        resources
    }: Props
) => {
    const i18n = createInstance();

    initTranslations(locale, namespaces, i18n, resources);

    return  <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
}


export default TranslationsProvider;