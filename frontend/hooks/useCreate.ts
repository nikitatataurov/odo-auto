import {ToastService} from "@/services/ToastService";
import axios from "axios";


export default function useCreate(
    path: string,
    data: IModelCreate,
    reset: () => void,
): void {
    (async () => {
        try {
            const apiResponseData: IApiResponseData = (await axios.post(path, data)).data

            if (apiResponseData.status === 201) {
                ToastService.notifyCreated();
                reset()
            } else {
                ToastService.notifyError(JSON.stringify(apiResponseData.data));
            }
        } catch (error: any) {
            ToastService.notifyError(error.message);
        }
    })()
}
