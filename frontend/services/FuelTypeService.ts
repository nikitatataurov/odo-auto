import {fetchAuthGet, fetchAuthPost} from "@/clients/fetchAuth";

export const FuelTypeService = {
    async getAll() {
        const response = await fetchAuthGet("/fuel-types/all/");

        return response.json();
    },

    async getPageData(page: number): Promise<IPage<IFuelType>> {
        const response = await fetchAuthGet(`/fuel-types/?page=${page}&size=10`);

        return response.json();
    },

    async getById(id: number) {
        const response = await fetchAuthGet(`/fuel-types/${id}/`);

        return response.json()
    },

    async create(data: IFuelTypeCreate) {
        return await fetchAuthPost("/fuel-types/", JSON.stringify(data));
    },

    async getCountAll() {
        const response = await fetchAuthGet("/fuel-types/count/all");

        return (await response.json())?.count;
    },
};
