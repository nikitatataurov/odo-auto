import {fetchAuthGet, fetchAuthPost} from "@/clients/fetchAuth";


export const CarService = {
    async getPageData(page: number): Promise<IPage<ICar>> {
        const response = await fetchAuthGet(`/cars/?page=${page}&size=10`);

        return response.json();
    },

    async getById(id: number): Promise<ICar> {
        const response = await fetchAuthGet(`/cars/${id}/`);

        return response.json()
    },

    async create(data: ICarCreate) {
        return await fetchAuthPost("/cars/", JSON.stringify(data));
    },

    async getCountAll() {
        const response = await fetchAuthGet("/cars/count/all");

        return (await response.json())?.count;
    },
};
