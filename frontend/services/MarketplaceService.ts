import {fetchAuthGet, fetchAuthPost} from "@/clients/fetchAuth";

export const MarketplaceService = {
    async getAll() {
        const response = await fetchAuthGet("/marketplaces/all/");

        return response.json();
    },

    async getPageData(page: number): Promise<IPage<IMarketplace>> {
        const response = await fetchAuthGet(`/marketplaces/?page=${page}&size=10`);

        return response.json();
    },

    async getById(id: number) {
        const response = await fetchAuthGet(`/marketplaces/${id}/`);

        return response.json();
    },

    async create(data: IMarketplaceCreate) {
        return await fetchAuthPost("/marketplaces/", JSON.stringify(data));
    },

    async getCountAll() {
        const response = await fetchAuthGet("/marketplaces/count/all");

        return (await response.json())?.count;
    },
};
