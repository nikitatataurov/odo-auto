import {fetchAuthGet, fetchAuthPost} from "@/clients/fetchAuth";

export const CountryService = {
    async getAll() {
        const response = await fetchAuthGet(`/countries/all/`);

        return response.json();
    },

    async getPageData(page: number): Promise<IPage<ICountry>> {
        const response = await fetchAuthGet(`/countries/?page=${page}&size=10`);

        return response.json();
    },

    async getById(id: number) {
        const response = await fetchAuthGet(`/countries/${id}/`);

        return response.json();
    },

    async create(data: ICountryCreate) {
        return await fetchAuthPost("/countries/", JSON.stringify(data));
    },

    async getCountAll() {
        const response = await fetchAuthGet("/countries/count/all");

        return (await response.json())?.count;
    },
};
