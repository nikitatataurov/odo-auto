import {getServerSession} from "next-auth";
import {authOptions} from "@/app/api/auth/[...nextauth]/route";

export const RoleService = {
    async isAdmin(): Promise<boolean> {
        const session = await getServerSession(authOptions);
        return Boolean(session?.user.is_admin);
    },
};
