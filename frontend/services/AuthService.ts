import axios from "axios";
import {BACKEND_URL} from "@/valueObjects/BackendUrl";


export const AuthService = {
    async signIn(data: object) {
        try {
            return await axios.post(`${BACKEND_URL.BASE}/auth/sign-in`, data);
        } catch (error: any) {
            return error.response;
        }
    },

    async signUp(data: object) {
        try {
            return await axios.post(`${BACKEND_URL.BASE}/auth/sign-up`, data);
        } catch (error: any) {
            return error.response;
        }
    },
};
