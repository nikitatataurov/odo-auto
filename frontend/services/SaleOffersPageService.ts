import {fetchAuthGet, fetchAuthPost} from "@/clients/fetchAuth";

export const SaleOffersPageService = {
    async getPageData(page: number): Promise<IPage<ISaleOffersPage>> {
        const response = await fetchAuthGet(`/sale-offers-pages/?page=${page}&size=10`);

        return response.json();
    },

    async getById(id: number) {
        const response = await fetchAuthGet(`/sale-offers-pages/${id}/`);

        return response.json()
    },

    async create(data: ISaleOffersPageCreate) {
        return await fetchAuthPost("/sale-offers-pages/", JSON.stringify(data));
    },

    async getCountAll() {
        const response = await fetchAuthGet("/sale-offers-pages/count/all");

        return (await response.json())?.count;
    },
};
