import {fetchAuthGet} from "@/clients/fetchAuth";

export const ParsingHistoryService = {
    async getPageData(page: number): Promise<IPage<IParsingHistory>> {
        const response = await fetchAuthGet(`/parsing/history/?page=${page}&size=10`);

        return response.json();
    },
};
