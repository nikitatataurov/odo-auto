import {fetchAuthGet} from "@/clients/fetchAuth";

export const SaleOfferService = {
    async getPageData(page: number): Promise<IPage<ISaleOffer>> {
        const response = await fetchAuthGet(`/sale-offers/?page=${page}&size=10`);

        return response.json();
    },

    async getById(id: number) {
        const response = await fetchAuthGet(`/sale-offers/${id}/`);

        return response.json();
    },

    async getCountAll() {
        const response = await fetchAuthGet("/sale-offers/count/all");

        return (await response.json())?.count;
    },
};
