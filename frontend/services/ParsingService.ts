import {fetchAuthGet, fetchAuthPost} from "@/clients/fetchAuth";

export const ParsingService = {
    async getRoutesInfo(): Promise<IParsingDescription[]> {
        const response = await fetchAuthGet("/parsing/routes/info/");

        return response.json();
    },

    async postToRoute(route: string): Promise<any> {
        return await fetchAuthPost(`/parsing${route}`, "");
    },

    async getToRoute(route: string): Promise<any> {
        return await fetchAuthGet(`/parsing${route}`);
    },
};
