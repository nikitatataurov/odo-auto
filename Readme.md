# ODO-AUTO PROJECT


## Scrapper
The scraper is needed to collect data from global automotive markets 
* Backend: Python3.11 + FastAPI + SQLAlchemy2.0
* Frontend: Next.js + Tailwind
* DB: Postgres
* Parsing driver: Selenium Chromium

#### Instruction:
* git pull https://gitlab.com/nikitatataurov/odo-auto-scraper
* mk .env file in scrapper dir with values for:
  * POSTGRES_USER
  * POSTGRES_PASSWORD
  * POSTGRES_DB
  * POSTGRES_DB_TEST
* mk .env file in backend dir with values for or use common root .env file and push it through volume to backend container:
  * POSTGRES_USER
  * POSTGRES_PASSWORD
  * POSTGRES_DB
  * POSTGRES_DB_TEST
  * SECRET_KEY
  * CRYPTOGRAPHIC_ALGORITHM
* cd scrapper && docker-compose up --build
* docker exec -ti scrapper_backend sh
* alembic revision --autogenerate -m "Init"
* alembic upgrade heads
#### Usage:
* create roles: user, admin
* create users with roles (only users with admin role can create entries through backend or frontend)
* create countries, fuel types, marketplaces, sale offers pages (certain pages on the marketplace)
* click parsing button
* look at sale offers page
* use API in other services

## ML service is https://gitlab.com/nikitatataurov/odo-auto-appraiser